package com.icatch.wcmapp3.ExtendComponent;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import com.icatch.wcmapp3.SDKAPI.PreviewStream;
import com.icatch.wcmapp3.Tool.ScaleTool;
import com.icatch.wcmapp3.camera.MyCamera;
import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.exception.IchAudioStreamClosedException;
import com.icatch.wificam.customer.exception.IchBufferTooSmallException;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStreamNotRunningException;
import com.icatch.wificam.customer.exception.IchTryAgainException;
import com.icatch.wificam.customer.exception.IchVideoStreamClosedException;
import com.icatch.wificam.customer.type.ICatchAudioFormat;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.iotgroup.roame.RoameApplication;

import java.nio.ByteBuffer;

public class PreviewMjpg extends SurfaceView implements SurfaceHolder.Callback {

	private int frameWidth;
	private int frameHeight;
	private byte[] pixelBuf;
	private ByteBuffer bmpBuf;
	private Rect drawFrameRect;
	private Bitmap videoFrameBitmap;
	private AudioTrack audioTrack;
	private PreviewStream previewStream = PreviewStream.getInstance();
	private SurfaceHolder holder;
	private VideoThread mySurfaceViewThread;
	private boolean hasSurface = false;
	private AudioThread audioThread;
	private boolean hasInit = false;
	private ICatchWificamPreview previewStreamControl;
	public ICatchWificamPreview icatchMedia;
	private MyCamera mCamera;
	private int screenWidth;
	private int screenHeight;

	public PreviewMjpg(Context context, AttributeSet attrs) {
		super(context, attrs);
		//JIRA ICOM-2098 Begin Add by b.jiang 2015-10-19
		holder = this.getHolder();
		holder.addCallback(this);
		holder.setFormat(PixelFormat.RGBA_8888);
		//JIRA ICOM-2098 End Add by b.jiang 2015-10-19
		
		
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		manager.getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
		
	}

	private void init() {
		frameWidth = previewStream.getVideoWidth(mCamera.getpreviewStreamClient());
		frameHeight = previewStream.getVideoHeigth(mCamera.getpreviewStreamClient());
		pixelBuf = new byte[frameWidth * frameHeight * 4];
		bmpBuf = ByteBuffer.wrap(pixelBuf);

		// Trigger onDraw with those initialize parameters
		if(videoFrameBitmap == null){
			videoFrameBitmap = Bitmap.createBitmap(frameWidth, frameHeight, Config.ARGB_8888);
		}
		drawFrameRect = new Rect(0, 0, frameWidth, frameHeight);
		// 创建一个新的SurfaceHolder， 并分配这个类作为它的回调(callback)
//		holder = this.getHolder();
//		holder.addCallback(this);


		hasInit = true;
		previewStreamControl = mCamera.getpreviewStreamClient();
	}

	public void start(MyCamera mCamera) {
		this.mCamera = mCamera;
		if (hasInit == false) {
			init();
		}
		// 创建和启动图像更新线程
		if (mySurfaceViewThread == null) {

			mySurfaceViewThread = new VideoThread();
			Log.d("1111", "PreviewMjpg: hasSurface =" + hasSurface);
			if (hasSurface == true)
				mySurfaceViewThread.start();
		}
		// 启动音频线程
		// JIRA ICOM-1844 Start Add by b.jiang 2015-08-14
		if (previewStream.supportAudio(mCamera.getpreviewStreamClient()) && (!RoameApplication.forbidAudioOutput)) {
			if (audioThread == null) {
				audioThread = new AudioThread();
				audioThread.start();
			}
		}
		// JIRA ICOM-1844 End Add by b.jiang 2015-08-14
	}

	public boolean stop() {
		// 杀死图像更新线程
		if (mySurfaceViewThread != null) {
			mySurfaceViewThread.requestExitAndWait();
			mySurfaceViewThread = null;
		}
		// 杀死音频线程
		if (audioThread != null) {
			audioThread.requestExitAndWait();
			audioThread = null;
		}
//		if (holder != null) {
//			holder.removeCallback(this);
//		}
		if (videoFrameBitmap != null) {
			videoFrameBitmap.recycle();
			videoFrameBitmap = null;
		}

		hasInit = false;
		return true;
	}

	private class VideoThread extends Thread {
		private boolean done;

		VideoThread() {
			super();
			done = false;
		}

		@Override
		public void run() {
			SurfaceHolder surfaceHolder = holder;
			ICatchFrameBuffer buffer = new ICatchFrameBuffer(screenWidth * screenHeight * 4);
			buffer.setBuffer(pixelBuf);
			boolean temp = false;
			boolean isSaveBitmapToDb = false;
			while (!done) {
				temp = false;
				try {
					// WriteLogToDevice.writeLog("[Normal] -- PreviewMjpg: ",
					// "start getNextVideoFrame");
					temp = previewStreamControl.getNextVideoFrame(buffer);
//					 WriteLogToDevice.writeLog("[Normal] -- PreviewMjpg: ","end getNextVideoFrame temp = " + temp);
				} catch (IchSocketException e) {
					// need to close preview get next video frame
					
					e.printStackTrace();
					return;
				} catch (IchBufferTooSmallException e) {
					
					e.printStackTrace();
					return;
				} catch (IchCameraModeException e) {
					
					e.printStackTrace();
					return;
				} catch (IchInvalidSessionException e) {
					
					e.printStackTrace();
					return;
				} catch (IchTryAgainException e) {
					
					e.printStackTrace();
				} catch (IchStreamNotRunningException e) {
					
					e.printStackTrace();
					return;
				} catch (IchInvalidArgumentException e) {
					
					e.printStackTrace();
					return;
				} catch (IchVideoStreamClosedException e) {
					
					e.printStackTrace();
					return;
				}
				if (temp == false) {
					continue;
				}
				if (buffer == null || buffer.getFrameSize() == 0) {
					continue;
				}

				bmpBuf.rewind();
				videoFrameBitmap.copyPixelsFromBuffer(bmpBuf);
				if (!isSaveBitmapToDb) {
					if(videoFrameBitmap != null){
//						CameraSlotSQLite.getInstance().updateImage(videoFrameBitmap);
						isSaveBitmapToDb = true;
					}				
				}

				// 锁定surface，并返回到要绘图的Canvas
				Canvas canvas = surfaceHolder.lockCanvas();
				if (canvas == null) {
					continue;
				}
				// // 待实现：在Canvas上绘图
				int w = getWidth();
				int h = getHeight();
				drawFrameRect = ScaleTool.getScaledPosition(screenWidth, screenHeight, w, h);
				canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
				// // 解锁Canvas，并渲染当前图像
				surfaceHolder.unlockCanvasAndPost(canvas);
			}
			if (videoFrameBitmap != null) {
//				CameraSlotSQLite.getInstance().updateImage(videoFrameBitmap);
			}
		}

		public void requestExitAndWait() {
			// 把这个线程标记为完成，并合并到主程序线程
			done = true;
			try {
				join();
			} catch (InterruptedException ex) {
			}
		}
	}

	private class AudioThread extends Thread {
		private boolean done = false;

		@Override
		public void run() {
			ICatchAudioFormat audioFormat = previewStream.getAudioFormat(previewStreamControl);
			int bufferSize = AudioTrack.getMinBufferSize(audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
					: AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT);

			audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
					: AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT,
					bufferSize, AudioTrack.MODE_STREAM);

			audioTrack.play();
			byte[] audioBuffer = new byte[1024 * 50];
			ICatchFrameBuffer icatchBuffer = new ICatchFrameBuffer(1024 * 50);
			icatchBuffer.setBuffer(audioBuffer);
			boolean temp = false;
			while (!done) {
				temp = false;
				try {
					temp = previewStreamControl.getNextAudioFrame(icatchBuffer);
				} catch (IchSocketException e) {
					e.printStackTrace();
					return;
				} catch (IchBufferTooSmallException e) {
					
					e.printStackTrace();
					return;
				} catch (IchCameraModeException e) {
					
					e.printStackTrace();
					return;
				} catch (IchInvalidSessionException e) {
					
					e.printStackTrace();
					return;
				} catch (IchTryAgainException e) {
					
					e.printStackTrace();
				} catch (IchStreamNotRunningException e) {
					
					e.printStackTrace();
					return;
				} catch (IchInvalidArgumentException e) {
					
					e.printStackTrace();
					return;
				} catch (IchAudioStreamClosedException e) {
					
					e.printStackTrace();

				}
				if (false == temp) {
					continue;
				}

				audioTrack.write(icatchBuffer.getBuffer(), 0, icatchBuffer.getFrameSize());
			}

			audioTrack.stop();
			audioTrack.release();

		}

		public void requestExitAndWait() {
			// 把这个线程标记为完成，并合并到主程序线程
			done = true;
			try {
				join();
			} catch (InterruptedException ex) {
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d("1111", "PreviewMjpg: surfaceCreated hasSurface =" + hasSurface);
		hasSurface = true;
		//VideoThread
		if (mySurfaceViewThread != null) {
			if (mySurfaceViewThread.isAlive() == false) {
				mySurfaceViewThread.start();
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d("1111", "PreviewMjpg: surfacesurfaceDestroyedCreated hasSurface =" + hasSurface);
		hasSurface = false;
		stop();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
	}

}
