package com.icatch.wcmapp3.ExtendComponent;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.icatch.wcmapp3.SDKAPI.VideoPlayback;
import com.icatch.wcmapp3.Tool.ScaleTool;
import com.icatch.wcmapp3.camera.MyCamera;
import com.icatch.wificam.customer.ICatchWificamVideoPlayback;
import com.icatch.wificam.customer.exception.IchAudioStreamClosedException;
import com.icatch.wificam.customer.exception.IchBufferTooSmallException;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchPbStreamPausedException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStreamNotRunningException;
import com.icatch.wificam.customer.exception.IchTryAgainException;
import com.icatch.wificam.customer.exception.IchVideoStreamClosedException;
import com.icatch.wificam.customer.type.ICatchAudioFormat;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchVideoFormat;

import java.nio.ByteBuffer;

public class VideoPbMjpg extends SurfaceView implements SurfaceHolder.Callback {

	private int frameWidth;
	private int frameHeight;
	private byte[] pixelBuf;
	private ByteBuffer bmpBuf;
	private Rect drawFrameRect;
	private Bitmap videoFrameBitmap;
	private VideoPlayback videoPlayback = VideoPlayback.getInstance();
	private ICatchWificamVideoPlayback videoPb;
	private int myWidth;
	private int myHeight;
	public final int ADJUST_LAYOUT_VIDEOPB = 1;
	private SurfaceHolder holder;
	private VideoThread mySurfaceViewThread;
	private boolean hasSurface = false;
	private AudioThread audioThread;
	private boolean hasInit = false;
	private AudioTrack audioTrack;
	private VideoPbUpdateBarLitener videoPbUpdateBarLitener;
	private boolean videoThreadDone = false;

	public VideoPbMjpg(Context context, AttributeSet attrs) {
		super(context, attrs);

		
		// JIRA ICOM-2029 Begin Add by b.jiang 2015-10-09;
		this.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				if (videoThreadDone) {
					reDrawBitmap();
				}
			}
		});
		// JIRA ICOM-2029 End Add by b.jiang 2015-10-09;
	}

	private boolean init() {
		frameWidth = 0;
		frameHeight = 0;
		// play之后还不能立马获取到frame 宽高，sdk获取到一帧才会获取到宽高;
		// 采用延时获取宽高，三次延时可以获取到;
		// JIRA ICOM-1912 Start:Add by b.jiang 2015-08-28
		videoThreadDone = false;
		long lastTime = System.currentTimeMillis();
		while (frameWidth == 0 || frameHeight == 0) {
			
			ICatchVideoFormat videoFormat = videoPlayback.getVideoFormat();
			
			if(videoFormat!=null){
				frameWidth = videoPlayback.getVideoFormat().getVideoW();
				frameHeight = videoPlayback.getVideoFormat().getVideoH();
			}
			
			try {
				Thread.sleep(33);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (System.currentTimeMillis() - lastTime > 5000) {
				break;
			}
		}
		if (frameWidth == 0 || frameHeight == 0) {
			return false;
		}
		// JIRA ICOM-1912 End:Add by b.jiang 2015-08-28
		pixelBuf = new byte[frameWidth * frameHeight * 4];
		bmpBuf = ByteBuffer.wrap(pixelBuf);

		// Trigger onDraw with those initialize parameters
		videoFrameBitmap = Bitmap.createBitmap(frameWidth, frameHeight, Config.ARGB_8888);
		// lastVideoFrameBitmap = Bitmap.createBitmap(frameWidth, frameHeight,
		// Config.ARGB_8888);
		drawFrameRect = new Rect(0, 0, frameWidth, frameHeight);
		holder = this.getHolder();
		holder.addCallback(this);
		holder.setFormat(PixelFormat.RGBA_8888);
		hasInit = true;
		myWidth = 0;
		myHeight = 0;
		return true;
	}

	public boolean start(MyCamera mCamera, Handler handler) {

		Log.d("1111", "start preview   hasInit =" + hasInit);
		videoPb = mCamera.getVideoPlaybackClint();
		if (hasInit == false) {
			if (!init()) {
				return false;
			}
		}

		// 创建和启动图像更新线程
		if (mySurfaceViewThread == null) {
			Log.d("1111", "mySurfaceViewThread == null) start new VideoThread!");
			mySurfaceViewThread = new VideoThread();
			if (hasSurface == true)
				mySurfaceViewThread.start();
		}
		// 启动音频线程
		if (audioThread == null) {
			audioThread = new AudioThread();
			audioThread.start();
		}
		return true;
	}

	public boolean start(MyCamera mCamera) {

		Log.d("1111", "start preview   hasInit =" + hasInit);
		videoPb = mCamera.getVideoPlaybackClint();
		if (hasInit == false) {
			if (!init()) {
				return false;
			}
		}

		// 创建和启动图像更新线程
		if (mySurfaceViewThread == null) {
			Log.d("1111", "mySurfaceViewThread == null) start new VideoThread!");
			mySurfaceViewThread = new VideoThread();
			if (hasSurface == true)
				mySurfaceViewThread.start();
		}
		// 启动音频线程
		if (audioThread == null) {
			audioThread = new AudioThread();
			audioThread.start();
		}
		return true;
	}
	
	public boolean stop() {
		// 杀死图像更新线程
		if (mySurfaceViewThread != null) {
			mySurfaceViewThread.requestExitAndWait();
			mySurfaceViewThread = null;
		}

		// 杀死音频线程
		if (audioThread != null) {
			audioThread.requestExitAndWait();
			audioThread = null;
		}
		if (holder != null) {
			holder.removeCallback(this);
		}
		hasInit = false;
		Log.d("1111", "end preview");
		return true;
	}

	private class VideoThread extends Thread {
		VideoThread() {
			super();
			videoThreadDone = false;
		}

		@Override
		public void run() {
			SurfaceHolder surfaceHolder = holder;
			ICatchFrameBuffer buffer = new ICatchFrameBuffer(frameWidth * frameHeight * 4);
			buffer.setBuffer(pixelBuf);
			boolean temp = false;
			long lastTime = System.currentTimeMillis();
			while (!videoThreadDone) {
				// ICOM-1913 ICOM-1911 End add by b.jiang 2015-08-28
				// 当宽高改变时，重新绘制画面;
				if (myWidth != getWidth() || myHeight != getHeight()) {
					if (getWidth() > 0 || getHeight() > 0) {
						myWidth = getWidth();
						myHeight = getHeight();
						if (videoFrameBitmap != null) {
							Canvas canvas = surfaceHolder.lockCanvas();
							drawFrameRect = ScaleTool.getScaledPosition(frameWidth, frameHeight, getWidth(), getHeight());
							canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
							surfaceHolder.unlockCanvasAndPost(canvas);
						}
					}
				}

				temp = false;
				try {
					// WriteLogToDevice.writeLog("[Error] -- Preview: ",
					// "start getNextVideoFrame");
					temp = videoPb.getNextVideoFrame(buffer);
				} catch (IchSocketException e) {
					// need to close preview get next video frame
					// 
					e.printStackTrace();
					return;
				} catch (IchBufferTooSmallException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchCameraModeException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchInvalidSessionException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchTryAgainException e) {
					// WriteLogToDevice.writeLog("[Error] -- VideoPbMjpg: ",
					// "IchTryAgainException");
					// 
					// Log.d("1111","IchTryAgainException!");
					e.printStackTrace();
				} catch (IchStreamNotRunningException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchInvalidArgumentException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchVideoStreamClosedException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchPbStreamPausedException e) {
					// 
					e.printStackTrace();
				}
				// WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ",
				// "end getNextVideoFrame retValue =" + retValue);
				if (temp == false) {
					continue;
				}
				if (buffer == null) {
					Log.e("Preview", "break");
					continue;
				}
				bmpBuf.rewind();
				videoFrameBitmap.copyPixelsFromBuffer(bmpBuf);
				// 锁定surface，并返回到要绘图的Canvas
				Canvas canvas = surfaceHolder.lockCanvas();
				drawFrameRect = ScaleTool.getScaledPosition(frameWidth, frameHeight, getWidth(), getHeight());
				if(canvas != null){
					canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
				}
				// 解锁Canvas，并渲染当前图像
				surfaceHolder.unlockCanvasAndPost(canvas);
				
				videoPbUpdateBarLitener.updateBar(buffer.getPresentationTime());
			}
		}

		public void requestExitAndWait() {
			// 把这个线程标记为完成，并合并到主程序线程
			videoThreadDone = true;
			
			try {
				join();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	private class AudioThread extends Thread {
		private boolean done = false;

		@Override
		public void run() {
			if (videoPlayback.containsAudioStream() == false) {
				return;
			}
			ICatchAudioFormat audioFormat = videoPlayback.getAudioFormat();
			int bufferSize = AudioTrack.getMinBufferSize(audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
					: AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT);

			audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
					: AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT,
					bufferSize, AudioTrack.MODE_STREAM);
			audioTrack.play();
			Log.d("1111", "audioTrack play");
			byte[] audioBuffer = new byte[1024 * 50];
			ICatchFrameBuffer icatchBuffer = new ICatchFrameBuffer();
			icatchBuffer.setBuffer(audioBuffer);
			boolean temp = false;
			while (!done) {
				temp = false;
				try {
					temp = videoPb.getNextAudioFrame(icatchBuffer);
				} catch (IchSocketException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchCameraModeException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchInvalidSessionException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchStreamNotRunningException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchBufferTooSmallException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchTryAgainException e) {
					// 
					// WriteLogToDevice.writeLog("[Error] -- AudioThread: ",
					// "videoPb.getNextAudioFrame IchTryAgainException");
					e.printStackTrace();
					continue;
				} catch (IchInvalidArgumentException e) {
					// 
					e.printStackTrace();
					return;
				} catch (IchAudioStreamClosedException e) {
					// 
					// WriteLogToDevice.writeLog("[Error] -- AudioThread: ",
					// "videoPb.getNextAudioFrame IchAudioStreamClosedException");
					e.printStackTrace();
				} catch (IchPbStreamPausedException e) {
					// 
					// WriteLogToDevice.writeLog("[Error] -- AudioThread: ",
					// "videoPb.getNextAudioFrame IchPbStreamPausedException");
					e.printStackTrace();
				}
				if (false == temp) {
					// WriteLogToDevice.writeLog("[Error] -- AudioThread: ",
					// "failed to getNextAudioFrame");
					continue;
				} else {
					// WriteLogToDevice.writeLog("[Normal] -- AudioThread: ",
					// "success to getNextAudioFrame");
				}
				if (icatchBuffer == null) {
					continue;
				}
				audioTrack.write(icatchBuffer.getBuffer(), 0, icatchBuffer.getFrameSize());
			}
			audioTrack.stop();
			audioTrack.release();
		}

		public void requestExitAndWait() {
			// 把这个线程标记为完成，并合并到主程序线程
			done = true;
			try {
				join();
			} catch (InterruptedException ex) {
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d("1111", "surfaceCreated");
		hasSurface = true;
		if (mySurfaceViewThread != null) {
			Log.d("1111", "surfaceCreated start mySurfaceViewThread");
			if (mySurfaceViewThread.isAlive() == false) {
				Log.d("1111", "surfaceCreated mySurfaceViewThread.isAlive() == false");
				mySurfaceViewThread.start();
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
		stop();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		Log.d("1111", " surfaceChanged");
		SurfaceHolder surfaceHolder = holder;
		if (videoFrameBitmap != null) {
			Canvas canvas = surfaceHolder.lockCanvas();
			drawFrameRect = ScaleTool.getScaledPosition(frameWidth, frameHeight, getWidth(), getHeight());
			canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
			surfaceHolder.unlockCanvasAndPost(canvas);
		}
	}

	public void destorySurface() {
		hasSurface = false;
	}

	public void startSurface(){
		hasSurface = true;
	}
	
	public interface VideoPbUpdateBarLitener {
		void updateBar(double pts);
	}

	public void addVideoPbUpdateBarLitener(VideoPbUpdateBarLitener videoPbUpdateBarLitener) {
		// h264ImageLitener.isShown();
		this.videoPbUpdateBarLitener = videoPbUpdateBarLitener;
	}

	public void reDrawBitmap() {
		SurfaceHolder surfaceHolder = holder;

		int lenght = videoFrameBitmap.getByteCount();

		if (videoFrameBitmap != null) {
			Canvas canvas = surfaceHolder.lockCanvas();
			System.out.println(surfaceHolder + "----->>>>>>>>-------" + canvas);
			drawFrameRect = ScaleTool.getScaledPosition(frameWidth, frameHeight, getWidth(), getHeight());
			if(canvas != null){
				canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
				surfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
	}
}
