package com.icatch.wcmapp3.SDKAPI;

import com.icatch.wcmapp3.global.sdk.SDKSession;
import com.icatch.wificam.customer.ICatchWificamAssist;
import com.icatch.wificam.customer.ICatchWificamControl;
import com.icatch.wificam.customer.ICatchWificamListener;
import com.icatch.wificam.customer.ICatchWificamSession;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchDeviceException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchListenerExistsException;
import com.icatch.wificam.customer.exception.IchListenerNotExistsException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStorageFormatException;
import com.icatch.wificam.customer.type.ICatchEventID;
import com.iotgroup.roame.operate.MyConstants;

public class CameraAction {
	private static CameraAction instance;
	private ICatchWificamControl cameraAction;
	public ICatchWificamAssist cameraAssist;

	public static CameraAction getInstance() {
		if (instance == null) {
			instance = new CameraAction();
		}
		return instance;
	}

	private CameraAction() {

	}

	public void initCameraAction() {
		cameraAction = MyConstants.getInstance().getCurrentCamera().getcameraActionClient();
		cameraAssist = MyConstants.getInstance().getCurrentCamera().getCameraAssistClint();
	}
	//用于本地vidoe播放对cameraAction的初始化;
	public void initCameraAction(ICatchWificamControl myWificamControl) {
		this.cameraAction = myWificamControl;
	}

	public boolean capturePhoto() {
		boolean ret = false;
		try {
			ret = cameraAction.capturePhoto();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public boolean triggerCapturePhoto() {
		boolean ret = false;
		try {
			ret = cameraAction.triggerCapturePhoto();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public boolean startMovieRecord() {
		boolean ret = false;

		try {
			ret = cameraAction.startMovieRecord();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public boolean startTimeLapse() {
		boolean ret = false;

		try {
			ret = cameraAction.startTimeLapse();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public boolean stopTimeLapse() {
		boolean ret = false;

		try {
			ret = cameraAction.stopTimeLapse();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public boolean stopVideoCapture() {
		boolean ret = false;

		try {
			ret = cameraAction.stopMovieRecord();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public boolean formatStorage() {
		boolean retVal = false;

		try {
			retVal = cameraAction.formatStorage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public boolean sleepCamera() {
		boolean retValue = false;
		try {
			try {
				retValue = cameraAction.toStandbyMode();
			} catch (IchDeviceException e) {
				e.printStackTrace();
			} catch (IchInvalidSessionException e) {
				e.printStackTrace();
			}
		} catch (IchSocketException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean addCustomEventListener(int eventID, ICatchWificamListener listener) {
		boolean retValue = false;
		try {
			retValue = cameraAction.addCustomEventListener(0x5001, listener);
		} catch (IchListenerExistsException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean delCustomEventListener(int eventID, ICatchWificamListener listener) {
		boolean retValue = false;
		try {
			retValue = cameraAction.delCustomEventListener(eventID, listener);
		} catch (IchListenerNotExistsException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}
	
	public boolean addEventListener(int eventID,ICatchWificamListener listener){
		boolean retValue = false;
		try {
			retValue = cameraAction.addEventListener(eventID, listener);
		} catch (IchListenerExistsException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean delEventListener(int eventID, ICatchWificamListener listener) {
		boolean retValue = false;
		try {
			retValue = cameraAction.delEventListener(eventID, listener);
		} catch (IchListenerNotExistsException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public static boolean addScanEventListener(ICatchWificamListener listener) {
		boolean retValue = false;
		try {
			retValue = ICatchWificamSession.addEventListener(ICatchEventID.ICATCH_EVENT_DEVICE_SCAN_ADD, listener);
		} catch (IchListenerExistsException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public static boolean delScanEventListener(ICatchWificamListener listener) {
		boolean retValue = false;
		try {
			//retValue = ICatchWificamSession.delEventListener(ICatchEventID.ICATCH_EVENT_DEVICE_SCAN_ADD, listener);
			retValue = ICatchWificamSession.delEventListener(ICatchEventID.ICATCH_EVENT_DEVICE_SCAN_ADD, listener);
		} catch (IchListenerNotExistsException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public String getCameraMacAddress() {
		String macAddress = "";
		macAddress = cameraAction.getMacAddress();
		return macAddress;
	}

	public boolean zoomIn() {
		boolean retValue = false;
		try {
			retValue = cameraAction.zoomIn();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchStorageFormatException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean zoomOut() {
		boolean retValue = false;
		try {
			retValue = cameraAction.zoomOut();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchStorageFormatException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean updateFW() {
		boolean ret = false;
		SDKSession mSDKSession = MyConstants.getInstance().getCurrentCamera().getSDKsession();
		try {
			ret = cameraAssist.updateFw(mSDKSession.getSDKSession(), MyConstants.UPDATEFW_FILENAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static boolean addStaicEventListener(int eventId, ICatchWificamListener listener) {
		boolean retValue = false;
		try {
			retValue = ICatchWificamSession.addEventListener(eventId, listener);
		} catch (IchListenerExistsException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public static boolean delStaicEventListener(int eventId, ICatchWificamListener listener) {
		boolean retValue = false;
		try {
			retValue = ICatchWificamSession.delEventListener(eventId, listener);
		} catch (IchListenerNotExistsException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean previewMove(int xshift, int yshfit) {
		boolean ret = false;
		ret = cameraAction.pan(xshift, yshfit);
		return ret;
		//return true;
	}
	
	public boolean resetPreviewMove() {
		boolean ret = false;
		ret = cameraAction.panReset();
		return ret;
		//return true;
	}
}
