package com.icatch.wcmapp3.SDKAPI;

import com.icatch.wificam.customer.ICatchWificamInfo;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.iotgroup.roame.operate.MyConstants;

public class CameraFixedInfo {
	private static CameraFixedInfo instance;
	private ICatchWificamInfo cameraFixedInfo;

	public static CameraFixedInfo getInstance() {
		if (instance == null) {
			instance = new CameraFixedInfo();
		}
		return instance;
	}

	private CameraFixedInfo() {

	}

	public void initCameraFixedInfo() {
		cameraFixedInfo = MyConstants.getInstance().getCurrentCamera().getCameraInfoClint();
	}

	public String getCameraName() {
		String name = "";
		try {
			name = cameraFixedInfo.getCameraProductName();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return name;
	}

	public String getCameraVersion() {
		String version = "";
		try {
			version = cameraFixedInfo.getCameraFWVersion();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return version;
	}

}
