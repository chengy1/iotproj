/**
 * Added by zhangyanhu C01012,2014-6-23
 */
package com.icatch.wcmapp3.SDKAPI;

import android.util.Log;

import com.icatch.wcmapp3.data.FwToApp;
import com.icatch.wcmapp3.global.App.PropertyId;
import com.icatch.wificam.customer.ICatchWificamControl;
import com.icatch.wificam.customer.ICatchWificamProperty;
import com.icatch.wificam.customer.ICatchWificamUtil;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchDevicePropException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchNoSDCardException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.type.ICatchCodec;
import com.icatch.wificam.customer.type.ICatchLightFrequency;
import com.icatch.wificam.customer.type.ICatchMode;
import com.icatch.wificam.customer.type.ICatchVideoFormat;
import com.icatch.wificam.customer.type.ICatchVideoSize;
import com.iotgroup.roame.operate.MyConstants;

import java.util.List;

public class CameraProperties {
	private List<Integer> fuction;
	// private PreviewStream previewStream = new PreviewStream();
	private List<ICatchMode> modeList;
	private static CameraProperties instance;
	private ICatchWificamProperty cameraConfiguration;
	private ICatchWificamControl cameraAction;

	public static CameraProperties getInstance() {
		if (instance == null) {
			instance = new CameraProperties();
		}
		return instance;
	}

	private CameraProperties() {

	}

	public void initCameraProperties() {
		cameraConfiguration = MyConstants.getInstance().getCurrentCamera().getCameraPropertyClint();
		cameraAction = MyConstants.getInstance().getCurrentCamera().getcameraActionClient();
	}

	public List<String> getSupportedImageSizes() {
		List<String> list = null;
		try {
			list = cameraConfiguration.getSupportedImageSizes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<String> getSupportedVideoSizes() {
		List<String> list = null;
		try {
			list = cameraConfiguration.getSupportedVideoSizes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<Integer> getSupportedWhiteBalances() {
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedWhiteBalances();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			
			e.printStackTrace();
		}
		return list;
	}

	public List<Integer> getSupportedCaptureDelays() {
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedCaptureDelays();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<Integer> getSupportedLightFrequencys() {
		List<Integer> list = null;

		try {
			list = cameraConfiguration.getSupportedLightFrequencies();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		// delete LIGHT_FREQUENCY_AUTO because UI don't need this option
		for (int ii = 0; ii < list.size(); ii++) {
			if (list.get(ii) == ICatchLightFrequency.ICH_LIGHT_FREQUENCY_AUTO) {
				list.remove(ii);
			}
		}
		return list;
	}

	public boolean setImageSize(String value) {
		boolean retVal = false;

		try {
			retVal = cameraConfiguration.setImageSize(value);
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public boolean setVideoSize(String value) {
		boolean retVal = false;

		try {
			retVal = cameraConfiguration.setVideoSize(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public boolean setWhiteBalance(int value) {
		boolean retVal = false;
		if (value < 0 || value == 0xff) {
			return false;
		}
		try {
			retVal = cameraConfiguration.setWhiteBalance(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public boolean setLightFrequency(int value) {
		boolean retVal = false;
		if (value < 0 || value == 0xff) {
			return false;
		}
		try {
			retVal = cameraConfiguration.setLightFrequency(value);
//			retVal = cameraConfiguration.setLightFreqency(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public String getCurrentImageSize() {
		String value = "unknown";

		try {
			value = cameraConfiguration.getCurrentImageSize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public String getCurrentVideoSize() {
		String value = "unknown";

		try {
			value = cameraConfiguration.getCurrentVideoSize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public int getCurrentWhiteBalance() {
		int value = 0xff;
		try {
			value = cameraConfiguration.getCurrentWhiteBalance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public int getCurrentLightFrequency() {
		int value = 0xff;
		try {
			value = cameraConfiguration.getCurrentLightFrequency();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public boolean setCaptureDelay(int value) {
		boolean retVal = false;

		try {
			retVal = cameraConfiguration.setCaptureDelay(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public int getCurrentCaptureDelay() {
		int retVal = 0;

		try {
			retVal = cameraConfiguration.getCurrentCaptureDelay();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public int getCurrentDateStamp() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentDateStamp();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setDateStamp(int dateStamp) {
		Boolean retValue = false;
		try {
			retValue = cameraConfiguration.setDateStamp(dateStamp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public List<Integer> getDateStampList() {
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedDateStamps();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<Integer> getSupportFuction() {
		List<Integer> fuction = null;
		// List<Integer> temp = null;
		try {
			fuction = cameraConfiguration.getSupportedProperties();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return fuction;
	}

	public int getCurrentBurstNum() {
		int number = 0xff;
		try {
			number = cameraConfiguration.getCurrentBurstNumber();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return number;
	}

	public int getCurrentAppBurstNum() {
		int number = 0xff;
		try {
			number = cameraConfiguration.getCurrentBurstNumber();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		number = FwToApp.getInstance().getAppBurstNum(number);
		return number;
	}

	public boolean setCurrentBurst(int burstNum) {
		if (burstNum < 0 || burstNum == 0xff) {
			return false;
		}
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setBurstNumber(burstNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getRemainImageNum() {
		int num = 0;
		try {
			num = cameraAction.getFreeSpaceInImages();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		} catch (IchNoSDCardException e) {
			e.printStackTrace();
		}
		return num;
	}

	public int getRecordingRemainTime() {
		int recordingTime = 0;

		try {
			recordingTime = cameraAction.getRemainRecordingTime();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		} catch (IchNoSDCardException e) {
			e.printStackTrace();
		}
		return recordingTime;
	}

	public boolean isSDCardExist() {
		Boolean isReady = false;
		// JIRA ICOM-1577 Begin:Modify by b.jiang C01063 2015-07-17

		// try {
		// isReady = cameraAction.isSDCardExist();
		// } catch (IchSocketException e) {
		// WriteLogToDevice.writeLog("[Error] -- CameraProperties: ",
		// "IchSocketException");
		// 
		// e.printStackTrace();
		// } catch (IchCameraModeException e) {
		// WriteLogToDevice.writeLog("[Error] -- CameraProperties: ",
		// "IchCameraModeException");
		// 
		// e.printStackTrace();
		// } catch (IchInvalidSessionException e) {
		// WriteLogToDevice.writeLog("[Error] -- CameraProperties: ",
		// "IchInvalidSessionException");
		// 
		// e.printStackTrace();
		// }
		// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
		// "end isSDCardExist isReady =" + isReady);
		// return isReady;

		return MyConstants.isSdCardExist;
	}

	public int getBatteryElectric() {
		int electric = 0;
		try {
			electric = cameraAction.getCurrentBatteryLevel();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return electric;
	}

	public boolean supportVideoPlayback() {
		boolean retValue = false;
		try {
			retValue = cameraAction.supportVideoPlayback();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
		// return false;
	}

	public boolean cameraModeSupport(ICatchMode mode) {
		Boolean retValue = false;
		if (modeList == null) {
			modeList = getSupportedModes();
		}
		if (modeList.contains(mode)) {
			retValue = true;
		}
		return retValue;
	}

	public String getCameraMacAddress() {
		String macAddress = cameraAction.getMacAddress();
		return macAddress;
	}

	public List<Integer> getConvertSupportedImageSizes() {
		List<String> list = getSupportedImageSizes();
		List<Integer> convertImageSizeList = null;
		// ICatchWificamUtil sizeAss = new ICatchWificamUtil();
		try {
			convertImageSizeList = ICatchWificamUtil.convertImageSizes(list);
		} catch (IchInvalidArgumentException e) {
			e.printStackTrace();
		}
		return convertImageSizeList;
	}

	public List<ICatchVideoSize> getConvertSupportedVideoSizes() {
		List<String> list = getSupportedVideoSizes();
		List<ICatchVideoSize> convertVideoSizeList = null;
		// ICatchWificamUtil sizeAss = new ICatchWificamUtil();
		try {
			convertVideoSizeList = ICatchWificamUtil.convertVideoSizes(list);
		} catch (IchInvalidArgumentException e) {
			e.printStackTrace();
		}
		return convertVideoSizeList;
	}

	public boolean hasFuction(int fuc) {
		if (fuction == null) {
			fuction = getSupportFuction();
		}
		Boolean retValue = false;
		if (fuction.contains(fuc) == true) {
			retValue = true;
		}
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-4
	 */
	public List<Integer> getsupportedDateStamps() {
		List<Integer> list = null;

		try {
			list = cameraConfiguration.getSupportedDateStamps();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<Integer> getsupportedBurstNums() {
		List<Integer> list = null;

		try {
			list = cameraConfiguration.getSupportedBurstNumbers();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-4
	 */
	public List<Integer> getSupportedFrequencies() {
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedLightFrequencies();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<ICatchMode> getSupportedModes() {
		List<ICatchMode> list = null;
		try {
			if(cameraAction != null){
				list = cameraAction.getSupportedModes();
			}
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return list;
	}

	// public List<Integer> getSupportedTimeLapseStillDurations() {
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "begin getSupportedTimeLapseStillDurations");
	// List<Integer> list = null;
	// //boolean retValue = false;
	// try {
	// list = cameraConfiguration.getSupportedTimeLapseStillDurations();
	// } catch (IchSocketException e) {
	// 
	// e.printStackTrace();
	// } catch (IchCameraModeException e) {
	// 
	// e.printStackTrace();
	// } catch (IchDevicePropException e) {
	// 
	// e.printStackTrace();
	// } catch (IchInvalidSessionException e) {
	// 
	// e.printStackTrace();
	// }
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "end getSupportedTimeLapseStillDurations list =" + list.size());
	// return list;
	// }
	//
	// public List<Integer> getSupportedTimeLapseStillintervals() {
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "begin getSupportedTimeLapseStillintervals");
	// List<Integer> list = null;
	// //boolean retValue = false;
	// try {
	// list = cameraConfiguration.getSupportedTimeLapseStillIntervals();
	// } catch (IchSocketException e) {
	// 
	// e.printStackTrace();
	// } catch (IchCameraModeException e) {
	// 
	// e.printStackTrace();
	// } catch (IchDevicePropException e) {
	// 
	// e.printStackTrace();
	// } catch (IchInvalidSessionException e) {
	// 
	// e.printStackTrace();
	// }
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "end getSupportedTimeLapseStillintervals list =" + list.size());
	// return list;
	// }

	public List<Integer> getSupportedTimeLapseDurations() {
		List<Integer> list = null;
		// boolean retValue = false;
		try {
			list = cameraConfiguration.getSupportedTimeLapseDurations();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return list;
	}

	public List<Integer> getSupportedTimeLapseIntervals() {
		List<Integer> list = null;
		// boolean retValue = false;
		try {
			list = cameraConfiguration.getSupportedTimeLapseIntervals();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return list;
	}

	public boolean setTimeLapseDuration(int timeDuration) {
		boolean retVal = false;
//		if (timeDuration < 0 || timeDuration == 0xff) {
//			return false;
//		}
		try {
			retVal = cameraConfiguration.setTimeLapseDuration(timeDuration);
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public int getCurrentTimeLapseDuration() {
		int retVal = 0xff;
		try {
			retVal = cameraConfiguration.getCurrentTimeLapseDuration();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public boolean setTimeLapseInterval(int timeInterval) {
		boolean retVal = false;
		//JIRA ICOM-1764 Start modify by b.jiang 20160126
//		if (timeInterval < 0 || timeInterval == 0xff) {
//			return false;
//		}
		//JIRA ICOM-1764 Start modify by b.jiang 20160126
		try {
			retVal = cameraConfiguration.setTimeLapseInterval(timeInterval);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public int getCurrentTimeLapseInterval() {
		int retVal = 0xff;
		try {
			retVal = cameraConfiguration.getCurrentTimeLapseInterval();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public int getMaxZoomRatio() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getMaxZoomRatio();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getCurrentZoomRatio() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentZoomRatio();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getCurrentUpsideDown() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentUpsideDown();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setUpsideDown(int upside) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setUpsideDown(upside);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getCurrentSlowMotion() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentSlowMotion();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setSlowMotion(int slowMotion) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setSlowMotion(slowMotion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setCameraDate(String date) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_DATE, date);
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setCameraEssidName(String ssidName) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.ESSID_NAME, ssidName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCameraEssidName() {
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.ESSID_NAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCameraEssidPassword() {
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.ESSID_PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setCameraEssidPassword(String ssidPassword) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.ESSID_PASSWORD, ssidPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setCameraSsid(String ssid) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_ESSID, ssid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setCameraName(String cameraName) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_NAME, cameraName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCameraName() {
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_NAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCameraName(ICatchWificamProperty cameraConfiguration1) {
		String retValue = null;
		try {
			retValue = cameraConfiguration1.getCurrentStringPropertyValue(PropertyId.CAMERA_NAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCameraPasswordNew() {
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_PASSWORD_NEW);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setCameraPasswordNew(String cameraNamePassword) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_PASSWORD_NEW, cameraNamePassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCameraSsid() {
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_ESSID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public void SetCameraPowerOff()
	{
		try {
			 cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_POWEROFF, "1");
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public boolean setCameraPassword(String password) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_PASSWORD, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCameraPassword() {
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setCaptureDelayMode(int value) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(PropertyId.CAPTURE_DELAY_MODE, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}
	
	public int getCurrentCaptureDelayMode() {
		int retValue = -1;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.CAPTURE_DELAY_MODE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getVideoRecordingTime() {
		int retValue = 0;
		try {
			// 0xD7F0 -> PropertyId.VIDEO_RECORDING_TIME
			// retValue = cameraConfiguration.getCurrentPropertyValue(0xD7F0);
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.VIDEO_RECORDING_TIME);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setServiceEssid(String value) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.SERVICE_ESSID, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setServicePassword(String value) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.SERVICE_PASSWORD, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean notifyFwToShareMode(int value) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(PropertyId.NOTIFY_FW_TO_SHARE_MODE, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public List<Integer> getSupportedPropertyValues(int propertyId) {
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedPropertyValues(propertyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public int getCurrentPropertyValue(int propertyId) {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(propertyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public String getCurrentStringPropertyValue(int propertyId) {
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(propertyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}
	
	public List<String> getSupportedStringPropertyValues(int propertyId){
		List<String> list = null;
		try {
			list = cameraConfiguration.getSupportedStringPropertyValues(propertyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public boolean setPropertyValue(int propertyId, int value) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(propertyId, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean setStringPropertyValue(int propertyId, String value) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(propertyId, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getVideoSizeFlow() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.VIDEO_SIZE_FLOW);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean notifyCameraConnectChnage(int value) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(PropertyId.CAMERA_CONNECT_CHANGE, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public List<ICatchVideoFormat> getResolutionList() {
		List<ICatchVideoFormat> retList = null;
		try {
			retList = cameraConfiguration.getSupportedStreamingInfos();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retList;
	}

	public String getBestResolution() {
		String bestResolution = null;

		List<ICatchVideoFormat> tempList = getResolutionList(cameraConfiguration);
		if (tempList == null || tempList.size() == 0) {
			return null;
		}
		Log.d("1111", "getResolutionList() tempList.size() = " + tempList.size());
		int tempWidth = 0;
		int tempHeigth = 0;

		ICatchVideoFormat temp;

		for (int ii = 0; ii < tempList.size(); ii++) {
			temp = tempList.get(ii);
			if (temp.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				if (bestResolution == null) {
					bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
				}

				if (temp.getVideoW() == 640 && temp.getVideoH() == 360) {
					bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
					return bestResolution;
				} else if (temp.getVideoW() == 640 && temp.getVideoH() == 480) {
					if (tempWidth != 640) {
						bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = 640;
						tempHeigth = 480;
					}
				} else if (temp.getVideoW() == 720) {
					if (tempWidth != 640) {
						if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
						{
							bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
						{
							if (tempWidth != 720)
								bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
										+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						}
					}
				} else if (temp.getVideoW() < tempWidth) {
					if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
					{
						bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
					{
						if (tempWidth != temp.getVideoW())
							bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					}
				}
			}
		}
		if (bestResolution != null) {
			return bestResolution;
		}
		for (int ii = 0; ii < tempList.size(); ii++) {
			temp = tempList.get(ii);
			if (temp.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				if (bestResolution == null) {
					bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
				}

				if (temp.getVideoW() == 640 && temp.getVideoH() == 360) {
					bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
					return bestResolution;
				} else if (temp.getVideoW() == 640 && temp.getVideoH() == 480) {
					if (tempWidth != 640) {
						bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = 640;
						tempHeigth = 480;
					}
				} else if (temp.getVideoW() == 720) {
					if (tempWidth != 640) {
						if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
						{
							bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
						{
							if (tempWidth != 720)
								bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
										+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						}
					}
				} else if (temp.getVideoW() < tempWidth) {
					if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
					{
						bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
					{
						if (tempWidth != temp.getVideoW())
							bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					}
				}
			}
		}

		return bestResolution;

	}

	public List<ICatchVideoFormat> getResolutionList(ICatchWificamProperty cameraConfiguration) {
		List<ICatchVideoFormat> retList = null;
		try {
			retList = cameraConfiguration.getSupportedStreamingInfos();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retList;
	}

	public String getAppDefaultResolution() {
		String appDefaultResolution = null;

		List<ICatchVideoFormat> tempList = getResolutionList(cameraConfiguration);
		if (tempList == null || tempList.size() == 0) {
			return null;
		}

		ICatchVideoFormat temp;

		for (int ii = 0; ii < tempList.size(); ii++) {
			temp = tempList.get(ii);

			if (temp.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				if (temp.getVideoW() == 1280 && temp.getVideoH() == 720 && temp.getBitrate() == 500000) {
					appDefaultResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
							+ "&";
					return appDefaultResolution;
				}
			}
		}

		return appDefaultResolution;

	}

	public String getFWDefaultResolution() {
		String resolution = null;
		ICatchVideoFormat retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStreamingInfo();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (retValue != null) {
			if (retValue.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				resolution = "H264?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			} else if (retValue.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				resolution = "MJPG?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			}
		}
		return resolution;

	}

	public boolean setStreamingInfo(ICatchVideoFormat iCatchVideoFormat) {
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStreamingInfo(iCatchVideoFormat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;

	}

	public String getCurrentStreamInfo() {
		ICatchVideoFormat retValue = null;
		String bestResolution = null;
		try {
			retValue = cameraConfiguration.getCurrentStreamingInfo();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			e.printStackTrace();
		}
		if (retValue == null) {
			return null;
		}
		//JIRA ICOM-2520 Start add by b.jiang 2015-12-23
		//判断是否支援property code 0XD7AE，如果支援送带FPS的，不支援就不送带fps的
		if (hasFuction(0xd7ae)) {
			if (retValue.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				bestResolution = "H264?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			} else if (retValue.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				bestResolution = "MJPG?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			}
		} else {
			if (retValue.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				bestResolution = "H264?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate();
			} else if (retValue.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				bestResolution = "MJPG?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate();
			}
		}
		return bestResolution;
	}

	public int getPreviewCacheTime() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getPreviewCacheTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getCameraTimeLapseVideoSizeListMask() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK);
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

}
