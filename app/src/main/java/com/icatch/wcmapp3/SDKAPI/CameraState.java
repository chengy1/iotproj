/**
 * Added by zhangyanhu C01012,2014-7-2
 */
package com.icatch.wcmapp3.SDKAPI;

import com.icatch.wificam.customer.ICatchWificamState;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.iotgroup.roame.operate.MyConstants;

/**
 * Added by zhangyanhu C01012,2014-7-2
 */
public class CameraState {
	private static CameraState instance;
	private ICatchWificamState cameraState;

	public static CameraState getInstance() {
		if (instance == null) {
			instance = new CameraState();
		}
		return instance;
	}

	private CameraState() {

	}

	public void initCameraState() {
		cameraState = MyConstants.getInstance().getCurrentCamera().getCameraStateClint();
	}

	public boolean isMovieRecording() {
		boolean retValue = false;
		try {
			retValue = cameraState.isMovieRecording();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean isTimeLapseVideoOn() {
		boolean retValue = false;
		try {
			retValue = cameraState.isTimeLapseVideoOn();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean isTimeLapseStillOn() {
		boolean retValue = false;
		try {
			retValue = cameraState.isTimeLapseStillOn();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean isSupportImageAutoDownload() {
		boolean retValue = false;
		try {
			retValue = cameraState.supportImageAutoDownload();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return retValue;

	}

	public boolean isStreaming() {
		boolean retValue = false;
		try {
			retValue = cameraState.isStreaming();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}

		return retValue;
	}
}
