/**
 * Added by zhangyanhu C01012,2014-6-27
 */
package com.icatch.wcmapp3.SDKAPI;

import com.icatch.wificam.customer.ICatchWificamPlayback;
import com.icatch.wificam.customer.type.ICatchFile;
import com.icatch.wificam.customer.type.ICatchFileType;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.iotgroup.roame.operate.MyConstants;

import java.util.List;

/**
 * Added by zhangyanhu C01012,2014-6-27
 */
public class FileOperation {
	private static FileOperation instance;
	private ICatchWificamPlayback cameraPlayback;

	public static FileOperation getInstance() {
		if (instance == null) {
			instance = new FileOperation();
		}
		return instance;
	}

	private FileOperation() {

	}

	public void initICatchWificamPlayback() {
		cameraPlayback = MyConstants.getInstance().getCurrentCamera().getplaybackClient();
	}
	
	//���ڱ���video����ʱ��FileOperation�ĳ�ʼ��;
	public void initFileOperation(ICatchWificamPlayback playback) {
		this.cameraPlayback = playback;
	}

	public boolean cancelDownload() {
		if(cameraPlayback == null){
			return true;
		}
		boolean retValue = false;
		try {
			retValue = cameraPlayback.cancelFileDownload();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public List<ICatchFile> getFileList(ICatchFileType type) {
		List<ICatchFile> list = null;
		try {
			list = cameraPlayback.listFiles(type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public boolean deleteFile(ICatchFile file) {
		boolean retValue = false;
		try {
			retValue = cameraPlayback.deleteFile(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean downloadFile(ICatchFile file, String path) {
		boolean retValue = false;
		try {
			retValue = cameraPlayback.downloadFile(file, path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public ICatchFrameBuffer downloadFile(ICatchFile curFile) {
		ICatchFrameBuffer buffer = null;
		try {
			buffer = cameraPlayback.downloadFile(curFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return buffer;
	}

	public ICatchFrameBuffer getQuickview(ICatchFile curFile) {
		ICatchFrameBuffer buffer = null;
		try {
			buffer = cameraPlayback.getQuickview(curFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return buffer;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public ICatchFrameBuffer getThumbnail(ICatchFile file) {
		ICatchFrameBuffer frameBuffer = null;
		try {
			frameBuffer = cameraPlayback.getThumbnail(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return frameBuffer;
	}
	
	public ICatchFrameBuffer getThumbnail(String filePath) {
		ICatchFile icathfile = new ICatchFile(33, ICatchFileType.ICH_TYPE_VIDEO, filePath,"", 0);
		
		
		ICatchFrameBuffer frameBuffer = null;
		try {
			frameBuffer = cameraPlayback.getThumbnail(icathfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return frameBuffer;
	}
	
	public ICatchFrameBuffer getThumbnail(ICatchWificamPlayback wificamPlayback,String filePath) {
		ICatchFile icathfile = new ICatchFile(33, ICatchFileType.ICH_TYPE_VIDEO, filePath,"", 0);
		ICatchFrameBuffer frameBuffer = null;
		try {
			frameBuffer = wificamPlayback.getThumbnail(icathfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return frameBuffer;
	}

}
