/**
 * Added by zhangyanhu C01012,2014-6-27
 */
package com.icatch.wcmapp3.SDKAPI;
import com.icatch.wcmapp3.baseItems.Tristate;
import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStreamNotRunningException;
import com.icatch.wificam.customer.exception.IchStreamNotSupportException;
import com.icatch.wificam.customer.type.ICatchAudioFormat;
import com.icatch.wificam.customer.type.ICatchCustomerStreamParam;
import com.icatch.wificam.customer.type.ICatchFileStreamParam;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchH264StreamParam;
import com.icatch.wificam.customer.type.ICatchMJPGStreamParam;
import com.icatch.wificam.customer.type.ICatchPreviewMode;
import com.icatch.wificam.customer.type.ICatchVideoFormat;
import com.iotgroup.roame.operate.MyConstants;

public class PreviewStream {
	private static PreviewStream instance;

	// private ICatchWificamPreview previewStreamControl;
	public static PreviewStream getInstance() {
		if (instance == null) {
			instance = new PreviewStream();
		}
		return instance;
	}

	//
	// private PreviewStream(){
	//
	// }
	//
	// public void initPreviewStream(){
	// previewStreamControl =
	// GlobalInfo.getInstance().getCurrentCamera().getpreviewStreamClient();
	// }

	public boolean stopMediaStream(ICatchWificamPreview previewStreamControl) {
		boolean retValue = false;
		try {
			retValue = previewStreamControl.stop();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean supportAudio(ICatchWificamPreview previewStreamControl) {
		boolean retValue = false;
		try {
			retValue = previewStreamControl.containsAudioStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// "end containsAudioStream retValue =" + retValue);
		// //retValue = false;
		// retValue = retValue & (!GlobalInfo.forbidAudioOutput);
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public boolean getNextVideoFrame(ICatchFrameBuffer buffer, ICatchWificamPreview previewStreamControl) {
		boolean retValue = false;
		try {
			retValue = previewStreamControl.getNextVideoFrame(buffer);
		} catch (Exception e) {
			// need to close preview get next video frame
			e.printStackTrace();
		}
		return retValue;
	}

	
	public boolean getNextAudioFrame(ICatchWificamPreview previewStreamControl, ICatchFrameBuffer icatchBuffer) {
		boolean retValue = false;
		try {
			retValue = previewStreamControl.getNextAudioFrame(icatchBuffer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public Tristate startMediaStream(ICatchWificamPreview previewStreamControl, ICatchMJPGStreamParam param, ICatchPreviewMode previewMode) {
		boolean temp = false;
		Tristate retValue = Tristate.FALSE;
		try {
			temp = previewStreamControl.start(param, previewMode, MyConstants.forbidAudioOutput);
			if(temp){
				retValue = Tristate.NORMAL;
			}else{
				retValue = Tristate.FALSE;
			}
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			retValue = Tristate.ABNORMAL;
			e.printStackTrace();
		}
		return retValue;
	}

	public Tristate startMediaStream(ICatchWificamPreview previewStreamControl, ICatchCustomerStreamParam param, ICatchPreviewMode previewMode) {
		boolean temp = false;
		Tristate retValue = Tristate.FALSE;
		try {
			temp = previewStreamControl.start(param, previewMode, MyConstants.forbidAudioOutput);
			if(temp){
				retValue = Tristate.NORMAL;
			}else{
				retValue = Tristate.FALSE;
			}
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			retValue = Tristate.ABNORMAL;
			e.printStackTrace();
		}
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public Tristate startMediaStream(ICatchWificamPreview previewStreamControl, ICatchH264StreamParam param, ICatchPreviewMode previewMode) {
		boolean temp = false;
		Tristate retValue = Tristate.FALSE;
		try {
			temp = previewStreamControl.start(param, previewMode, MyConstants.forbidAudioOutput);
			if(temp){
				retValue = Tristate.NORMAL;
			}else{
				retValue = Tristate.FALSE;
			}
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			retValue = Tristate.ABNORMAL;
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean changePreviewMode(ICatchWificamPreview previewStreamControl, ICatchPreviewMode previewMode) {
		boolean retValue = false;
		try {
			retValue = previewStreamControl.changePreviewMode(previewMode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;

	}

	public int getVideoWidth(ICatchWificamPreview previewStreamControl) {
		int retValue = 0;

		try {
			retValue = previewStreamControl.getVideoFormat().getVideoW();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getVideoHeigth(ICatchWificamPreview previewStreamControl) {
		int retValue = 0;

		try {
			retValue = previewStreamControl.getVideoFormat().getVideoH();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getCodec(ICatchWificamPreview previewStreamControl) {
		int retValue = 0;

		try {
			ICatchVideoFormat videoFormat = previewStreamControl.getVideoFormat();
			if(videoFormat != null){
//				retValue = previewStreamControl.getVideoFormat().getCodec();
				retValue = videoFormat.getCodec();
			}
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public int getBitrate(ICatchWificamPreview previewStreamControl) {
		int retValue = 0;

		try {
			retValue = previewStreamControl.getVideoFormat().getBitrate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public ICatchAudioFormat getAudioFormat(ICatchWificamPreview previewStreamControl) {
		ICatchAudioFormat retValue = null;

		try {
			retValue = previewStreamControl.getAudioFormat();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean startMediaStream(ICatchWificamPreview previewStreamControl, ICatchFileStreamParam param, ICatchPreviewMode previewMode) {
		boolean temp = false;
		try {
			temp = previewStreamControl.start(param, previewMode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}
	
	
	public boolean enableAudio(ICatchWificamPreview previewStreamControl){
		boolean value = false;
		try {
			value = previewStreamControl.enableAudio();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}
		public boolean disableAudio(ICatchWificamPreview previewStreamControl){
			boolean value = false;
			try {
				value = previewStreamControl.disableAudio();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return value;
		}
}
