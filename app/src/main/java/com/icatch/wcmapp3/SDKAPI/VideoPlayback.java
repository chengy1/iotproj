/**
 * Added by zhangyanhu C01012,2014-6-27
 */
package com.icatch.wcmapp3.SDKAPI;

import com.icatch.wificam.customer.ICatchWificamVideoPlayback;
import com.icatch.wificam.customer.exception.IchAudioStreamClosedException;
import com.icatch.wificam.customer.exception.IchBufferTooSmallException;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchNoSuchFileException;
import com.icatch.wificam.customer.exception.IchPauseFailedException;
import com.icatch.wificam.customer.exception.IchPbStreamPausedException;
import com.icatch.wificam.customer.exception.IchResumeFailedException;
import com.icatch.wificam.customer.exception.IchSeekFailedException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStreamNotRunningException;
import com.icatch.wificam.customer.exception.IchTryAgainException;
import com.icatch.wificam.customer.exception.IchVideoStreamClosedException;
import com.icatch.wificam.customer.type.ICatchAudioFormat;
import com.icatch.wificam.customer.type.ICatchFile;
import com.icatch.wificam.customer.type.ICatchFileType;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchVideoFormat;
import com.iotgroup.roame.operate.MyConstants;

public class VideoPlayback {
	private static VideoPlayback instance;
	private ICatchWificamVideoPlayback videoPlayback;

	public static VideoPlayback getInstance() {
		if (instance == null) {
			instance = new VideoPlayback();
		}
		return instance;
	}

	private VideoPlayback() {

	}

	public void initVideoPlayback() {
		videoPlayback = MyConstants.getInstance().getCurrentCamera().getVideoPlaybackClint();
	}
	//用于本地video播放时对vidoePlayback的初始化;
	public void initVideoPlayback(ICatchWificamVideoPlayback videoPlayback) {
		this.videoPlayback = videoPlayback;
	}

	public boolean stopPlaybackStream() {
		if (videoPlayback == null) {
			return true;
		}
		boolean retValue = false;
		try {
			retValue = videoPlayback.stop();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean startPlaybackStream(ICatchFile file) {
		boolean retValue = false;
		try {
			retValue = videoPlayback.play(file);
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchNoSuchFileException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}
	
	public boolean startPlaybackStream(String fileName) {
		boolean retValue = false;
		ICatchFile icathfile = new ICatchFile(33, ICatchFileType.ICH_TYPE_VIDEO, fileName,"", 0);
		try {
			retValue = videoPlayback.play(icathfile, false, false);
//			retValue = videoPlayback.play(icathfile);
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchNoSuchFileException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean pausePlayback() {
		boolean retValue = false;
		try {
			retValue = videoPlayback.pause();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchPauseFailedException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	/**
	 * 
	 * Added by zhangyanhu C01012,2014-3-20
	 */
	public boolean resumePlayback() {
		boolean retValue = false;
		try {
			retValue = videoPlayback.resume();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchResumeFailedException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public int getVideoDuration() {
		double temp = 0;
		try {
			temp = videoPlayback.getLength();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		}
		return (new Double(temp * 100).intValue());
	}

	public boolean videoSeek(double position) {
		boolean retValue = false;
		try {
			retValue = videoPlayback.seek(position);
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchSeekFailedException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public boolean getNextVideoFrame(ICatchFrameBuffer buffer) {
		boolean retValue = false;
		try {
			retValue = videoPlayback.getNextVideoFrame(buffer);
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		} catch (IchBufferTooSmallException e) {
			
			e.printStackTrace();
		} catch (IchTryAgainException e) {
			
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			
			e.printStackTrace();
		} catch (IchVideoStreamClosedException e) {
			
			e.printStackTrace();
		} catch (IchPbStreamPausedException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean getNextAudioFrame(ICatchFrameBuffer buffer) {
		boolean retValue = false;
		try {
			retValue = videoPlayback.getNextAudioFrame(buffer);
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		} catch (IchBufferTooSmallException e) {
			
			e.printStackTrace();
		} catch (IchTryAgainException e) {
			
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			
			e.printStackTrace();
		} catch (IchAudioStreamClosedException e) {
			
			e.printStackTrace();
		} catch (IchPbStreamPausedException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public boolean containsAudioStream() {
		boolean retValue = false;
		try {
			retValue = videoPlayback.containsAudioStream();
		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public ICatchAudioFormat getAudioFormat() {
		ICatchAudioFormat retValue = null;
		try {
			retValue = videoPlayback.getAudioFormat();

		} catch (IchSocketException e) {
			
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			
			e.printStackTrace();
		}
		return retValue;
	}

	public ICatchVideoFormat getVideoFormat() {
		ICatchVideoFormat retValue = null;
		try {
			retValue = videoPlayback.getVideoFormat();
		} catch (IchSocketException e) {
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			e.printStackTrace();
		}

		return retValue;
	}

}
