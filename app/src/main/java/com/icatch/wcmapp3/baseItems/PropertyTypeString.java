package com.icatch.wcmapp3.baseItems;

import android.content.Context;

import com.icatch.wcmapp3.SDKAPI.CameraProperties;
import com.icatch.wcmapp3.global.App.PropertyId;
import com.icatch.wcmapp3.hash.PropertyHashMapDynamic;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class PropertyTypeString {

	private int propertyId;
	private List<String> valueListString;
	private List<String> valueListStringUI;
	private HashMap<String, ItemInfo> hashMap;
	private String[] valueArrayString;

	public PropertyTypeString(int propertyId, Context context) {
		this.propertyId = propertyId;
		initItem();
	}
	private Boolean checkWithTimeLapseMask(int TimeLapseMaskValue, int index) {
		int intShiftValue = 0x1;
		int intMatchValue = 0 ;  
		if ( index > 0) {
			intShiftValue = intShiftValue << index;			
		}  
		intMatchValue = TimeLapseMaskValue & intShiftValue; 
		if (intMatchValue > 0) {			
			return true;
		} else {
			return false;
		}		
	}

	public void initItem() {
		if (hashMap == null) {
			if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
				hashMap = PropertyHashMapDynamic.getInstance().getDynamicHashString(PropertyId.VIDEO_SIZE);
			} else {
				hashMap = PropertyHashMapDynamic.getInstance().getDynamicHashString(propertyId);	
			} 
			
			//hashMap = PropertyHashMapDynamic.getInstance().getDynamicHashString(propertyId);
		}
		if (propertyId == PropertyId.IMAGE_SIZE) {
			valueListString = CameraProperties.getInstance().getSupportedImageSizes();
		}
		if (propertyId == PropertyId.VIDEO_SIZE) {
			valueListString = CameraProperties.getInstance().getSupportedVideoSizes();
		}
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			valueListString = CameraProperties.getInstance().getSupportedVideoSizes();
			// Check support property : TIMELAPSE_VIDEO_SIZE_LIST_MASK
			int intTimeLapseVideoSizeListMask = 0;
			if (CameraProperties.getInstance().hasFuction(PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) == true) {
				intTimeLapseVideoSizeListMask = CameraProperties.getInstance().getCameraTimeLapseVideoSizeListMask();
				for (int ii = 0; ii < valueListString.size(); ii++) {
					if (checkWithTimeLapseMask(intTimeLapseVideoSizeListMask, ii) == false) {
						valueListString.remove(ii);
						ii--;
						intTimeLapseVideoSizeListMask = intTimeLapseVideoSizeListMask >> 1 ;
					} 
				}		
			}			
		}
		
		for (int ii = 0; ii < valueListString.size(); ii++) {
			if (hashMap.containsKey(valueListString.get(ii)) == false) {
				valueListString.remove(ii);
				ii--;
			}
		}
		valueListStringUI = new LinkedList<String>();
		valueArrayString = new String[valueListString.size()];
		if (valueListString != null) {
			for (int ii = 0; ii < valueListString.size(); ii++) {
				valueListStringUI.add(ii, hashMap.get(valueListString.get(ii)).uiStringInSettingString);
				valueArrayString[ii] = hashMap.get(valueListString.get(ii)).uiStringInSettingString;
			}
		}

	}

	public String getCurrentValue() {
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			return CameraProperties.getInstance().getCurrentStringPropertyValue( PropertyId.VIDEO_SIZE);
		} else { 
			return CameraProperties.getInstance().getCurrentStringPropertyValue( propertyId);
		}
		//return CameraProperties.getInstance().getCurrentStringPropertyValue(propertyId);
	}

	public String getCurrentUiStringInSetting() {
		ItemInfo itemInfo = hashMap.get(getCurrentValue());
		String ret = null;
		if (itemInfo == null) {
			ret = "Unknown";
		} else {
			ret = itemInfo.uiStringInSettingString;
		}
		return ret;
	}

	public String getCurrentUiStringInPreview() {
		ItemInfo itemInfo = hashMap.get(getCurrentValue());
		String ret = null;
		if (itemInfo == null) {
			ret = "Unknown";
		} else {
			ret = itemInfo.uiStringInPreview;
		}
		return ret;
	}

	public String getCurrentUiStringInSetting(int position) {
		return valueListString.get(position);
	}

	public List<String> getValueList() {
		return valueListString;
	}

	public List<String> getValueListUI() {
		return valueListString;
	}

	public Boolean setValue(String value) {
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			return CameraProperties.getInstance().setStringPropertyValue( PropertyId.VIDEO_SIZE, value);
		} else { 
			return CameraProperties.getInstance().setStringPropertyValue( propertyId, value);	
		}
		//return CameraProperties.getInstance().setStringPropertyValue( propertyId, value);
	}

	public boolean setValueByPosition(int position) {
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			return CameraProperties.getInstance().setStringPropertyValue( PropertyId.VIDEO_SIZE,
					valueListString.get(position));
		} else {
			return CameraProperties.getInstance().setStringPropertyValue( propertyId,
					valueListString.get(position));	
		}
		//return CameraProperties.getInstance().setStringPropertyValue( propertyId,valueListString.get(position));
	}

	public String[] getValueArrayString() {
		return valueArrayString;
	}
}
