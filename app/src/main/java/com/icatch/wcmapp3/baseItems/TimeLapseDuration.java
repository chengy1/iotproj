/**
 * Added by zhangyanhu C01012,2014-8-29
 */
package com.icatch.wcmapp3.baseItems;

import android.content.Context;

import com.icatch.wcmapp3.SDKAPI.CameraProperties;
import com.icatch.wificam.customer.type.ICatchMode;

import java.util.ArrayList;
import java.util.List;

public class TimeLapseDuration {
	public static final int TIME_LAPSE_DURATION_2MIN = 2;
	public static final int TIME_LAPSE_DURATION_5MIN = 5;
	public static final int TIME_LAPSE_DURATION_10MIN = 10;
	public static final int TIME_LAPSE_DURATION_15MIN = 15;
	public static final int TIME_LAPSE_DURATION_20MIN = 20;
	public static final int TIME_LAPSE_DURATION_30MIN = 30;
	public static final int TIME_LAPSE_DURATION_60MIN = 60;
	public static final int TIME_LAPSE_DURATION_UNLIMITED = 0xffff;
	
	private String[] valueListString;
	private int[] valueListInt;
	private Context mContext;

	public TimeLapseDuration(Context context){
		this.mContext = context;
		initTimeLapseDuration();
	}

	public String getCurrentValue() {
		return convertTimeLapseDuration(cameraProperty.getCurrentTimeLapseDuration());
	}

	public String[] getValueStringList() {
		return valueListString;
	}

	public int[] getValueStringInt() {
		return valueListInt;
	}
	
	public boolean setValueByPosition(int position){
		boolean retValue;		
		retValue = CameraProperties.getInstance().setTimeLapseDuration(valueListInt[position]);
		return retValue;
	}
	
	private CameraProperties cameraProperty = CameraProperties.getInstance();
	
	public void initTimeLapseDuration() {
		if (cameraProperty.cameraModeSupport(ICatchMode.ICH_MODE_TIMELAPSE) == false) {
			return;
		}
		List<Integer> list = cameraProperty.getSupportedTimeLapseDurations();
		int length = list.size();
		ArrayList<String> tempArrayList = new ArrayList<String>();
		valueListInt = new int[length];
		
		for (int ii = 0; ii < length; ii++) {
			tempArrayList.add(convertTimeLapseDuration(list.get(ii)));
			valueListInt[ii] = list.get(ii);
		}
		
		valueListString = new String[tempArrayList.size()];
		for(int ii = 0;ii < tempArrayList.size();ii++){
			valueListString[ii] = tempArrayList.get(ii);
		}	
	}
	
	public String convertTimeLapseDuration(int value){
		if(value == 0xffff){
//			return mContext.getResources().getString(R.string.setting_time_lapse_duration_unlimit);
			return null;
		}
		String time = "";
		int h = value / 60;
		int m = value % 60;
		if(h > 0){
			time = time + h+"HR";
		}
		if(m > 0){
			time = time + m + "Min";
		}
		return time;
	}
}
