package com.icatch.wcmapp3.camera;

import android.content.Context;
import com.icatch.wcmapp3.SDKAPI.CameraFixedInfo;
import com.icatch.wcmapp3.SDKAPI.CameraProperties;
import com.icatch.wcmapp3.SDKAPI.CameraState;
import com.icatch.wcmapp3.global.App.AppInfo;
import com.icatch.wcmapp3.global.App.PropertyId;
import com.icatch.wcmapp3.global.App.SettingMenu;
import com.icatch.wificam.customer.type.ICatchCameraProperty;
import com.icatch.wificam.customer.type.ICatchMode;
import com.iotgroup.roame.R;

import java.util.LinkedList;

public class UIDisplaySource {
	public static final int CAPTURE_SETTING_MENU = 1;
	public static final int VIDEO_SETTING_MENU = 2;
	public static final int TIMELAPSE_SETTING_MENU = 3;
	public static final int OTHER_SETTING_MENU = 4;

	private static UIDisplaySource uiDisplayResource;
	private CameraState cameraState = CameraState.getInstance();
	private static Context mContext;

	public static UIDisplaySource getinstance(Context context) {
		if (uiDisplayResource == null) {
			uiDisplayResource = new UIDisplaySource();
			mContext = context;
		}

		return uiDisplayResource;
	}

	public static void createInstance() {
		uiDisplayResource = new UIDisplaySource();
	}

	public LinkedList<SettingMenu> getList(int type, MyCamera currCamera) {
		switch (type) {
		case CAPTURE_SETTING_MENU:
			return getForCaptureMode(currCamera);
		case VIDEO_SETTING_MENU:
			return getForVideoMode(currCamera);
		case OTHER_SETTING_MENU:
			return getForOtherMode(currCamera);
		default:
			return null;
		}
	}

	public LinkedList<SettingMenu> getA10List(int type, MyCamera currCamera) {
		switch (type) {
		case CAPTURE_SETTING_MENU:
			return getForA10CaptureMode(currCamera);
		case VIDEO_SETTING_MENU:
			return getForA10VideoMode(currCamera);
		case OTHER_SETTING_MENU:
			return getForA10OtherMode(currCamera);
		default:
			return null;
		}
	}
	
	/**
	 * A10拍照参数的获取
	 * @param currCamera
	 * @return
	 */
	public LinkedList<SettingMenu> getForA10CaptureMode(MyCamera currCamera) {
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();
		temp.add(new SettingMenu(R.string.video_resolutions, currCamera.getImageSize().getCurrentUiStringInSetting()));
		
		return temp;
	}
	
	/**
	 * S91拍照参数的获取
	 * @param currCamera
	 * @return
	 */
	public LinkedList<SettingMenu> getForCaptureMode(MyCamera currCamera) {
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();
		temp.add(new SettingMenu(R.string.video_resolutions, currCamera.getImageSize().getCurrentUiStringInSetting()));
//		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_BURST_NUMBER) == true) {
//			temp.add(new SettingMenu(R.string.burst, currCamera.getBurst().getCurrentUiStringInSetting()));
//		}
		
		if (CameraProperties.getInstance().cameraModeSupport(ICatchMode.ICH_MODE_TIMELAPSE)) {
			String currentTimeLapse = currCamera.getTimeLapseInterval().getCurrentValue();
			if(currentTimeLapse.contains("1 Sec")){
				currentTimeLapse = "1s";
			}else if(currentTimeLapse.contains("2 Sec")){
				currentTimeLapse = "2s";
			}else if(currentTimeLapse.contains("3 Sec")){
				currentTimeLapse = "3s";
			}else if(currentTimeLapse.contains("5 Sec")){
				currentTimeLapse = "5s";
			}else if(currentTimeLapse.contains("10 Sec")){
				currentTimeLapse = "10s";
			}else if(currentTimeLapse.contains("30 Sec")){
				currentTimeLapse = "30s";
			}else if(currentTimeLapse.contains("1 Min")){
				currentTimeLapse = "60s";
			}
			temp.add(new SettingMenu(R.string.setting_time_lapse_interval, currentTimeLapse));
		}
		
//		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_DATE_STAMP) == true) {
//			temp.add(new SettingMenu(R.string.setting_datestamp, currCamera.getDateStamp().getCurrentUiStringInSetting()));
//		}
		return temp;
	}

	/**
	 * A10录像参数的获取
	 * @param currCamera
	 * @return
	 */
	public LinkedList<SettingMenu> getForA10VideoMode(MyCamera currCamera) {   //左边条目的标题和参数
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();
		temp.add(new SettingMenu(R.string.video_resolutions, CameraProperties.getInstance().getCurrentStringPropertyValue(PropertyId.VIDEO_RESOLUTION) + "fps"));
		return temp;
	}
	
	/**
	 * S91录像参数的获取
	 * @param currCamera
	 * @return
	 */
	public LinkedList<SettingMenu> getForVideoMode(MyCamera currCamera) {   //左边条目的标题和参数
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();
		temp.add(new SettingMenu(R.string.video_resolutions, CameraProperties.getInstance().getCurrentStringPropertyValue(PropertyId.VIDEO_RESOLUTION) + "fps"));
		
		//-----------------------------------------新增参数-----------------------------------------------------------------
		//比特率
		int currentBitrate = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.BITRATE);
		if(currentBitrate == 0){
			temp.add(new SettingMenu(R.string.bit_rate, mContext.getResources().getString(R.string.hight)));
		}else if(currentBitrate == 1){
			temp.add(new SettingMenu(R.string.bit_rate, mContext.getResources().getString(R.string.common)));
		}
		
		//视角
		int currentPerspective = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.PERSPECTIVE);
		if(currentPerspective == 0){
			temp.add(new SettingMenu(R.string.drone_setting_view, mContext.getResources().getString(R.string.vastitude)));
		}else if(currentPerspective == 1){
			temp.add(new SettingMenu(R.string.drone_setting_view, mContext.getResources().getString(R.string.middle)));
		}else if(currentPerspective == 2){
			temp.add(new SettingMenu(R.string.drone_setting_view, mContext.getResources().getString(R.string.narrow)));
		}		
		
//		if (CameraProperties.getInstance().hasFuction(PropertyId.UP_SIDE)) {
//			temp.add(new SettingMenu(R.string.upside, currCamera.getUpside().getCurrentUiStringInSetting()));
//		}
		
		//画面显示时间
		int videoShowTime = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.VIDEOSHOWTIME);
		if(videoShowTime == 1){
			temp.add(new SettingMenu(R.string.setting_datestamp, mContext.getResources().getString(R.string.setting_off)));
		}else{
			temp.add(new SettingMenu(R.string.setting_datestamp, mContext.getResources().getString(R.string.setting_on)));
		}
		
		//自动启动录影
		int currentAutoStartRecord = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.AUTOSTART_RECORD);
		if(currentAutoStartRecord == 0){
			temp.add(new SettingMenu(R.string.autostartrecord, mContext.getResources().getString(R.string.setting_off)));
		}else if(currentAutoStartRecord == 5){
			temp.add(new SettingMenu(R.string.autostartrecord, mContext.getResources().getString(R.string.autostart_5s)));
		}else if(currentAutoStartRecord == 10){
			temp.add(new SettingMenu(R.string.autostartrecord, mContext.getResources().getString(R.string.autostart_10s)));
		}else if(currentAutoStartRecord == 20){
			temp.add(new SettingMenu(R.string.autostartrecord, mContext.getResources().getString(R.string.autostart_20s)));
		}
		
		
		//录像同步拍照
//		int currentRecordSyncPic = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.VIDEO_SYNC_PHOTOGRAPH);
//		if(currentRecordSyncPic == 0){
//			temp.add(new SettingMenu(R.string.record_sync_pic, mContext.getResources().getString(R.string.manually)));
//		}else if(currentRecordSyncPic == 5){
//			temp.add(new SettingMenu(R.string.record_sync_pic, mContext.getResources().getString(R.string.autostart_5s)));
//		}else if(currentRecordSyncPic == 10){
//			temp.add(new SettingMenu(R.string.record_sync_pic, mContext.getResources().getString(R.string.autostart_10s)));
//		}else if(currentRecordSyncPic == 30){
//			temp.add(new SettingMenu(R.string.record_sync_pic, mContext.getResources().getString(R.string.autostart_30s)));
//		}else if(currentRecordSyncPic == 60){
//			temp.add(new SettingMenu(R.string.record_sync_pic, mContext.getResources().getString(R.string.autostart_60s)));
//		}
		
		//录影内容循环覆盖
		int currentLoopCover = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.RECORDING_COVER_LOOP);
		if(currentLoopCover == 0){
			temp.add(new SettingMenu(R.string.setup_loop_back, mContext.getResources().getString(R.string.setting_off)));
		}else if(currentLoopCover == 1){
			temp.add(new SettingMenu(R.string.setup_loop_back, mContext.getResources().getString(R.string.setting_on)));
		}
		//-----------------------------------------新增参数-----------------------------------------------------------------
		
		return temp;
	}
	
	public LinkedList<SettingMenu> getForA10OtherMode(MyCamera currCamera) {
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();
		if(CameraProperties.getInstance().hasFuction(PropertyId.CAMERA_ESSID)){//camera password and wifi
			temp.add(new SettingMenu(R.string.camera_wifi_configuration,""));
		}
		return temp;
	}

	public LinkedList<SettingMenu> getForOtherMode(MyCamera currCamera) {
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();

//		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_WHITE_BALANCE)) {
//			temp.add(new SettingMenu(R.string.setting_awb, currCamera.getWhiteBalance().getCurrentUiStringInSetting()));
//		}
		//-----------------------------------------新增参数------------------------------------------------------------------------
		//状态指示灯
		int currentIndicatorLight = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.INDICATOR_LIGHT);
		if(currentIndicatorLight == 0){
			temp.add(new SettingMenu(R.string.setup_selflamp, mContext.getResources().getString(R.string.setting_off)));
		}else if(currentIndicatorLight == 1){
			temp.add(new SettingMenu(R.string.setup_selflamp, mContext.getResources().getString(R.string.setting_on)));
		}
		
		//提示音
		int currentPrompt = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.PROMPT);
		if(currentPrompt == 0){
			temp.add(new SettingMenu(R.string.setup_key_tone, mContext.getResources().getString(R.string.setting_off)));
		}else if(currentPrompt == 1){
			temp.add(new SettingMenu(R.string.setup_key_tone, mContext.getResources().getString(R.string.medium)));
		}else if(currentPrompt == 2){
			temp.add(new SettingMenu(R.string.setup_key_tone, mContext.getResources().getString(R.string.standard)));
		}
		
		//电视输出格式
		int currentOutputFormat = CameraProperties.getInstance().getCurrentPropertyValue(PropertyId.VIEDO_OUTPUT_FORMATS);
		if(currentOutputFormat == 0){
			temp.add(new SettingMenu(R.string.setup_system_type, "NTSC"));
		}else if(currentOutputFormat == 1){
			temp.add(new SettingMenu(R.string.setup_system_type, "PAL"));
		}
		
		//-----------------------------------------新增参数------------------------------------------------------------------------
		
		temp.add(new SettingMenu(R.string.format, ""));

		temp.add(new SettingMenu(R.string.nesignation, ""));
		
		if(CameraProperties.getInstance().hasFuction(PropertyId.CAMERA_ESSID)){//camera password and wifi
			temp.add(new SettingMenu(R.string.camera_wifi_configuration,""));
		
		}
		temp.add(new SettingMenu(R.string.version, AppInfo.getAppVer()));
		temp.add(new SettingMenu(R.string.setting_product_name, CameraFixedInfo.getInstance().getCameraName()));
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_FW_VERSION)) {
			temp.add(new SettingMenu(R.string.setting_firmware_version, CameraFixedInfo.getInstance().getCameraVersion()));
		}
		return temp;
	}

}
