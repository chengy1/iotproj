package com.icatch.wcmapp3.function;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;

import com.icatch.wcmapp3.SDKAPI.FileOperation;
import com.icatch.wificam.customer.type.ICatchFile;
import com.icatch.wificam.customer.type.ICatchFileType;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.iotgroup.roame.operate.MyConstants;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class BitmapsLoad {
	private boolean isLoadComplete;
	private static BitmapsLoad instance = new BitmapsLoad();
	private FileOperation fileOperation = FileOperation.getInstance();
	private ExecutorService executor;
	private Future<Object> future;	
	public LinkedList<ThreadInfo> taskList;
	private HashMap<Integer, Boolean> taskMap;
	public Handler bitMapHandler;
	private boolean isAllowLoad = true;

	public static BitmapsLoad getInstance() {
		return instance;
	}

	public void startLoadOneBitmap(int fileHandle, Handler handler, int position) {
		if (isAllowLoad == false) {
			return;
		}
		ThreadInfo threadInfo = new ThreadInfo();
		threadInfo.mFileHandle = fileHandle;
		threadInfo.mHandler = handler;
		threadInfo.position = position;
		if (taskMap.containsKey(fileHandle) != true) {
			taskMap.put(fileHandle, true);
			taskList.add(threadInfo);
			if (future == null || future.isDone() || future.isCancelled()) {
				future = executor.submit(new LoadOneBitMapThread(), null);
			}
		} else {
		}
	}

	public void cancelAllTasks() {
		taskList.clear();
		taskMap.clear();
		fileOperation.cancelDownload();
	}

	public class LoadOneBitMapThread implements Runnable {

		private int fileHandle = 0;
		private Handler handler = null;
		ThreadInfo mThreadInfo;

		@Override
		public void run() {
			Thread.currentThread().setPriority(6);
			while (taskList.isEmpty() == false) {
				while (isLoadComplete == false) {
					// wait for last loading complete
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (taskList.isEmpty() == true) {
					return;
				}
				mThreadInfo = taskList.removeFirst();
				fileHandle = mThreadInfo.mFileHandle;
				taskMap.remove(fileHandle);
				handler = mThreadInfo.mHandler;
//				bitmap = null;
				ICatchFile file = new ICatchFile(fileHandle);
				isLoadComplete = false;
				ICatchFrameBuffer buffer = fileOperation.getThumbnail(file);
				isLoadComplete = true;
				if (buffer == null) {
					handler.obtainMessage(MyConstants.MESSAGE_LOAD_BITMAP_FAILED, mThreadInfo).sendToTarget();
					continue;
				}

				int datalength = buffer.getFrameSize();
				if (datalength > 0) {
					mThreadInfo.mBitmap = BitmapFactory.decodeByteArray(buffer.getBuffer(), 0, datalength);
					if (mThreadInfo.mBitmap == null) {
						handler.obtainMessage(MyConstants.MESSAGE_LOAD_BITMAP_FAILED, mThreadInfo).sendToTarget();
						continue;
					}
				} else {
					handler.obtainMessage(MyConstants.MESSAGE_LOAD_BITMAP_FAILED, mThreadInfo).sendToTarget();
					continue;
				}
				isLoadComplete = true;
				handler.obtainMessage(MyConstants.MESSAGE_LOAD_BITMAP_SUCCESS, mThreadInfo).sendToTarget();
			}
			handler.obtainMessage(MyConstants.MESSAGE_ALL_TASKS_DONE, fileHandle).sendToTarget();
		}
	}

	public class ThreadInfo {
		public int mFileHandle;
		public Handler mHandler;
		public Bitmap mBitmap;
		public int position;
		public ICatchFileType fileType;

		public ThreadInfo() {
			mFileHandle = 0;
			mHandler = null;
			mBitmap = null;
			position = 0;
			fileType = ICatchFileType.ICH_TYPE_UNKNOWN;
		}
	}

	public void killLoadThread() {
		executor.shutdown(); // Disable new tasks from being submitted
		try {
			if (!executor.awaitTermination(1, TimeUnit.SECONDS)) {
				executor.shutdownNow(); // Cancel currently executing tasks
			}
		} catch (InterruptedException e) {
			// (Re-)cancel if current thread also interrupted
			executor.shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		}
		isAllowLoad = false;
	}

	public void initData() {
		isAllowLoad = true;
		executor = Executors.newFixedThreadPool(1);
		fileOperation = FileOperation.getInstance();
		isLoadComplete = true;
		taskList = new LinkedList<ThreadInfo>();
		taskMap = new HashMap<Integer, Boolean>();
		future = null;
	}
}