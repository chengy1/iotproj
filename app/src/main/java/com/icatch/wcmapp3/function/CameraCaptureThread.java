package com.icatch.wcmapp3.function;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;

import com.icatch.wcmapp3.SDKAPI.CameraAction;
import com.icatch.wcmapp3.SDKAPI.CameraProperties;
import com.icatch.wificam.customer.type.ICatchCameraProperty;
import com.iotgroup.roame.R;
import com.iotgroup.roame.operate.MyConstants;

import java.util.Timer;
import java.util.TimerTask;

public class CameraCaptureThread implements Runnable {
	private Context context;
	private MediaPlayer stillCaptureStartBeep;
	private MediaPlayer delayBeep;
	private MediaPlayer continuousCaptureBeep;
	// private CameraProperties cameraProperties = new CameraProperties();
	// private CameraAction cameraAction = new CameraAction();
	private Handler handler;

	public CameraCaptureThread(Context context, Handler handler) {
		this.context = context;
		this.handler = handler;
		stillCaptureStartBeep = MediaPlayer.create(context, R.raw.captureshutter);
		delayBeep = MediaPlayer.create(context, R.raw.delay_beep);
		continuousCaptureBeep = MediaPlayer.create(context, R.raw.captureburst);
	}

	@Override
	public void run() {
		// photo capture
		long lastTime = 0;
		int delayTime = MyConstants.getInstance().getCurrentCamera().getCaptureDelay().getCurrentValue();
		int remainBurstNumer = 1;
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_BURST_NUMBER) == true) {
			remainBurstNumer = CameraProperties.getInstance().getCurrentAppBurstNum();
		} else {
			remainBurstNumer = 1;
			// stillCaptureStartBeep.start();
		}
		if (remainBurstNumer == 1) {
			CaptureAudioTask captureAudioTask = new CaptureAudioTask(remainBurstNumer,0);
			Timer captureAudioTimer = new Timer(true);
			captureAudioTimer.schedule(captureAudioTask, delayTime, 200);
		}else{
			CaptureAudioTask captureAudioTask = new CaptureAudioTask(remainBurstNumer,1);
			Timer captureAudioTimer = new Timer(true);
			captureAudioTimer.schedule(captureAudioTask, delayTime, 200);
		}

		int count = delayTime / 1000;
		int timerDelay = 0;
		if (delayTime >= 5000) {
			Timer delayTimer = new Timer(true);
			DelayTimerTask delayTimerTask = new DelayTimerTask(count / 2,delayTimer);
			delayTimer.schedule(delayTimerTask, 0, 1000);
			timerDelay = delayTime;
		} else {
			timerDelay = 0;
			count = delayTime / 500;
		}
		if (delayTime >= 3000) {
			Timer delayTimer1 = new Timer(true);
			DelayTimerTask delayTimerTask1 = new DelayTimerTask(count/2,delayTimer1);
			delayTimer1.schedule(delayTimerTask1, timerDelay / 2, 500);
			timerDelay = delayTime;
		} else {
			timerDelay = 0;
			count = delayTime / 250;
		}
		Timer delayTimer2 = new Timer(true);
		DelayTimerTask delayTimerTask2 = new DelayTimerTask(count, delayTimer2);
		delayTimer2.schedule(delayTimerTask2, timerDelay - timerDelay / 4, 250);
		CameraAction.getInstance().triggerCapturePhoto();
	}

	private class CaptureAudioTask extends TimerTask {
		private int burstNumber;
		private int type = 0;

		public CaptureAudioTask(int burstNumber,int type) {
			this.burstNumber = burstNumber;
			this.type = type;
		}

		@Override
		public void run() {
			if(type == 0){
				if (burstNumber > 0) {
					stillCaptureStartBeep.start();
					burstNumber--;
				} else {
					cancel();
				}
			}else{
				if (burstNumber > 0) {
					continuousCaptureBeep.start();
					burstNumber--;
				} else {
					cancel();
				}
			}
			
		}
	}

	private class DelayTimerTask extends TimerTask {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.TimerTask#run()
		 */
		private int count;
		private Timer timer;

		public DelayTimerTask(int count, Timer timer) {
			this.count = count;
			this.timer = timer;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (count-- > 0) {
				delayBeep.start();
			} else {
				if (timer != null) {
					timer.cancel();
				}
			}
		}

	}
}
