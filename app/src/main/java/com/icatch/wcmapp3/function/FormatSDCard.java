package com.icatch.wcmapp3.function;

import android.os.Handler;

import com.icatch.wcmapp3.SDKAPI.CameraAction;
import com.iotgroup.roame.operate.MyConstants;

public class FormatSDCard extends Thread{
	//private CameraAction cameraAction;
	private Handler handler;
	public FormatSDCard(Handler handler){
		this.handler = handler;
		//cameraAction = new CameraAction();
	}
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		handler.obtainMessage(MyConstants.MESSAGE_FORMAT_SD_START).sendToTarget();
		try {
			sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(CameraAction.getInstance().formatStorage()){
			handler.obtainMessage(MyConstants.MESSAGE_FORMAT_SUCCESS).sendToTarget();
		}else{
			handler.obtainMessage(MyConstants.MESSAGE_FORMAT_FAILED).sendToTarget();
		}
	}

}
