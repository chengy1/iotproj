package com.icatch.wcmapp3.global.App;
public class AppProperties {
	private int timeLapseMode = -1;
	public static AppProperties appProperties;

	public static AppProperties getInstanse() {
		if (appProperties == null) {
			appProperties = new AppProperties();
		}
		return appProperties;
	}

	public int getTimeLapseMode() {
		return timeLapseMode;
	}

	public void setTimeLapseMode(int timeLapseMode) {
		this.timeLapseMode = timeLapseMode;
	}
}
