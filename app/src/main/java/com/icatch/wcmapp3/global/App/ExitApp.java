package com.icatch.wcmapp3.global.App;

import android.app.Activity;
import android.app.Application;

import com.icatch.wcmapp3.SDKAPI.FileOperation;
import com.icatch.wcmapp3.SDKAPI.PreviewStream;
import com.icatch.wcmapp3.SDKAPI.VideoPlayback;
import com.icatch.wcmapp3.camera.MyCamera;
import com.iotgroup.roame.operate.MyConstants;

import java.util.LinkedList;
import java.util.List;

public class ExitApp extends Application {

	private List<Activity> activityList = new LinkedList<Activity>();
	private static ExitApp instance;

	public static ExitApp getInstance() {
		if (instance == null) {
			instance = new ExitApp();
		}
		return instance;
	}

	public void addActivity(Activity activity) {
		activityList.add(activity);
	}

	public void removeActivity(Activity activity) {
		activityList.remove(activity);
	}

	public void exit() {
		boolean temp = false;
		PreviewStream previewStream = PreviewStream.getInstance();
		FileOperation fileOperation = FileOperation.getInstance();
		VideoPlayback videoPlayback = VideoPlayback.getInstance();

//		for (MyCamera camera : AeeApplication.getInstance().getCameraList()) {
//			if (camera.getSDKsession().isSessionOK() == true) {
//
//				temp = fileOperation.cancelDownload();
//				temp = false;
//				temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
//
//				temp = false;
//				temp = videoPlayback.stopPlaybackStream();
//				temp = false;
//				temp = camera.destroyCamera();
//			}
//		}
		
		MyCamera camera = MyConstants.getInstance().getCurrentCamera();
		temp = fileOperation.cancelDownload();
		temp = false;
		temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());

		temp = false;
		temp = videoPlayback.stopPlaybackStream();
		temp = false;
		temp = camera.destroyCamera();
		
//		for (Activity activity : activityList) {
//			activity.finish();
//		}
		System.exit(0);
	}
	
	public void exitWhenScreenOff() {
		boolean temp = false;
		PreviewStream previewStream = PreviewStream.getInstance();
		FileOperation fileOperation = FileOperation.getInstance();
		VideoPlayback videoPlayback = VideoPlayback.getInstance();

//		for (MyCamera camera : AeeApplication.getInstance().getCameraList()) {
//			if (camera.getSDKsession().isSessionOK() == true) {
//				temp = fileOperation.cancelDownload();
//				temp = false;
//				temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
//				temp = false;
//				temp = videoPlayback.stopPlaybackStream();
//				temp = false;
//				temp = camera.destroyCamera();
//			}
//		}
		
		MyCamera camera = MyConstants.getInstance().getCurrentCamera();
		if (camera.getSDKsession().isSessionOK() == true) {
			temp = fileOperation.cancelDownload();
			temp = false;
			temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
			temp = false;
			temp = videoPlayback.stopPlaybackStream();
			temp = false;
			temp = camera.destroyCamera();
		}
		
		for (Activity activity : activityList) {
			activity.finish();
		}
	}

	public void finishAllActivity() {
		PreviewStream previewStream = PreviewStream.getInstance();
		boolean temp = false;

//		for (MyCamera camera : AeeApplication.getInstance().getCameraList()) {
//			temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
//			temp = false;
//			temp = camera.destroyCamera();
//		}
		MyCamera camera = MyConstants.getInstance().getCurrentCamera();
		temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
		temp = false;
		temp = camera.destroyCamera();
		
		for (Activity activity : activityList) {
			activity.finish();
		}
	}
}
