package com.icatch.wcmapp3.global.App;

public class SettingMenu {
	public int name;
	public String value;

	public SettingMenu(int name, String value) {
		this.name = name;
		this.value = value;
	}

	public int getName() {
		return name;
	}

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return name + "--------------------" + value;
	}
}
