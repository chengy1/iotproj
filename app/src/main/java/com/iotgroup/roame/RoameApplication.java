package com.iotgroup.roame;

import android.app.Application;

import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.constants.RoameConstants;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;

/**
 * Created by huanghao on 2017/2/22.
 */

public class RoameApplication extends Application {

    private static RoameApplication instance;
    public static boolean forbidAudioOutput = false;
    public static boolean enableSoftwareDecoder = false;

    public static boolean isNeedReboot = false;

    public SendFlightData sendFlightData = null;

    public static RoameApplication getInstance(){
        if(instance == null){
            try {
                throw new Exception("Application is null!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        //Log配置
        Logger.init(RoameConstants.ROAMETAG)
              .methodCount(3)
              .logLevel(LogLevel.NONE)
              .methodOffset(2);

    }
}
