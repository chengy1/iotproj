package com.iotgroup.roame.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iotgroup.roame.R;
import com.iotgroup.roame.RoameApplication;
import com.iotgroup.roame.bean.FlightMessage;
import com.iotgroup.roame.bean.ReceiveFlightData;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.constants.RoameConstants;
import com.iotgroup.roame.utils.AppUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by huanghao on 2017/3/22.
 */

public class AppInfoActivity extends AppCompatActivity {


    @BindView(R.id.iv_settingback)
    ImageView ivSettingback;

    @BindView(R.id.video_resolution)
    ImageView videoResolution;

    @BindView(R.id.tv_appcachesize)
    TextView tvAppcachesize;

    @BindView(R.id.clear_appcache)
    RelativeLayout clearAppcache;

    @BindView(R.id.iv_firmware_update)
    ImageView ivFirmwareUpdate;

    @BindView(R.id.rl_firmware_update)
    RelativeLayout rlFirmwareUpdate;

    @BindView(R.id.iv_accelerometer_calibration)
    ImageView ivAccelerometerCalibration;

    @BindView(R.id.rl_accelerometer_calibration)
    RelativeLayout rlAccelerometerCalibration;

    @BindView(R.id.tv_app_version)
    TextView tvAppVersion;

    @BindView(R.id.rl_app_version)
    RelativeLayout rlAppVersion;

    @BindView(R.id.tv_accelerometer_calibration_instruction)
    TextView tvAccelerometerCalibrationInstruction;

    @BindView(R.id.bt_start_calibration)
    Button btStartCalibration;

    private FlightMessage flightMessage;

    /**
     * 加速度计校准的返回值 step1:32 step2:64 step3:96 step4:128 step5:160 step6:192 正在校准:224 等待校准:0
     */
    private int accelerometerCalibrationFlag;

    private SendFlightData sendFlightData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_appinfo);

        ButterKnife.bind(this);

        EventBus.getDefault().register(this);

        initData();

    }

    private void initData() {

        sendFlightData = RoameApplication.getInstance().sendFlightData;

        tvAppVersion.setText(AppUtils.getVersionName(this));

    }

    private boolean isStratCalibration = false;

    private int calibrationStep = -1;

    private boolean flagStep1 = true;
    private boolean flagStep2 = true;
    private boolean flagStep3 = true;
    private boolean flagStep4 = true;
    private boolean flagStep5 = true;
    private boolean flagStep6 = true;
    private boolean flagError = true;

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void getReceiveFligtData(ReceiveFlightData data) {

        byte[] receiveData = data.getReceiveData();
        flightMessage = new FlightMessage();
        flightMessage.checkReceiveData(receiveData);
        accelerometerCalibrationFlag = flightMessage.getAccelerometerCalibrationFlag();

        if (accelerometerCalibrationFlag == 32) {
            //第一步校准成功
            isStratCalibration = true;
            if (flagStep1) {
                flagStep1 = false;
                String step1 = this.getString(R.string.accelerometer_calibration_step1);
                calibrationStep = 1;
                tvAccelerometerCalibrationInstruction.setText(step1);
                btStartCalibration.setText(R.string.calibrae_step1);

            }
        } else if (accelerometerCalibrationFlag == 64) {
            //第二步校准成功
            isStratCalibration = true;
            if(flagStep2){
                flagStep2 = false;
                String step2 = this.getString(R.string.accelerometer_calibration_step2);
                calibrationStep = 2;
                tvAccelerometerCalibrationInstruction.setText(step2);
                btStartCalibration.setText(R.string.calibrae_step2);
            }
        } else if (accelerometerCalibrationFlag == 96) {
            //第三步校准成功
            isStratCalibration = true;
            if(flagStep3){
                flagStep3 = false;
                String step3 = this.getString(R.string.accelerometer_calibration_step3);
                calibrationStep = 3;
                tvAccelerometerCalibrationInstruction.setText(step3);
                btStartCalibration.setText(R.string.calibrae_step3);
            }
        } else if (accelerometerCalibrationFlag == 128) {
            //第四步校准成功
            isStratCalibration = true;
            if(flagStep4){
                flagStep4 = false;
                String step4 = this.getString(R.string.accelerometer_calibration_step4);
                calibrationStep = 4;
                tvAccelerometerCalibrationInstruction.setText(step4);
                btStartCalibration.setText(R.string.calibrae_step4);
            }
        } else if (accelerometerCalibrationFlag == 160) {
            //第五步校准成功
            isStratCalibration = true;
            if(flagStep5){
                flagStep5 = false;
                String step5 = this.getString(R.string.accelerometer_calibration_step5);
                calibrationStep = 5;
                tvAccelerometerCalibrationInstruction.setText(step5);
                btStartCalibration.setText(R.string.calibrae_step5);
            }
        } else if (accelerometerCalibrationFlag == 192) {
            //校准成功
            isStratCalibration = false;
            if(flagStep6){
                flagStep6 = false;
                calibrationStep = 6;
                String success = this.getResources().getString(R.string.accelerometer_calibration_success);
                tvAccelerometerCalibrationInstruction.setText(success);
                btStartCalibration.setText(R.string.calibrae_finish);
            }
        } else if (accelerometerCalibrationFlag == 0) {
            //等待校准
            if (isStratCalibration) {
                isStratCalibration = false;
                if(flagError){
                    flagError = false;
                    calibrationStep = -1;
                    String failed = this.getResources().getString(R.string.accelerometer_calibration_failed);
                    tvAccelerometerCalibrationInstruction.setText(failed);
                    btStartCalibration.setText(R.string.start_calibration);
                    sendFlightData.setDefaultAccelerometer();
                }
            }
        }

        Log.e(RoameConstants.ROAMETAG, "getReceiveFligtData:------>accelerometerCalibrationFlag: " + accelerometerCalibrationFlag);

    }

    /*****************************************************************************************************************************
     * onClick
     *****************************************************************************************************************************/

    @OnClick(R.id.iv_settingback)
    public void setBack() {
        finish();
    }

    /**
     * 加速度计校准
     */
    @OnClick(R.id.rl_accelerometer_calibration)
    public void accelerometerCalibration() {

        tvAccelerometerCalibrationInstruction.setVisibility(View.VISIBLE);
        btStartCalibration.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.bt_start_calibration)
    public void statrtCalibration(){

        if(calibrationStep == -1){
            sendFlightData.setAccelerometerStep1();
        }else if(calibrationStep == 1){
            sendFlightData.setAccelerometerStep2();
        }else if(calibrationStep == 2){
            sendFlightData.setAccelerometerStep3();
        }else if(calibrationStep == 3){
            sendFlightData.setAccelerometerStep4();
        }else if(calibrationStep == 4){
            sendFlightData.setAccelerometerStep5();
        }else if(calibrationStep == 5){
            sendFlightData.setAccelerometerStep6();
        }

    }

    /****************************************************************************************************************************
     * onClick
     ****************************************************************************************************************************/


    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);

    }

}
