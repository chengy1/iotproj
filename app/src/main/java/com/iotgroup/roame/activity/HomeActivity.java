package com.iotgroup.roame.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import com.iotgroup.roame.R;
import com.iotgroup.roame.operate.CameraOperate;
import com.iotgroup.roame.service.FlightControlOI100Service;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by huanghao on 2017/2/24.
 */

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.bt_connect)
    Button connect;

    @BindView(R.id.bt_joystick)
    Button joystick;
    private CameraOperate mCameraOperate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        initData();
    }

    private void initData() {
        mCameraOperate = CameraOperate.getInstance();
        mCameraOperate.setMyContext(this);
    }

    Intent connectService;

    @OnClick(R.id.bt_connect)
    public void connect(){
        connectService = new Intent(this, FlightControlOI100Service.class);
        startService(connectService);
    }

    @OnClick(R.id.bt_joystick)
    public void joysticks(){
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        boolean connectSuccess = mCameraOperate.aeeConnectCamera(wifiManager);
        if(connectSuccess){
            Intent intent = new Intent(this, TraceActivity.class);
            startActivity(intent);
        }else{

            Toast.makeText(HomeActivity.this, getResources().getString(R.string.connect_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(connectService != null){
            stopService(connectService);
        }
    }

}
