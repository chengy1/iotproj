package com.iotgroup.roame.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.iotgroup.roame.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by huanghao on 2017/3/20.
 */

public class LauncherActivity extends AppCompatActivity {

    @BindView(R.id.launcher_image)
    ImageView launcherImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_launcher);
        ButterKnife.bind(this);

        initLauncher();

    }

    private void initLauncher() {
        launcherImage.setImageResource(R.mipmap.luancher);

        new Thread(){
            @Override
            public void run() {
                SystemClock.sleep(2500);
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        startActivity(new Intent(LauncherActivity.this, MainActivity.class));
                        finish();
                    }
                });
            };
        }.start();
    }


}
