package com.iotgroup.roame.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import com.iotgroup.roame.R;
import com.iotgroup.roame.RoameApplication;
import com.iotgroup.roame.bean.EventMsg;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.operate.CameraOperate;
import com.iotgroup.roame.service.FlightControlOI100Service;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by huanghao on 2017/3/20.
 */

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.iv_homebg)
    ImageView ivHomebg;

    @BindView(R.id.iv_shop)
    ImageView ivShop;

    @BindView(R.id.iv_homesetting)
    ImageView ivHomesetting;

    @BindView(R.id.iv_entercamera)
    ImageView ivEntercamera;

    @BindView(R.id.iv_home_lib)
    ImageView ivHomeLib;

    private RotateAnimation mRa;

    private CameraOperate mCameraOperate;

    private SendFlightData flightData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mains);

        EventBus.getDefault().register(this);

        ButterKnife.bind(this);

        initView();

        initData();

        bindService();

    }

    private Intent connectService;

    /**
     * 绑定服务
     */
    private void bindService() {
        connectService = new Intent(this, FlightControlOI100Service.class);
        startService(connectService);
    }

    private void initData() {
        mCameraOperate = CameraOperate.getInstance();
        mCameraOperate.setMyContext(this);
    }

    private void initView() {
        //背景图片进行旋转动画
        mRa = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mRa.setInterpolator(new LinearInterpolator());
        mRa.setFillAfter(true);
        mRa.setDuration(20000);
        mRa.setRepeatCount(Animation.INFINITE);
        ivHomebg.setAnimation(mRa);
        mRa.start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void getSendFlightDatas(EventMsg eventMsg) {

        flightData = eventMsg.getFlightData();

        RoameApplication.getInstance().sendFlightData = flightData;

    }



    /****************************************************************************************************************************
     * onClick
     ***************************************************************************************************************************/

    @OnClick(R.id.iv_entercamera)
    public void startSession(){

        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        boolean connectSuccess = mCameraOperate.aeeConnectCamera(wifiManager);
        if(connectSuccess){
            Intent intent = new Intent(this, TraceActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, getResources().getString(R.string.connect_fail), Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.iv_homesetting)
    public void homeSetting(){

        startActivity(new Intent(this,AppInfoActivity.class));

    }

    /****************************************************************************************************************************
     * onClick
     ***************************************************************************************************************************/

    @Override
    protected void onRestart() {
        super.onRestart();
        ivHomebg.setAnimation(mRa);
        mRa.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ivHomebg.clearAnimation();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (connectService != null) {
            stopService(connectService);
        }

        EventBus.getDefault().unregister(this);

    }

}
