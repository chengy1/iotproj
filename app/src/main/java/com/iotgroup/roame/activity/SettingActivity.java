package com.iotgroup.roame.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.iotgroup.roame.R;
import com.iotgroup.roame.bean.EventMsg;
import com.iotgroup.roame.bean.FlightMessage;
import com.iotgroup.roame.bean.ReceiveFlightData;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.fragment.CompassFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * Created by huanghao on 2017/3/14.
 */

public class SettingActivity extends AppCompatActivity {

    @BindView(R.id.iv_compass_calibration)
    public ImageView compassCalibration;

    @BindView(R.id.fr_setting_content)
    public FrameLayout settingContent;

    public SendFlightData flightData;

    private int compassCalibrationFlag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fresco.initialize(this);

        setContentView(R.layout.activity_setting);

        EventBus.getDefault().register(this);

        initView();


    }


    CompassFragment compassFragment;

    FragmentTransaction fragmentTransaction;

    private void initView() {

        compassFragment = new CompassFragment();

        FragmentManager supportFragmentManager = getSupportFragmentManager();
        fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fr_setting_content, compassFragment).commit();

//        Intent intent = getIntent();
//        flightData = (SendFlightData)intent.getParcelableExtra("FlightData");
//        Bundle bundle = intent.getExtras();
//        flightData = bundle.getParcelable("FlightData");
//        flightData = (SendFlightData) getIntent().getSerializableExtra("FlightData");

    }

    FlightMessage flightMessage;

    /****************************************************************************************************
     * EventBus接收数据
     ****************************************************************************************************/

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void getSendFlightData(EventMsg msg){
        flightData = msg.getFlightData();
        if(sendFlightListener != null){
            sendFlightListener.getSendFlightData(flightData);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void getReceiveFligtData(ReceiveFlightData data){
        byte[] receiveData = data.getReceiveData();
        flightMessage = new FlightMessage();
        flightMessage.checkReceiveData(receiveData);
        compassCalibrationFlag = flightMessage.getCompassCalibrationFlag();
        if(compassListener != null){
            compassListener.complassFlagChanged(compassCalibrationFlag);
        }
    }

    /****************************************************************************************************
     * EventBus接收数据
     ****************************************************************************************************/

    //指南针校准监听
    private CompassCalibrationFlagListener compassListener;

    public void setCompassCalibrationChangeListener(CompassCalibrationFlagListener listener){
        compassListener = listener;
    }

    public interface  CompassCalibrationFlagListener{

        void complassFlagChanged(int flag);

    }

    private GetSendFlightChangeListener sendFlightListener;

    public void setSendFlightChangeListener(GetSendFlightChangeListener listener){
        sendFlightListener = listener;
    }

    public interface GetSendFlightChangeListener{
        void getSendFlightData(SendFlightData flightData);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        fragmentTransaction.remove(compassFragment);
        EventBus.getDefault().unregister(this);

    }

}
