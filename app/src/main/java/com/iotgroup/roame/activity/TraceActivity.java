package com.iotgroup.roame.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lib_gui.DragRectView;
import com.example.lib_gui.DrawRectView;
import com.icatch.wcmapp3.ExtendComponent.PreviewH264;
import com.icatch.wcmapp3.ExtendComponent.PreviewMjpg;
import com.iotgroup.roame.R;
import com.iotgroup.roame.RoameApplication;
import com.iotgroup.roame.bean.EventMsg;
import com.iotgroup.roame.bean.ReceiveFlightData;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.constants.FlyStatus;
import com.iotgroup.roame.constants.RoameConstants;
import com.iotgroup.roame.customview.JoystickControl;
import com.iotgroup.roame.customview.MyCustomTransparentDialog;
import com.iotgroup.roame.operate.CameraOperate;
import com.iotgroup.roame.track.TrackerViewHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by huanghao on 2017/3/8.
 */

public class TraceActivity extends AppCompatActivity {

    @BindView(R.id.control_frame)
    public FrameLayout controlFrame;

    @BindView(R.id.preview_mjpg)
    PreviewMjpg mPreviewMjpg;

    @BindView(R.id.preview_h264)
    PreviewH264 mPreviewH264;

    @BindView(R.id.iv_camera_left)
    public ImageView takeOff;

    @BindView(R.id.iv_no_head)
    public ImageView noHead;

    @BindView(R.id.drawRectView)
    public DrawRectView drawRectView;

    @BindView(R.id.dragRectView)
    public DragRectView dragRectView;

    @BindView(R.id.iv_face_track)
    public ImageView faceTrack;

    @BindView(R.id.iv_wifi)
    public ImageView wifiSignal;

    @BindView(R.id.iv_battery)
    public ImageView batteryBat;

    @BindView(R.id.tv_gps_star)
    public TextView gpsStart;

    @BindView(R.id.tv_channel_quantity)
    public TextView channelQuanitity;

    @BindView(R.id.tv_warn_msg)
    public TextView warnMsg;

    @BindView(R.id.iv_main_set)
    public ImageView mainSetting;

    private CameraOperate mCameraOperate;

    private View controlView;

    private JoystickControl joystickControl;

    private SendFlightData flightData;

    private byte[] receiveFlightData;

    private boolean isRunListen = false;

    private static final int RECEIVE_FLIGHT_DATA_LENGHT = 7;

    private static final byte END_MARK = 0x33;

    private static final byte START_MARK = (byte) 0xCC;

    private TrackerViewHelper trackerViewHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        EventBus.getDefault().register(this);

        ButterKnife.bind(this);



        initView();
        initData();
        initTrack();
    }


    private void initView() {

        controlView = View.inflate(this,R.layout.view_control,null);
        joystickControl = new JoystickControl(TraceActivity.this,controlView);
        controlFrame.addView(controlView);

    }

    int resized_width;
    int resized_height;
    Handler handler;

    private void initData() {

        flightData = RoameApplication.getInstance().sendFlightData;

        resized_width = getResources().getDimensionPixelOffset(R.dimen.resized_width);
        resized_height = getResources().getDimensionPixelOffset(R.dimen.resized_height);

        mCameraOperate = CameraOperate.getInstance();
        mCameraOperate.setMyContext(this);
        mCameraOperate.startPreview(this, mPreviewMjpg, mPreviewH264);

        handler = new Handler();

    }

    private void initTrack() {

        drawRectView.setVisibility(View.GONE);
        dragRectView.setVisibility(View.GONE);

        trackerViewHelper = new TrackerViewHelper(dragRectView,drawRectView,handler);
        mPreviewH264.setTrackerViewHelper(trackerViewHelper);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mCameraOperate.initMode();
        if(controlView != null){
            controlFrame.addView(controlView);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(controlView != null){
            controlFrame.removeView(controlView);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();

        isRunListen = true;

        showWifiSignal();

        trackerViewHelper.startBackgroundThread();
    }

    @Override
    protected void onPause() {
        super.onPause();

        trackerViewHelper.stopBackgroundThread();
    }


    /**
     * 显示wifi信号强度
     */
    private void showWifiSignal() {

        new Thread( new Runnable() {
            @Override
            public void run() {
                while (isRunListen) {
                    final int wifiRssi = getWifiSignal();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(wifiRssi >= -50 && wifiRssi <= 0){
                                wifiSignal.setImageResource(R.mipmap.wifi_3);
                            }else if(wifiRssi >= -70 && wifiRssi < -50){
                                wifiSignal.setImageResource(R.mipmap.wifi_2);
                            }else if(wifiRssi >= -100 && wifiRssi < -70){
                                wifiSignal.setImageResource(R.mipmap.wifi_1);
                            }else if(wifiRssi < -100){
                                wifiSignal.setImageResource(R.mipmap.wifi_0);
                            }

                        }
                    });
                }
            }
        }).start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取wifi信号
     */
    private int getWifiSignal() {
        @SuppressLint("WifiManagerLeak") WifiManager wifiService = (WifiManager) getSystemService(WIFI_SERVICE);

        if (wifiService != null) {
            WifiInfo wifiInfo = wifiService.getConnectionInfo();
            int wifiRssi = wifiInfo.getRssi();
            return wifiRssi;
        } else {
            return 0;
        }
    }

    /****************************************************************************************************
     *
     * EventBus接收数据
     *
     ****************************************************************************************************/

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void getSendFlightData(EventMsg eventMsg) {
        flightData = eventMsg.getFlightData();
        Log.e(RoameConstants.ROAMETAG, "getSendFlightData11111: "+flightData );

    }

    /**
     * Wifi断开状态监听
     *
     * @param flyStatus
     */
    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void monitorWifi(FlyStatus flyStatus) {
        switch (flyStatus) {
            case DISCONNECTED:
                SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                dialog.setTitleText("Wifi Error");
                dialog.setContentText("Wifi is not connected now");
                dialog.setConfirmText("Are you sure to exit?");
                dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                });
                dialog.show();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void getReceiveFlightData(ReceiveFlightData data) {

        receiveFlightData = data.getReceiveData();
        checkReceiveData(receiveFlightData);
//        Log.e(RoameConstants.ROAMETAG, " reciveData:" + Hex.printStringDivision(Hex.encodeHexStr(receiveFlightData).toUpperCase(), 2));
    }
    /****************************************************************************************************
     *
     *      EventBus接收数据
     *
     ****************************************************************************************************/

    /**
     * 错误信息计时
     */
    private int errorTimeCounter = -1;

    /**
     * 错误信息标记
     */
    private int errorMsgFlag = -1;

    /**
     * 接收飞控数据校验
     *
     * @param receiveFlightData
     */
    private void checkReceiveData(byte[] receiveFlightData) {

        if (receiveFlightData.length == RECEIVE_FLIGHT_DATA_LENGHT) {

            byte enddData = receiveFlightData[RECEIVE_FLIGHT_DATA_LENGHT - 1];
            byte startData = receiveFlightData[0];

            if (enddData == END_MARK && startData == START_MARK) {

                byte commandID = receiveFlightData[1];

                if (commandID == (byte) 0x83) {

                    byte data1 = receiveFlightData[2];
                    byte data2 = receiveFlightData[3];
                    byte data3 = receiveFlightData[4];

                    byte reCheckData = checkSum(commandID,data1, data2, data3);

                    byte checkData = receiveFlightData[5];

                    //校验通过
                    if (reCheckData == checkData) {
                        //一键起飞状态
                        int takeOffFlag = data1 & 0x01;

                        //一键降落状态
                        int landFlag = data1 & 0x02;

                        //无头模式
                        int headless = data1 & 0x04;

                        //指南针校准 0:等到校准指令 8:step1成功 16:step2成功 24:执行校准指令
                        int compassCalibrationFlag = data1 & 0x18;
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: 指南针校准--》compassCalibrationFlag---->"+compassCalibrationFlag );

                        //加速度计校准
                        int accelerometerCalibrationFlag = data1 & 0xe0 ;

                        //指南针状态 0:ok 1:异常
                        int compassFlag = data2 & 0x01;
                        if(compassFlag == 1){
                            String strCompassError = this.getString(R.string.compass_error);
                            warnMsg.setTextColor(Color.RED);
                            warnMsg.setText(strCompassError);
                        }else if(compassFlag == 0) {
                            String strNormal = this.getString(R.string.fly_normal);
                            warnMsg.setTextColor(Color.WHITE);
                            warnMsg.setText(strNormal);
                        }
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: compassFlag ==== " + compassFlag);

                        //低电报警  0:正常 2:低电,自动降落
                        int lowVoltageFlag = data2 & 0x02;
                        if(lowVoltageFlag == 2){
                            String strLowVolatge = this.getString(R.string.low_voltage);
                            String lowVoltageLand = this.getString(R.string.low_voltage_land);
                            String strLowlandding = this.getString(R.string.low_landding);

                            final SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                            dialog.setTitleText(strLowVolatge);
                            dialog.setContentText(lowVoltageLand);
                            dialog.setConfirmText(strLowlandding);
                            dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    dialog.cancel();
                                }
                            });
                            dialog.show();
                        }
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: lowVoltageFlag === " + lowVoltageFlag);

                        //电池电量 0:25%以下 4:25%-50% 8:50%-75% 12:75%-100%
                        int batteryLevelFlag = data2 & 0x0C;
                        showFlyBattery(batteryLevelFlag);
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: batteryLevelFlag === " + batteryLevelFlag);

                        //GPS搜星数    0-15
                        int gpsLevelData = data2 & 0xF0;
                        gpsStart.setText(" "+gpsLevelData);
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: gpsLevelData === " + gpsLevelData);

                        //wifi链接状态(丢包率)  0:75% 1:50% 2:25%-50% 3:<25%
                        int wifiFlag = data3 & 0x03;
                        showChanelQuanitity(wifiFlag);
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: wifiFlag === " + wifiFlag);

                    }

                }

            }

        }

    }

    /**
     * 显示通道信号量
     * @param wifiFlag
     *
     * 0:75%以上 1:50%-75% 2:25%-50% 3:<25%
     */
    private void showChanelQuanitity(int wifiFlag) {
        if(wifiFlag == 0){
            channelQuanitity.setTextColor(Color.RED);
            channelQuanitity.setText(" 18%");
        }else if(wifiFlag == 1){
            channelQuanitity.setTextColor(Color.RED);
            channelQuanitity.setText(" 42%");
        }else if(wifiFlag == 2){
            channelQuanitity.setTextColor(Color.GREEN);
            channelQuanitity.setText(" 63%");
        }else if(wifiFlag == 3){
            channelQuanitity.setTextColor(Color.WHITE);
            channelQuanitity.setText(" 88%");
        }
    }

    /**
     * 显示飞行电池电量
     * @param batteryLevelFlag
     *
     * 0:25%以下 4:25%-50% 8:50%-75% 12:75%-100%
     */
    private void showFlyBattery(int batteryLevelFlag) {

        if(batteryLevelFlag == 0){
            batteryBat.setImageResource(R.mipmap.battery_0);
        }else if(batteryLevelFlag == 4){
            batteryBat.setImageResource(R.mipmap.battery_1);
        }else if(batteryLevelFlag == 8){
            batteryBat.setImageResource(R.mipmap.battery_2);
        }else if(batteryLevelFlag == 12){
            batteryBat.setImageResource(R.mipmap.battery_3);
        }

    }

    /**
     * 显示异常状态信息
     */
    private void showErrorMsg(int errorFlag) {

        if(errorFlag == 1){
            String compassError = getString(R.string.compass_error);
            Toast.makeText(this,compassError,Toast.LENGTH_LONG).show();
        }else if(errorFlag == 2){
            String lowVoltage = getString(R.string.low_voltage);
            Toast.makeText(this,lowVoltage,Toast.LENGTH_LONG).show();
        }

    }

    /**
     * 校验位检验
     *
     * @param commandID
     * @param data1
     * @param data2
     * @param data3
     * @return
     */
    private byte checkSum(byte commandID,byte data1, byte data2, byte data3) {
        return (byte) (((commandID & 0xFF) ^(data1 & 0xFF) ^ (data2 & 0xFF) ^ (data3 & 0xFF))&0xFF) ;
    }

    int takeOrland = -1;

    private boolean isHead = true;

    private boolean isTrack = false;

    /***********************************************************************************************
     * onClick
     ***********************************************************************************************/

    @OnClick(R.id.iv_camera_left)
    public void takeOff() {
        //一键起飞  or  一键降落
        String string = this.getString(R.string.is_take_off);
        takeOrland = 0;
        showConfirmDialog(string);
    }

    @OnClick(R.id.iv_camera_right)
    public void land() {
        // 一键降落
        flightData.setLand();
//            String string = this.getString(R.string.is_land);
//            takeOrland = 1;
//            showConfirmDialog(string);
    }

    @OnClick(R.id.iv_no_head)
    public void noHead() {
        //设置为无头模式
        isHead = !isHead;
        takeOrland = 2;
        String noHeadMsg;
        if (!isHead) {
            noHeadMsg = this.getString(R.string.is_no_head);
        } else {
            noHeadMsg = this.getString(R.string.is_head);
        }
        showConfirmDialog(noHeadMsg);

    }

    @OnClick(R.id.iv_face_track)
    public void track(){

        if(!isTrack){
            takeOrland = 3;
            String autoTrack = this.getString(R.string.track);
            showConfirmDialog(autoTrack);
        }else{
            drawRectView.setVisibility(View.GONE);
            dragRectView.setVisibility(View.GONE);
            if(controlView != null){
                controlFrame.addView(controlView);
            }
        }


    }

    @OnClick(R.id.iv_main_set)
    public void paramSetting(){

        Intent intent = new Intent(this, SettingActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putParcelable("FlightData",flightData);
//        intent.putExtras(bundle);
//        intent.putExtra("FlightData",flightData);
//        Log.e(RoameConstants.ROAMETAG, "paramSetting-----> "+flightData );

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new EventMsg(flightData));
            }
        }, 2000);

        startActivity(intent);

    }

//    @OnClick(R.id.iv_face_track)
//    public void faceTrack() {
//        isTrack = !isTrack;
//
////        threadPool = Executors.newFixedThreadPool(5);
//
//        mPreviewH264.setOnYUVDataChangeListener(new PreviewH264.YUVDataChangeListener() {
//            @Override
//            public void onDataChange(byte[] data, final int width, final int height) {
//                yuvData = data;
//                dataWidth = width;
//                dataHeight = height;
//                data = null;
//
//                Log.e(RoameConstants.ROAMETAG, "onDataChange: ");
//
//                if (data != null) {
//                    System.gc();
//                }
//
//        /*
//
//        threadPool.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        final byte[] rsz_bytes = new byte[resized_width * resized_height];
//
////                        if(isTrack){
//
//                            CuriUtility.reduceYBytes(
//                                    yuvData,
//                                    dataWidth,
//                                    dataHeight,
//                                    rsz_bytes,
//                                    resized_width,
//                                    resized_height);
//
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    doWork(rsz_bytes,resized_width,resized_height);
//                                }
//                            });
////                        }
//                    }
//                });
//
//        */
//
//
////                    if(!isTrack){
////                        dragRectView.setVisibility(View.VISIBLE);
////                    }
//
//            }
//        });
//
//    }

    /***********************************************************************************************
     * onClick
     ***********************************************************************************************/

    private void showConfirmDialog(String msg) {
        final MyCustomTransparentDialog dialog = new MyCustomTransparentDialog(this, false);
        dialog.show();
        dialog.setCancelBtnText(R.string.cancel);
        dialog.setConfirmBtnText(R.string.sure);
        dialog.setMessage(msg);
        dialog.setOnTransparentDialogClickListener(new MyCustomTransparentDialog.OnTransparentDialogClickListener() {
            @Override
            public void onConfirmBtnClick() {
                if (takeOrland == 0) {
                    //一键起飞
                    flightData.setTakeOff();
                } else if (takeOrland == 1) {
                    //一键降落
                    flightData.setLand();
                } else if (takeOrland == 2) {
                    //有头 无头模式
                    flightData.setNoHead(isHead);
                }else if(takeOrland == 3){
                    //视觉跟踪开启
                    isTrack = !isTrack;
                    if(isTrack){
                        mPreviewH264.setFaceTrack(isTrack);
                        drawRectView.setVisibility(View.VISIBLE);
                        dragRectView.setVisibility(View.VISIBLE);

                        Log.e(RoameConstants.ROAMETAG, "onConfirmBtnClick: flightData------>"+flightData );

                        trackerViewHelper.mHandler.sendMessage(trackerViewHelper.mHandler.obtainMessage(123, flightData));
                        if(controlView != null){
                            controlFrame.removeView(controlView);
                        }
                    }

                }

                try {
                    TimeUnit.MILLISECONDS.sleep(180);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onCancelBtnClick() {
                dialog.dismiss();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCameraOperate.aeeExitCamera();
        isRunListen = false;
        EventBus.getDefault().unregister(this);
    }

}
