package com.iotgroup.roame.bean;



/**
 * Created by c on 2017/2/23.
 */

public class EventMsg {

    private SendFlightData flightData;

    public EventMsg(SendFlightData data){
        flightData = data;
    }

    public SendFlightData getFlightData() {
        return flightData;
    }

    public void setFlightData(SendFlightData flightData) {
        this.flightData = flightData;
    }
}
