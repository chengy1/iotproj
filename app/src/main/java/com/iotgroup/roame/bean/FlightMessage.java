package com.iotgroup.roame.bean;

import com.iotgroup.roame.service.impl.FlightControlOI100;


/**
 * Created by huanghao on 2017/3/14.
 */

public class FlightMessage {

    private static final int RECEIVE_FLIGHT_DATA_LENGHT = 7;

    private static final byte END_MARK = 0x33;

    private static final byte START_MARK = (byte) 0xCC;

    /**
     * 一键起飞状态
     */
    private int takeOffFlag;

    /**
     * 一键降落状态
     */
    private int landFlag;

    /**
     * 无头模式状态
     */
    private int headlessFlag;

    /**
     * 指南针校准状态
     */
    private int compassCalibrationFlag;

    /**
     * 加速度计状态
     */
    private int accelerometerCalibrationFlag;

    /**
     * 指南针状态
     */
    private int compassFlag;

    /**
     * 低电报警状态
     */
    private int lowVoltageFlag;

    /**
     * 电池电量
     */
    private int batteryLevelFlag;

    /**
     * GPS搜星数
     */
    private int gpsLevelData;

    /**
     * 通信量
     */
    private int channelFlag;


    private static  FlightMessage flightMessage;

    public static FlightMessage getInstance() {
        if (flightMessage == null) {
            synchronized (FlightControlOI100.class) {
                if (flightMessage == null) {

                    flightMessage = new FlightMessage();

                }
            }
        }

        return flightMessage;

    }

    /**
     * 获取对应的飞行状态信息
     * @param receiveFlightData
     */
    public void checkReceiveData(byte[] receiveFlightData) {

        if(receiveFlightData != null){
            if (receiveFlightData.length == RECEIVE_FLIGHT_DATA_LENGHT) {

                byte enddData = receiveFlightData[RECEIVE_FLIGHT_DATA_LENGHT - 1];
                byte startData = receiveFlightData[0];

                if (enddData == END_MARK && startData == START_MARK) {

                    byte commandID = receiveFlightData[1];

                    if (commandID == (byte) 0x83) {

                        byte data1 = receiveFlightData[2];
                        byte data2 = receiveFlightData[3];
                        byte data3 = receiveFlightData[4];

                        byte reCheckData = checkSum(commandID, data1, data2, data3);

                        byte checkData = receiveFlightData[5];

                        //校验通过
                        if (reCheckData == checkData) {
                            //一键起飞状态
                            takeOffFlag = data1 & 0x01;

                            //一键降落状态
                            landFlag = data1 & 0x02;

                            //无头模式
                            headlessFlag = data1 & 0x04;

                            //指南针校准 0:等到校准指令 1:step1成功 2:step2成功 3:执行校准指令
                            compassCalibrationFlag = data1 & 0x18;

//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: 指南针校准--》compassCalibrationFlag---->" + compassCalibrationFlag);

                            //加速度计校准
                            accelerometerCalibrationFlag = data1 & 0xe0;

                            //指南针状态 0:ok 1:异常
                            compassFlag = data2 & 0x01;
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: compassFlag ==== " + compassFlag);

                            //低电报警  0:正常 1:低电,自动降落
                            lowVoltageFlag = data2 & 0x02;
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: lowVoltageFlag === " + lowVoltageFlag);

                            //电池电量 0:25%以下 1:25%-50% 2:50%-75% 3:75%-100%
                            batteryLevelFlag = data2 & 0x0C;
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: batteryLevelFlag === " + batteryLevelFlag);

                            //GPS搜星数    0-15
                            gpsLevelData = data2 & 0xF0;
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: gpsLevelData === " + gpsLevelData);

                            //wifi链接状态(丢包率)  0:75% 1:50% 2:25%-50% 3:<25%
                            channelFlag = data3 & 0x03;
//                        Log.e(RoameConstants.ROAMETAG, "checkReceiveData: wifiFlag === " + wifiFlag);

                        }

                    }

                }

            }
        }

    }

    /**
     * 校验位检验
     *
     * @param commandID
     * @param data1
     * @param data2
     * @param data3
     * @return
     */
    private byte checkSum(byte commandID, byte data1, byte data2, byte data3) {
        return (byte) (((commandID & 0xFF) ^ (data1 & 0xFF) ^ (data2 & 0xFF) ^ (data3 & 0xFF)) & 0xFF);
    }


    public int getTakeOffFlag() {
        return takeOffFlag;
    }

    public void setTakeOffFlag(int takeOffFlag) {
        this.takeOffFlag = takeOffFlag;
    }

    public int getLandFlag() {
        return landFlag;
    }

    public void setLandFlag(int landFlag) {
        this.landFlag = landFlag;
    }

    public int getHeadlessFlag() {
        return headlessFlag;
    }

    public void setHeadlessFlag(int headlessFlag) {
        this.headlessFlag = headlessFlag;
    }

    public int getCompassCalibrationFlag() {
        return compassCalibrationFlag;
    }

    public void setCompassCalibrationFlag(int compassCalibrationFlag) {
        this.compassCalibrationFlag = compassCalibrationFlag;
    }

    public int getAccelerometerCalibrationFlag() {
        return accelerometerCalibrationFlag;
    }

    public void setAccelerometerCalibrationFlag(int accelerometerCalibrationFlag) {
        this.accelerometerCalibrationFlag = accelerometerCalibrationFlag;
    }

    public int getCompassFlag() {
        return compassFlag;
    }

    public void setCompassFlag(int compassFlag) {
        this.compassFlag = compassFlag;
    }

    public int getLowVoltageFlag() {
        return lowVoltageFlag;
    }

    public void setLowVoltageFlag(int lowVoltageFlag) {
        this.lowVoltageFlag = lowVoltageFlag;
    }

    public int getBatteryLevelFlag() {
        return batteryLevelFlag;
    }

    public void setBatteryLevelFlag(int batteryLevelFlag) {
        this.batteryLevelFlag = batteryLevelFlag;
    }

    public int getGpsLevelData() {
        return gpsLevelData;
    }

    public void setGpsLevelData(int gpsLevelData) {
        this.gpsLevelData = gpsLevelData;
    }

    public int getChannelFlag() {
        return channelFlag;
    }

    public void setChannelFlag(int channelFlag) {
        this.channelFlag = channelFlag;
    }

}
