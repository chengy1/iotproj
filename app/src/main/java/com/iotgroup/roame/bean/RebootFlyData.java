package com.iotgroup.roame.bean;

/**
 * Created by huanghao on 2017/3/20.
 */

public class RebootFlyData {

    /**
     * 协议头
     */
    private static final byte HEADER =  (byte) 0XCC;

    /**
     * 协议尾
     */
    private static final byte END =  (byte) 0X33;

    private  byte[] commanCmdData ={(byte) 0xCC,(byte)0x21 ,(byte) 0x00};

    private int cmdLength = 5;

    private byte [] cmdData;

    public RebootFlyData(){

        cmdData = new byte [cmdLength];
        System.arraycopy(commanCmdData, 0, cmdData, 0, 2);
        int checkSum = checkSum();
        cmdData[2] = (byte)(0x01);
        cmdData[3] = (byte)(checkSum & 0XFF);
        cmdData[4] = END;

    }

    private int checkSum() {

        int checkSum = 0;

        for(int i =1;i<=2;i++){

            if(i == 1){
                checkSum = (cmdData[i] & 0xFF);
            }else{
                checkSum ^= (cmdData[i] & 0xFF);
            }

        }

        return checkSum;

    }

    /**
     * 飞控停留在bootloader用于固件升级
     */

    public byte[] getCmdData(){

        byte[] sendDatas = new byte[this.cmdLength];

        int checkSum = checkSum();

        cmdData[3] = (byte)(checkSum & 0XFF);

        System.arraycopy(this.cmdData,0,sendDatas,0,this.cmdLength);

        cmdData[4] = (byte)0x33;

        return cmdData;

    }

}
