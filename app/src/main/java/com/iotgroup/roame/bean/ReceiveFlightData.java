package com.iotgroup.roame.bean;

/**
 * Created by c on 2017/3/8.
 */

public class ReceiveFlightData {

    private byte[] receiveData;

    public ReceiveFlightData(byte[] data){

        receiveData = data;

    }

    public void setReceiveData(byte[] data){
        receiveData = data;
    }

    public byte[] getReceiveData(){
        return receiveData;
    }


}
