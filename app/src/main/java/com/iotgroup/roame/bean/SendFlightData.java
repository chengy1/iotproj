package com.iotgroup.roame.bean;


public class SendFlightData {

	/**
	 * 协议头
	 */
	private static final byte HEADER =  (byte) 0XCC;
	
	/**
	 * 协议尾
	 */
	private static final byte END =  (byte) 0X33;
	
	private  byte[] commanCmdData ={(byte) 0xCC,(byte)0x16 ,(byte) 0x80, (byte) 0x80,
			(byte) 0x80, (byte) 0x80,(byte) 0x80,(byte) 0x00};
	
	/**
	 * 左右摇杆参数
	 */
	public int left = 128;
	
	/**
	 * 前后摇杆参数
	 */
	public int forward = 128;
	
	/**
	 * 油门摇杆参数
	 */
	public int throttle = 128;
	
	/**
	 * 航向摇杆参数
	 */
	public int roll = 128;
	
	private int cmdLength = 10;
	
	private byte [] cmdData;

//	private static final long serialVersionUID=1L;
//	private LogWriter logWriter;
//	private FileOutputStream fos;

	public SendFlightData(){
		
		cmdData = new byte [cmdLength];
		System.arraycopy(commanCmdData, 0, cmdData, 0, 7);
		int checkSum = checkSum();
		cmdData[8] = (byte)(checkSum & 0XFF);
		cmdData[9] = (byte)0x33;

	}

	/**
	 * 
	 * 数据校验
	 * @return
	 */
	private int checkSum(){
		
		int checkSum = 0;
		
		for(int i =1;i<=7;i++){
			
			if(i == 1){
				checkSum = (cmdData[i] & 0xFF);
			}else{
				checkSum ^= (cmdData[i] & 0xFF);
			}
			
		}
		
		return checkSum;
	}
	
	public byte[] getCmdData(){

		byte[] sendDatas = new byte[this.cmdLength];

		int checkSum = checkSum();

		cmdData[8] = (byte)(checkSum & 0XFF);

		System.arraycopy(this.cmdData,0,sendDatas,0,this.cmdLength);

		cmdData[9] = (byte)0x33;

		return cmdData;

	}

	public void setFlightData(int left, int forward, int throttle, int roll){

		this.left= left;
		this.forward = forward;
		this.throttle = throttle;
		this.roll = roll;

		commanCmdData[1] = (byte)0x16;
		cmdData[2] = (byte) (left & 0xFF);
		cmdData[3] = (byte) (forward & 0xFF);
		cmdData[4] = (byte) (throttle & 0xFF);
		cmdData[5] = (byte) (roll & 0xFF);

	}

	private int flyAction = 0;

	/**
	 * 一键起飞
	 */
	public void setTakeOff(){

		flyAction = (flyAction & 0xFF)^0x01;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 一键降落
	 */
	public void setLand(){

		flyAction = (flyAction & 0xFF)^0x02;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 设置无头模式
	 */
	public void setNoHead(boolean isHead){

		if(isHead){
			//有头
			flyAction = (flyAction & 0xFF)^0x04;
		}else{
			//无头
			flyAction = (flyAction & 0xFF)^0x04;
		}
		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 设置为默认的校准模式
	 */
	public void setDafaultCompass(){

		flyAction = (flyAction & 0xFF)&0xe7;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;
	}

	/**
	 * 指南针校准第一步
	 */
	public void setCompassStep1(){

//		Log.e(RoameConstants.ROAMETAG, "setCompassStep1: " );

		flyAction = (flyAction & 0xFF)^0x08;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 指南针校准第二步
	 */
	public void setCompassStep2(){

		flyAction = (flyAction & 0xFF)^0x18;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 加速度计校准第一步
	 */
	public void setAccelerometerStep1(){

		flyAction = (flyAction & 0xFF)^0x20;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 加速度计校准第二步
	 */
	public void setAccelerometerStep2(){

		flyAction = (flyAction & 0xFF)^0x60;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 加速度计校准第三步
	 */
	public void setAccelerometerStep3(){

		flyAction = (flyAction & 0xFF)^0x20;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 加速度计校准第四步
	 */
	public void setAccelerometerStep4(){

		flyAction = (flyAction & 0xFF)^0xe0;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 加速度计校准第五步
	 */
	public void setAccelerometerStep5(){

		flyAction = (flyAction & 0xFF)^0x20;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 加速度计校准第六步
	 */
	public void setAccelerometerStep6(){

		flyAction = (flyAction & 0xFF)^0x60;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 设置为默认的加速度计模式
	 */
	public void setDefaultAccelerometer(){

		flyAction = (flyAction & 0xFF)&0x1f;

		byte temp = (byte) flyAction;

		cmdData[7] = temp;

	}

	/**
	 * 视觉跟踪
	 */
	public void setTrack(int left, int forward){

//		Log.e(RoameConstants.ROAMETAG, "---------setTrack-----------");

		cmdData[1] = (byte)0x36;
		this.left= left;
		this.forward = forward;
		cmdData[2] = (byte) (left & 0xFF);
		cmdData[3] = (byte) (forward & 0xFF);
	}

	/**
	 * 切换为摇杆模式
	 * @param left
	 * @param forward
	 * @param throttle
     * @param roll
     */
	public void setJotstickMode(int left, int forward, int throttle, int roll){
		this.left= left;
		this.forward = forward;
		this.throttle = throttle;
		this.roll = roll;

		cmdData[1] = (byte)0x16;
		cmdData[2] = (byte) (left & 0xFF);
		cmdData[3] = (byte) (forward & 0xFF);
		cmdData[4] = (byte) (throttle & 0xFF);
		cmdData[5] = (byte) (roll & 0xFF);
	}

	/**
	 * 将单个字节转为bit字节数组
	 * @param b
	 * @return
     */
	public static byte[] getBooleanArray(byte b) {
		byte[] array = new byte[8];
		for (int i = 7; i >= 0; i--) {
			array[i] = (byte)(b & 1);
			b = (byte) (b >> 1);
		}
		return array;
	}


}
