package com.iotgroup.roame.customview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.iotgroup.roame.R;
import com.iotgroup.roame.activity.TraceActivity;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.service.impl.FlightControlOI100;


/**
 * Created by huanghao on 2017/2/22.
 */

public class JoystickControl implements  RockerView.SingleRudderListener {

    private final LayoutInflater inflater;

    private TraceActivity mainActivity;

    private View joystickView;

    private View layoutControl;

    /**
     * 左摇杆
     */
    private RockerView leftRocker;

    /**
     * 右摇杆
     */
    private RockerView rightRocker;

    private SendFlightData flightData;

    /***
     * 改变方向
     */
    private boolean changeDirection = false;

    private final FlightControlOI100 flightControlOI100;

    public JoystickControl(TraceActivity activity, View view){
        mainActivity = activity;
        joystickView = view;

        flightControlOI100 = FlightControlOI100.getInstance(activity);

        flightData = flightControlOI100.flightData;

//        Log.e(RoameConstants.ROAMETAG, "JoystickControl------> "+flightData );

//        flightData = new SendFlightData();

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initView();
        initListener();

    }

    private void initListener() {
        flightControlOI100.setOnFlightDataChangesListener(new FlightControlOI100.FlightDataChangesListener() {
            @Override
            public void onDataChange(SendFlightData data) {
                flightData = data;

            }
        });
    }

    private void initView() {

        layoutControl =  joystickView.findViewById(R.id.layout_control);
        layoutControl.setVisibility(View.VISIBLE);

        leftRocker = (RockerView) joystickView.findViewById(R.id.leftRocker);
        rightRocker = (RockerView) joystickView.findViewById(R.id.rightRocker);

        leftRocker.setRockerType(0);
        leftRocker.setSingleRudderListener(this);
        rightRocker.setRockerType(1);
        rightRocker.setSingleRudderListener(this);


    }

    @Override
    public void onSteeringWheelChanged(int rockerType, int action, int angle, float ratio) {

        if(action == RockerView.ACTION_STOP){

            if(rockerType == 0){
                flightData.roll = 128;
                flightData.throttle = 128;
            }else{
                flightData.left = 128;
                flightData.forward = 128;
            }

//            VibrateUtils.shakeStop();

        }else if(action == RockerView.ACTION_RUNNER){

//            VibrateUtils.shakeComplicated(mainActivity, new long[] { 1000, 50, 1000, 50, 1000 },1);

            if(rockerType == 0){

                if (angle >= 83 && angle <= 97) {
                    angle = 90;
                } else if (angle >= 263 && angle <= 277) {
                    angle = 270;
                }

                flightData.throttle = getFlightValue(angle,ratio,1,false);
                flightData.roll = getFlightValue(angle,ratio,0,false);

            }else{

                if (angle >= 83 && angle <= 97) {
                    angle = 90;
                } else if (angle >= 263 && angle <= 277) {
                    angle = 270;
                }

                if(changeDirection){
                    flightData.left = getFlightValue(angle,ratio,0,true);
                    flightData.forward = getFlightValue(angle,ratio,1,true);
                }else{
                    flightData.left = getFlightValue(angle,ratio,0,true);
                    flightData.forward = getFlightValue(angle,ratio,1,false);
                }

            }
        }

//        Log.e(RoameConstants.ROAMETAG,"摇杆方向：rockerType=="+rockerType+"角度：angle=="+angle+"比例：ratio=="+ratio+"左右：left=="+flightData.left+"前后：forward=="+flightData.forward
//                    +"摇杆：roll=="+flightData.roll+"油门：throttle=="+flightData.throttle);

        flightControlOI100.changeFlightData(flightData);

    }

    private static final float radian = (float) (Math.PI / 180.0);

    private static final int BASEVALUE = 128;

    private int getFlightValue(int angle, float ratio, int flag, boolean isReverse) {
        int currentValue = 128;
        int currentAngle = angle;

        if(isReverse){
            currentAngle = angle + 180;
        }

        if(flag == 0){
            //左右飞行
            currentValue = (int) ((ratio * Math.cos(currentAngle * radian)) * BASEVALUE + BASEVALUE);
        }else if(flag == 1){
            //上下飞行
            currentValue = (int) ((ratio * Math.sin(currentAngle * radian)) * BASEVALUE + BASEVALUE);
        }

        if(currentValue > 255){
            currentValue = 255;
        }else if(currentValue < 0){
            currentValue = 0;
        }

        return currentValue;

    }

}
