package com.iotgroup.roame.customview;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iotgroup.roame.R;

public class MyCustomTransparentDialog extends Dialog implements View.OnClickListener{

	private boolean isIconWarnShow;
	private ImageView iv_warning;
	private TextView tv_warning;
	private TextView tv_confirm;
	private TextView tv_cancel;
	private OnTransparentDialogClickListener mDialogClickListener;

	public MyCustomTransparentDialog(Context context) {
		this(context, R.style.TransparentDialog);
	}
	
	/**
	 * 
	 * @param context
	 * @param isIconWarnShow      显示的对话框是否是显示警告图标的类型,true为显示
	 */
	public MyCustomTransparentDialog(Context context, boolean isIconWarnShow) {
		this(context);
		this.isIconWarnShow = isIconWarnShow;
	}
	
	public MyCustomTransparentDialog(Context context, int theme) {
		super(context, theme);
	}

	public MyCustomTransparentDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_warning);
		initView();
		initListener();
	}

	private void initView() {
		iv_warning = (ImageView) findViewById(R.id.icon_iv);
		tv_warning = (TextView) findViewById(R.id.warn_tv);
		tv_confirm = (TextView) findViewById(R.id.sure_tv);
		tv_cancel = (TextView) findViewById(R.id.cancel_tv);
		
		setWarningIcon(R.mipmap.icon_circlewarning);
	}
	
	private void initListener() {
		tv_confirm.setOnClickListener(this);
		tv_cancel.setOnClickListener(this);
	}
	
	/**
	 * 设置警告图标
	 * @param resId  图片的资源ID
	 */
	public void setWarningIcon(int resId){
		if(isIconWarnShow){
			iv_warning.setVisibility(View.VISIBLE);
			iv_warning.setImageResource(resId);
		}
	}
	
	/**
	 * 设置提示的内容
	 * @param text  提示的内容
	 */
	public void setMessage(String text){
		tv_warning.setText(text);
	}
	public void setMessage(int resId){
		tv_warning.setText(resId);
	}
	
	/**
	 * 设置提示内容的颜色
	 * @param color  
	 */
	public void setMessageTextColor(int color){
		tv_warning.setTextColor(color);
	}
	
	/**
	 * 设置确认选项的内容
	 * @param text  确认选项的内容
	 */
	public void setConfirmBtnText(String text){
		tv_confirm.setText(text);
	}
	public void setConfirmBtnText(int resId){
		tv_confirm.setText(resId);
	}
	
	/**
	 * 设置取消选项的内容
	 * @param text  取消选项的内容
	 */
	public void setCancelBtnText(String text){
		tv_cancel.setText(text);
	}
	
	public void setCancelBtnText(int resId){
		tv_cancel.setText(resId);
	}
	
	public void setOnTransparentDialogClickListener(OnTransparentDialogClickListener listener){
		this.mDialogClickListener = listener;
	}
	
	public interface OnTransparentDialogClickListener{
		void onConfirmBtnClick();
		void onCancelBtnClick();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sure_tv:
			if(mDialogClickListener != null){
				mDialogClickListener.onConfirmBtnClick();
			}
			break;

		case R.id.cancel_tv:
			if(mDialogClickListener != null){
				mDialogClickListener.onCancelBtnClick();
			}
			break;
		}
	}
}
