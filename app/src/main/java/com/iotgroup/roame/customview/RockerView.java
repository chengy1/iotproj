package com.iotgroup.roame.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.iotgroup.roame.R;
import com.iotgroup.roame.utils.MathUtils;

/**
 * Created by huanghao on 2017/2/22.
 */

public class RockerView extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder mHolder;
    private Paint mPaint;
    public Point mRockerPosition; // 摇杆位置
    private Point mCtrlPoint;// 摇杆起始位置
    private int mWheelRadius;// 摇杆活动范围半径
    private int wheelRadius;// 摇杆半径
    int isHide = 0;
    private SingleRudderListener listener = null; // 事件回调接口
    Bitmap bitmap, bitmap2;
    private Point origin; // 摇杆原点
    private SensorEventListener mySensorListener;
    private SensorManager sm;
    private Sensor sensor;
    private int width;
    private int height;
    private TextPaint textPaint;
    public static final int ACTION_RUNNER = 1, ACTION_ATTACK_DEVICEMOVE = 2, ACTION_STOP = 3,
            ACTION_ATTACK_CAMERAMOVE = 4;
    /**
     * 摇杆类型 0：左侧 1：右侧
     */
    private int rockerType = 0;

    public RockerView(Context context) {
        super(context);
        init();
    }

    public RockerView(Context context, AttributeSet attribute) {
        super(context, attribute);
        init();
        // regSensor(context);
    }

    public RockerView(Context context, View parent) {
        super(context);
        init();
    }

    private void init() {
        this.setKeepScreenOn(true);
        mHolder = this.getHolder();
        mHolder.addCallback(this);
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setTextSize(18);
        mPaint.setAntiAlias(true);// 抗锯齿

        textPaint = new TextPaint();
        textPaint.setARGB(0xFF, 0xFF, 0xFF, 0xFF);
        textPaint.setTextSize(20.0F);
        textPaint.setAntiAlias(true);

        setFocusable(true);
        setFocusableInTouchMode(true);
        setZOrderOnTop(true);
        mHolder.setFormat(PixelFormat.TRANSPARENT);// 设置背景透明
        bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.joystick_frame);
        bitmap2 = BitmapFactory.decodeResource(getResources(), R.mipmap.joystick_thumb);
        int width = this.getWidth() / 2;
        int height = this.getHeight() / 2;

        wheelRadius = bitmap2.getWidth() / 2;

        mWheelRadius = bitmap.getWidth() / 2;

        mCtrlPoint = new Point(width, height);
        origin = new Point(mCtrlPoint.x + mWheelRadius, mCtrlPoint.y + mWheelRadius);
        mRockerPosition = new Point(origin.x - wheelRadius, origin.y - wheelRadius);
    }

    public int getRockerType() {
        return rockerType;
    }

    public void setRockerType(int rockerType) {
        this.rockerType = rockerType;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        width = View.MeasureSpec.getSize(widthMeasureSpec) / 2;
        height = View.MeasureSpec.getSize(heightMeasureSpec) / 2;

        arc_x = width - wheelRadius;
        arc_y = height - wheelRadius;

        wheelRadius = bitmap2.getWidth() / 2;

        mWheelRadius = bitmap.getWidth() / 2;

        mCtrlPoint = new Point(width - mWheelRadius, height - mWheelRadius);
        origin = new Point(mCtrlPoint.x + mWheelRadius, mCtrlPoint.y + mWheelRadius);
        mRockerPosition = new Point(origin.x - wheelRadius, origin.y - wheelRadius);
        // Canvas_OK();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * 注册重力传感器监听器
     *
     * @param context
     */
    public void regSensor(Context context) {
        // 通过服务得到传感器管理对象
        sm = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);// 得到一个重力传感器实例
        mySensorListener = new SensorEventListener() {
            @Override
            // 传感器获取值发生改变时在响应此函数
            public void onSensorChanged(SensorEvent event) {// 备注1
                // 传感器获取值发生改变，在此处理
                float x = event.values[0]; // 手机横向翻滚
                // x>0 说明当前手机左翻 x<0右翻
                float y = event.values[1]; // 手机纵向翻滚
                // y>0 说明当前手机下翻 y<0上翻
                float z = event.values[2]; // 屏幕的朝向
                // z>0 手机屏幕朝上 z<0 手机屏幕朝下
                arc_x -= x;// 备注2
                arc_y += y;

                if (Math.abs(x) < 0.5 && Math.abs(y) < 0.5) {
                    arc_x = width - wheelRadius;
                    arc_y = height - wheelRadius;
                }
                Canvas_OK_SENSOR(x, y);
            }

            @Override
            // 传感器的精度发生改变时响应此函数
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        sm.registerListener(mySensorListener, sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * 注销重力传感器监听器
     */
    public void unRegSensor() {
        if (sm != null) {
            sm.unregisterListener(mySensorListener);
            mRockerPosition = new Point(origin.x - wheelRadius, origin.y - wheelRadius);
            arc_x = 0;
            arc_y = 0;
            onSteeringWheelChanged(ACTION_STOP, 0, 0);
            Canvas_OK();
            sm = null;
        }
    }

    /**
     * @param action
     *            摇杆动作
     * @param angle
     *            摇杆角度
     * @param ratio
     *            摇杆距离中心点距离与半径的比率
     */
    public void onSteeringWheelChanged(int action, int angle, float ratio) {
        if (listener != null)
            listener.onSteeringWheelChanged(rockerType, action, angle, ratio);
    }

    // 回调接口
    public interface SingleRudderListener {
        /**
         * @param rockerType
         *            摇杆类型 0：左侧 1：右侧
         * @param action
         *            摇杆动作
         * @param angle
         *            摇杆角度
         * @param ratio
         *            摇杆距离中心点距离与半径的比率
         */
        void onSteeringWheelChanged(int rockerType, int action, int angle, float ratio);
    }

    // 设置回调接口
    public void setSingleRudderListener(SingleRudderListener rockerListener) {
        listener = rockerListener;
    }

    int len;
    private float arc_x;
    private float arc_y;
    private int angleFlag;

    /**
     * 边沿阈值
     */
    private float EDGE_VALUE = (float) 1.0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            if (isHide == 0) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        len = MathUtils.getLength(origin.x, origin.y, event.getX(), event.getY());
                        // 如果屏幕接触点不在摇杆挥动范围内,则不处理
                        if (len > mWheelRadius) {
                            return true;
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:

                        len = MathUtils.getLength(origin.x, origin.y, event.getX(), event.getY());
                        float ratio = 0;
                        if (len <= mWheelRadius * EDGE_VALUE) {
                            // 如果手指在摇杆活动范围内，则摇杆处于手指触摸位置
                            mRockerPosition.set((int) event.getX() - wheelRadius, (int) event.getY() - wheelRadius);

                            ratio = len / (float) mWheelRadius;

                        } else {
                            // 设置摇杆位置，使其处于手指触摸方向的 摇杆活动范围边缘
                            mRockerPosition = MathUtils.getBorderPoint(origin,
                                    new Point((int) event.getX(), (int) event.getY()), (int) (mWheelRadius * EDGE_VALUE));
                            mRockerPosition = new Point(mRockerPosition.x - wheelRadius, mRockerPosition.y - wheelRadius);

                            int imaxa = MathUtils.getLength(origin.x, event.getY(), event.getX(), event.getY());
                            int imaxb = MathUtils.getLength(event.getX(), origin.y, event.getX(), event.getY());
                            float fmaxa = 0;
                            float fmaxb = 0;
                            if (imaxa >= mWheelRadius * EDGE_VALUE) {
                                fmaxa = mWheelRadius * EDGE_VALUE;
                            } else {
                                fmaxa = imaxa;
                            }

                            if (imaxb >= mWheelRadius * EDGE_VALUE) {
                                fmaxb = mWheelRadius * EDGE_VALUE;
                            } else {
                                fmaxb = imaxb;
                            }

                            float fmaxc = (float) Math.sqrt(fmaxa * fmaxa + fmaxb * fmaxb);
                            ratio = fmaxc / mWheelRadius * EDGE_VALUE;
                            // ratio = 1;

                        }
                        if (listener != null) {
                            float radian = MathUtils.getRadian(origin, new Point((int) event.getX(), (int) event.getY()));

                            onSteeringWheelChanged(ACTION_RUNNER, getAngleCouvert(radian), ratio);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        mRockerPosition = new Point(origin.x - wheelRadius, origin.y - wheelRadius);
                        onSteeringWheelChanged(ACTION_STOP, 0, 0);
                        break;
                }
                Canvas_OK();
                Thread.sleep(30);
            } else {
                Thread.sleep(40);
            }
        } catch (Exception e) {

        }
        return true;
    }

    // 获取摇杆偏移角度 0-360°
    private int getAngleCouvert(float radian) {
        int tmp = (int) Math.round(radian / Math.PI * 180);
        if (tmp < 0) {
            return -tmp;
        } else {
            return 180 + (180 - tmp);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Canvas_OK();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        unRegSensor();
    }

    // 设置是否隐藏
    public void Hided(int opt) {
        isHide = opt;
        Canvas_OK();
    }

    /**
     * 精确计算文字宽度
     *
     * @param paint
     * @param str
     * @return
     */
    public static int getTextWidth(Paint paint, String str) {
        int iRet = 0;
        if (str != null && str.length() > 0) {
            int len = str.length();
            float[] widths = new float[len];
            paint.getTextWidths(str, widths);
            for (int j = 0; j < len; j++) {
                iRet += (int) Math.ceil(widths[j]);
            }
        }
        return iRet;
    }

/*    public void canvasText(Canvas canvas) {
        if (getRockerType() == 0) {
            String info = getResources().getString(R.string.rising);
            int textWidth = getTextWidth(mPaint, info);
            canvas.drawText(info, mCtrlPoint.x + mWheelRadius - textWidth / 2, mCtrlPoint.y + 30, mPaint);

            info = getResources().getString(R.string.falling);
            textWidth = getTextWidth(mPaint, info);
            canvas.drawText(info, mCtrlPoint.x + mWheelRadius - textWidth / 2, mCtrlPoint.y + 2 * mWheelRadius - 20,
                    mPaint);

            info = getResources().getString(R.string.turn_left);
            textWidth = getTextWidth(mPaint, info);
            StaticLayout layout = new StaticLayout(info, textPaint, 300, Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
            canvas.save();
            canvas.translate(mCtrlPoint.x + 15, mCtrlPoint.y + mWheelRadius - 15);
            layout.draw(canvas);
            canvas.restore();

            // canvas.drawText(info, mCtrlPoint.x + 10 , mCtrlPoint.y +
            // mWheelRadius, mPaint);

            info = getResources().getString(R.string.turn_right);
            textWidth = getTextWidth(mPaint, info);
            layout = new StaticLayout(info, textPaint, 300, Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
            canvas.save();
            canvas.translate(mCtrlPoint.x + 2 * mWheelRadius - textWidth, mCtrlPoint.y + mWheelRadius - 15);
            layout.draw(canvas);
            canvas.restore();

            // canvas.drawText(info, mCtrlPoint.x + 2* mWheelRadius - textWidth
            // - 10 , mCtrlPoint.y + mWheelRadius, mPaint);

        } else {
            String info = getResources().getString(R.string.fly_forward);
            int textWidth = getTextWidth(mPaint, info);
            canvas.drawText(info, mCtrlPoint.x + mWheelRadius - textWidth / 2, mCtrlPoint.y + 30, mPaint);

            info = getResources().getString(R.string.fly_back);
            textWidth = getTextWidth(mPaint, info);
            canvas.drawText(info, mCtrlPoint.x + mWheelRadius - textWidth / 2, mCtrlPoint.y + 2 * mWheelRadius - 20,
                    mPaint);

            info = getResources().getString(R.string.fly_left);
            textWidth = getTextWidth(mPaint, info);
            StaticLayout layout = new StaticLayout(info, textPaint, 300, Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
            canvas.save();
            canvas.translate(mCtrlPoint.x + 15, mCtrlPoint.y + mWheelRadius - 15);
            layout.draw(canvas);
            canvas.restore();

            // canvas.drawText(info, mCtrlPoint.x + 10 , mCtrlPoint.y +
            // mWheelRadius, mPaint);

            info = getResources().getString(R.string.fly_right);
            textWidth = getTextWidth(mPaint, info);
            layout = new StaticLayout(info, textPaint, 300, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
            canvas.save();
            canvas.translate(mCtrlPoint.x + 2 * mWheelRadius - textWidth, mCtrlPoint.y + mWheelRadius - 15);
            layout.draw(canvas);
            canvas.restore();
        }
    }*/

    // 绘制图像
    public void Canvas_OK() {
        Canvas canvas = null;
        try {
            canvas = mHolder.lockCanvas();
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);// 清除屏幕
            // canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, mCtrlPoint.x, mCtrlPoint.y, mPaint);
            // canvas.drawBitmap(bitmap1, mCtrlPoint.x+
            // mWheelRadius-wheelRadius, mCtrlPoint.y+ mWheelRadius-wheelRadius,
            // mPaint);
            canvas.drawBitmap(bitmap2, mRockerPosition.x, mRockerPosition.y, mPaint);
            // canvasText(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                mHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    // 绘制图像
    public void Canvas_OK_SENSOR(float x, float y) {
        Canvas canvas = null;
        try {
            canvas = mHolder.lockCanvas();
            len = MathUtils.getLength(origin.x, origin.y, arc_x, arc_y);

            float ratio = 0;
            ratio = len / (float) mWheelRadius;
            if (len > mWheelRadius * EDGE_VALUE) {
                // 设置摇杆位置，使其处于手指触摸方向的 摇杆活动范围边缘
                mRockerPosition = MathUtils.getBorderPoint(origin, Math.atan2(y, -x), (int) (mWheelRadius * 0.85));
                mRockerPosition = new Point(mRockerPosition.x - wheelRadius, mRockerPosition.y - wheelRadius);
                arc_x = mRockerPosition.x;
                arc_y = mRockerPosition.y;

                ratio = 1;
            }
            if (listener != null) {

                if (arc_x == width - wheelRadius && arc_y == height - wheelRadius) {
                    onSteeringWheelChanged(ACTION_STOP, 0, 0);
                } else {
                    float radian = (float) Math.atan2(y, -x);
                    int angle = getAngleCouvert(radian);
                    onSteeringWheelChanged(ACTION_RUNNER, angle, ratio);

                    if (Math.abs(angleFlag - angle) > 5) {
                        angleFlag = angle;
                        return;
                    } else
                        angleFlag = angle;
                }
            }
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);// 清除屏幕
            // canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, mCtrlPoint.x, mCtrlPoint.y, mPaint);
            // canvas.drawBitmap(bitmap1, mCtrlPoint.x+
            // mWheelRadius-wheelRadius, mCtrlPoint.y+ mWheelRadius-wheelRadius,
            // mPaint);
            canvas.drawBitmap(bitmap2, arc_x, arc_y, mPaint);

//            canvasText(canvas);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                mHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

}
