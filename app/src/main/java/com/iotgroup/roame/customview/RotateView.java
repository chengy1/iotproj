package com.iotgroup.roame.customview;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.iotgroup.roame.utils.DisplayUtils;


/**
 * Created by huanghao on 2016/7/1.
 */
public class RotateView extends RelativeLayout{

    private int mWidth;
    private ImageView mIv;
    private Context context;
    private int mHeight;
    private int mWidthPx;
    private int mHeightPx;
    private RelativeLayout mRl;

    public RotateView(Context context) {
        super(context);
    }

    public RotateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        //获取屏幕宽高
        mWidthPx = DisplayUtils.getWidthPx((Activity) context);
        mHeightPx = DisplayUtils.getHeightPx((Activity) context);

    }

    public RotateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        ViewGroup.LayoutParams params = getLayoutParams();
        params.width = 2 * mWidthPx + 140;
        params.height = 2 * mWidthPx + 140;
        setLayoutParams(params);

        mIv = (ImageView) getChildAt(0);
        mRl = (RelativeLayout) getChildAt(1);
        mWidth = mIv.getMeasuredWidth();
        mHeight = mIv.getMeasuredHeight();

        LayoutParams layoutParams = (LayoutParams) mRl.getLayoutParams();
        layoutParams.width = mWidthPx;
        layoutParams.height = mHeightPx;
        mRl.setLayoutParams(layoutParams);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int h = (mHeight / 2 - mHeightPx / 2);
        mIv.layout(-(mWidth/2 + 70), -h-35, DisplayUtils.getWidthPx((Activity) context) + 70, mHeightPx + h + 35);
        mRl.layout(0,0,mWidthPx,mHeightPx);
    }
}
