package com.iotgroup.roame.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.iotgroup.roame.R;
import com.iotgroup.roame.RoameApplication;
import com.iotgroup.roame.activity.SettingActivity;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.customview.MyCustomTransparentDialog;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.iotgroup.roame.RoameApplication.isNeedReboot;

/**
 * Created by huanghao on 2017/3/14.
 */

public class CompassFragment extends Fragment implements SettingActivity.CompassCalibrationFlagListener, SettingActivity.GetSendFlightChangeListener {

//    @BindView(R.id.my_image_view)
//    public SimpleDraweeView simpleDraweeView;

    @BindView(R.id.my_image_view)
    public ImageView simpleDraweeView;

    @BindView(R.id.tv_compass_calibration_instruction)
    TextView tvCompassCalibrationInstruction;

    @BindView(R.id.tv_start_calibration)
    TextView tvStartCalibration;

    private SendFlightData mflightData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_compass_calibration, container, false);

        ButterKnife.bind(this, view);

        return view;

    }

    SettingActivity settingActivity;

    //当Activity的onCreate方法返回时调用
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        Uri uri = Uri.parse("asset://com.iotgroup.roame/c_horizontal.gif");
//        AbstractDraweeController draweeController = Fresco.newDraweeControllerBuilder()
//                .setAutoPlayAnimations(true).setUri(uri).build();
//        simpleDraweeView.setController(draweeController);

        Glide.with(RoameApplication.getInstance()).load(R.drawable.c_horizontal).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(simpleDraweeView);

        settingActivity= (SettingActivity) getActivity();
        settingActivity.setCompassCalibrationChangeListener(this);
        settingActivity.setSendFlightChangeListener(this);

    }

//    private CompassCalibrationListener compassCalibrationListener;

//    public void setCompassCalibrationListener(CompassCalibrationListener compassCalibrationListener){
//        this.compassCalibrationListener = compassCalibrationListener;
//    }

//    private int calibrationSTep = 0;

    /*****************************************onClick***********************************************/
    @OnClick(R.id.tv_start_calibration)
    public void startCalibration(){//点击校准

//        if (compassCalibrationListener != null){
//            compassCalibrationListener.startCalibration( simpleDraweeView,tvCompassCalibrationInstruction);
//        }
//        if(calibrationSTep == 0){
//            mflightData.setCompassStep1();
//        }else if(calibrationSTep == 1){
//            mflightData.setCompassStep2();
//        }
        mflightData.setCompassStep1();

    }
    /*****************************************onClick***********************************************/

    @Override
    public void onDestroy() {
        super.onDestroy();

        Glide.with(RoameApplication.getInstance()).onDestroy();

    }

    private boolean compassFlag = true;

    private boolean rebootFlag = true;

    private boolean isStartCalibration = false;

    @Override
    public void complassFlagChanged(int flag) {
        //指南针校准 0:等到校准指令 8:step1成功 16:step2成功 24:执行校准指令
//        Log.e(RoameConstants.ROAMETAG, "getReceiveFligtData: compassCalibrationFlag---->"+flag);
        if(flag == 24){
            isStartCalibration = true;
            tvStartCalibration.setText(R.string.calibrating);
        }else if(flag == 8){
            isStartCalibration = true;
            if(compassFlag){
                compassFlag = false;
//                Uri uri = Uri.parse("asset://com.iotgroup.roame/c_vertical.gif");
//                AbstractDraweeController draweeController1 = Fresco.newDraweeControllerBuilder()
//                        .setAutoPlayAnimations(true).setUri(uri).build();
//                simpleDraweeView.setController(draweeController1);
                Glide.with(RoameApplication.getInstance()).load(R.drawable.c_vertical).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(simpleDraweeView);
                tvCompassCalibrationInstruction.setText(R.string.compass_calibration_step2);
                mflightData.setCompassStep2();
            }

        }else if(flag == 16){
            isStartCalibration = false;
            tvStartCalibration.setText(R.string.calibrae_finish);
            tvCompassCalibrationInstruction.setText(R.string.compass_calibration_step3);

            if(rebootFlag){
                rebootFlag = false;
                final MyCustomTransparentDialog dialog = new MyCustomTransparentDialog(settingActivity, false);
                String msg = settingActivity.getResources().getString(R.string.calibrae_finish_reboot);
                dialog.show();
                dialog.setCancelBtnText(R.string.cancel);
                dialog.setConfirmBtnText(R.string.sure);
                dialog.setMessage(msg);
                dialog.setOnTransparentDialogClickListener(new MyCustomTransparentDialog.OnTransparentDialogClickListener() {
                    @Override
                    public void onConfirmBtnClick() {
                        isNeedReboot = true;
                        try {
                            TimeUnit.MILLISECONDS.sleep(180);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onCancelBtnClick() {
                        dialog.dismiss();
                    }
                });

            }

        }else if(flag == 0){
            compassFlag = true;
            rebootFlag = true;
            if(isStartCalibration){
                isStartCalibration = false;
                String errorMsg = settingActivity.getResources().getString(R.string.calibrae_error);
                tvCompassCalibrationInstruction.setText(errorMsg);
                tvStartCalibration.setText(R.string.start_calibration);
                mflightData.setDafaultCompass();
                Glide.with(RoameApplication.getInstance()).load(R.drawable.c_horizontal).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(simpleDraweeView);
            }
        }


    }

    @Override
    public void getSendFlightData(SendFlightData flightData) {
        mflightData = flightData;
    }

//    public interface CompassCalibrationListener{
//        void startCalibration(SimpleDraweeView simpleDraweeView,TextView tvCompassCalibrationInstruction);
//    }

}
