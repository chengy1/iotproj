package com.iotgroup.roame.operate;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import com.icatch.wcmapp3.ExtendComponent.PreviewH264;
import com.icatch.wcmapp3.ExtendComponent.PreviewMjpg;
import com.icatch.wcmapp3.ExtendComponent.VideoPbMjpg;
import com.icatch.wcmapp3.SDKAPI.CameraAction;
import com.icatch.wcmapp3.SDKAPI.CameraProperties;
import com.icatch.wcmapp3.SDKAPI.CameraState;
import com.icatch.wcmapp3.SDKAPI.FileOperation;
import com.icatch.wcmapp3.SDKAPI.PreviewStream;
import com.icatch.wcmapp3.SDKAPI.VideoPlayback;
import com.icatch.wcmapp3.Tool.BitmapDecode;
import com.icatch.wcmapp3.Tool.ConvertTools;
import com.icatch.wcmapp3.Tool.ResolutionConvert;
import com.icatch.wcmapp3.baseItems.Tristate;
import com.icatch.wcmapp3.camera.MyCamera;
import com.icatch.wcmapp3.function.FormatSDCard;
import com.icatch.wcmapp3.function.SettingView;
import com.icatch.wcmapp3.function.WifiCheck;
import com.icatch.wcmapp3.global.App.AppInfo;
import com.icatch.wcmapp3.global.App.ExitApp;
import com.icatch.wcmapp3.global.App.PropertyId;
import com.icatch.wcmapp3.global.App.SettingMenu;
import com.icatch.wcmapp3.global.sdk.SDKEvent;
import com.icatch.wificam.customer.ICatchWificamConfig;
import com.icatch.wificam.customer.ICatchWificamListener;
import com.icatch.wificam.customer.ICatchWificamLog;
import com.icatch.wificam.customer.ICatchWificamPlayback;
import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.ICatchWificamSession;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.type.ICatchCodec;
import com.icatch.wificam.customer.type.ICatchCustomerStreamParam;
import com.icatch.wificam.customer.type.ICatchEvent;
import com.icatch.wificam.customer.type.ICatchEventID;
import com.icatch.wificam.customer.type.ICatchFile;
import com.icatch.wificam.customer.type.ICatchFileType;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchLogLevel;
import com.icatch.wificam.customer.type.ICatchMJPGStreamParam;
import com.icatch.wificam.customer.type.ICatchMode;
import com.icatch.wificam.customer.type.ICatchPreviewMode;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CameraOperate {
	
	public static final int APP_STATE_STILL_PREVIEW = 1;
	public static final int APP_STATE_VIDEO_PREVIEW = 2;
	private static final int APP_STATE_STILL_CAPTURE = 3;
	private static final int APP_STATE_VIDEO_CAPTURE = 4;
	
	private static final int EVENT_VIDEO_PLAY_COMPLETED = 0X111;
	private static final int EVENT_CACHE_STATE_CHANGED = 0x107;
	private static final int EVENT_CACHE_PROGRESS_NOTIFY = 0x108;
	
	private static final int PIMA_DPC_CUSTOME_STR_LED = 0xD826;
	public static final int VIDEO_RESOLUTION = 0xD605;
	public static final int RESETCAMERA_A10 = 0xD811;
	
	private Context context;
	private CameraProperties cameraProperties;
	private CameraAction cameraAction;
	private CameraState cameraState;
	private PreviewStream previewStream;
	private FileOperation fileOperation;
	private ICatchWificamPreview cameraPreviewStreamClint;
	private int curMode = APP_STATE_VIDEO_PREVIEW;
	private Boolean captureDelayMode = false;
	private int cacheTime;
	private PreviewMjpg previewMjpg;
	private PreviewH264 previewH264;
	private Boolean supportStreaming = true;
	private int currentCodec;
	private MediaPlayer videoCaptureStartBeep;
	private MediaPlayer modeSwitchBeep;
	private MediaPlayer stillCaptureStartBeep;
	private MyCamera currentCamera;
	private MyConstants globalInfo;
	private SDKEvent sdkEvent;
//	private FormatSDCardListener listener;
	private boolean isFormatSuccess = false;
	private String videoResolutionArrays[];
	private boolean isDeleteListener = false;
	private ICatchWificamPlayback mphotoClient;
	private VideoPbMjpg videoPbMjpg;
	private ICatchWificamSession session;
	private VideoPlayback videoPlayback;
	private VideoIsEndListener videoIsEndListener;
	private CacheStateChangedListener cacheStateChangedListener;
	private CacheProgressListener cacheProgressListener;
	private ICatchFile playFile;
	private PlaybackCompletedListener listener;
	private PlaybackCacheStateListener listener_one;
	private PlaybackSBProgressChangeListener listener_two;
	private BatteryLevelChangeListener listener_three;
	private BitmapDecode bitmapDecode;
	
	private Handler myHandler = new Handler(){
		
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MyConstants.EVENT_CAPTURE_COMPLETED:
				Tristate ret = Tristate.FALSE;
				if (curMode == APP_STATE_STILL_CAPTURE) {
					ret = changeCameraMode(ICatchPreviewMode.ICH_STILL_PREVIEW_MODE);
				}
				if (ret == Tristate.FALSE) {
					return;
				}
				if (ret == Tristate.NORMAL) {
					aeeStartPreview();
					curMode = APP_STATE_STILL_PREVIEW;
				}
				break;

			case MyConstants.MESSAGE_FORMAT_SD_START:
//				Log.v("FormatStart", "---------------------->>FormatStart");
//				System.out.println("---------------------->>FormatStart");
				break;

			case MyConstants.MESSAGE_FORMAT_SUCCESS:
//				Log.v("FormatSuccess", "---------------------->>FormatSuccess");
//				System.out.println("---------------------->>FormatSuccess");
				isFormatSuccess = true;
				break;
				
			case MyConstants.MESSAGE_FORMAT_FAILED:
//				Log.v("FormatFailed", "---------------------->>FormatFailed");
//				System.out.println("---------------------->>FormatFailed");
				isFormatSuccess = false;
				break;
				
			case MyConstants.MESSAGE_UPDATE_VIDEOPB_BAR:
				if(listener_two != null){
					listener_two.onProgressChange(msg.arg1);
				}
				break;
			
			case MyConstants.EVENT_BATTERY_ELETRIC_CHANGED:
				if(cameraProperties == null){
					cameraProperties = CameraProperties.getInstance();
				}
				int current = cameraProperties.getBatteryElectric();
				if(listener_three != null){
					listener_three.onChange(current);
				}

				break;
				
			case EVENT_VIDEO_PLAY_COMPLETED:
				if(listener != null){
					listener.onCompleted();
				}
				finishTheVideoPage((ICatchFile) msg.obj);
				break;
			
			case EVENT_CACHE_STATE_CHANGED:
				if(listener_one != null){
					listener_one.onReceiveState(msg.arg1);
				}
				break;
				
			default:
				break;
			}
		};
	};
	private static CameraOperate cameraOperate;
	private WifiCheck wifiTool;

	public static CameraOperate getInstance(){
		if(cameraOperate == null){
			cameraOperate = new CameraOperate();
		}
		return cameraOperate;
	}

	public void setMyContext(Context context){
		this.context = context;
	}

	/**
	 * 连接相机
	 */
	public boolean aeeConnectCamera(WifiManager wifiManager){
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		String wifiSsid = wifiInfo.getSSID().replaceAll("\"", "");
		return connectCam(wifiSsid);
	}
	
	/**
	 * 开启预览画面
	 */
	public void startPreview(Activity activity, PreviewMjpg previewMjpg, PreviewH264 previewH264){
		this.previewMjpg = previewMjpg;
		this.previewH264 = previewH264;
		
		currentCamera = MyConstants.getInstance().getCurrentCamera();
		videoCaptureStartBeep = MediaPlayer.create(context, context.getResources().getIdentifier("camera_timer", "raw", context.getPackageName()));
		stillCaptureStartBeep = MediaPlayer.create(context, context.getResources().getIdentifier("captureburst", "raw", context.getPackageName()));
		modeSwitchBeep = MediaPlayer.create(context, context.getResources().getIdentifier("focusbeep", "raw", context.getPackageName()));
		
		initClint();
		initStatus(activity);
	}
	
	public void stopPreview(){
		if (curMode == APP_STATE_STILL_CAPTURE) {
			return;
		}
		
		if(curMode == APP_STATE_VIDEO_CAPTURE){
			boolean b = stopMoiveRecording();
		}
		
		if (stopAppPreview() == false) {
			return;
		}
		
		if (stopMediaStream() == false) {
			return;
		}
		
		deleteTheListener();
	}
	
	public boolean aeeSetCameraMode(int cameraMode){
		if(cameraMode == curMode){
			return false;
		}
		
		if(cameraMode < 1 || cameraMode > 2){
			return false;
		}
		
		if(curMode == APP_STATE_VIDEO_PREVIEW){
			boolean changeSuccess = changeToPhotoMode();
//			if(changeSuccess){
//				currentCamera.getBurst().setValueByPosition(0);
//			}
			return changeSuccess;
		}
		
		if(curMode == APP_STATE_STILL_PREVIEW){
			return changeToNormalVideoMode();
		}
		return false;
	}
	
	public boolean aeeStartRecord(){
		if(curMode == APP_STATE_VIDEO_PREVIEW){
			return startToRecord();
		}
		return false;
	}
	
	public boolean aeeStopRecord(){
		if(curMode == APP_STATE_VIDEO_CAPTURE){
			return stopRecording();
		}
		return false;
	}
	
	public boolean aeeTakePhoto(){
		if(curMode == APP_STATE_STILL_PREVIEW){
			return takePicture();
		}
		return false;
	}
	
	public String aeeGetCurVideoSize(){
		SettingView settingView =  new SettingView(context, currentCamera);
		List<SettingMenu> videoSettingParams = settingView.getVideoSettingParamsFromA10();
		String s = videoSettingParams.get(0).value;
		String str[] = s.split("x")[1].split(" ");
		String currentResolution = str[0] + "P " +  str[1].substring(0, str[1].length() - 3);
		return currentResolution;
	}
	
	public String aeeGetCurPhotoSize(){
		SettingView settingView =  new SettingView(context, currentCamera);
		List<SettingMenu> photoSettingParams = settingView.getPhotoSettingParamsFromA10();
		String photoResolutino = photoSettingParams.get(0).value;
		return photoResolutino;
	}
	
	public int aeeGetCurrentBatteryLever(){
		if(cameraProperties == null){
			cameraProperties = CameraProperties.getInstance();
		}
		return cameraProperties.getBatteryElectric();
	}
	
	public int aeeGetCurrentMode(){
		return curMode;
	}
	
	private void initClint() {
		cameraProperties = CameraProperties.getInstance();
		cameraAction = CameraAction.getInstance();
		cameraState = CameraState.getInstance();
		MyConstants.getInstance().initClint();
		previewStream = PreviewStream.getInstance();
		fileOperation = FileOperation.getInstance();
		cameraPreviewStreamClint = MyConstants.currentpreviewStreamClint;

//		enableSDKLog();
	}

	private void enableSDKLog() {
		String path = null;
		path = Environment.getExternalStorageDirectory().toString() + "/IcatchSportCamera_SDK_Log";
		Environment.getExternalStorageDirectory();
		Log.d("AppStart", "path: " + path);
		ICatchWificamLog.getInstance().setFileLogPath(path);

		Log.d("AppStart", "path: " + path);
		if (path != null) {
			File file = new File(path);
			if (!file.exists()) {
				file.mkdirs();
			}
		}
		ICatchWificamLog.getInstance().setSystemLogOutput(true);
		ICatchWificamLog.getInstance().setFileLogOutput(true);
		ICatchWificamLog.getInstance().setRtpLog(true);
		ICatchWificamLog.getInstance().setPtpLog(true);
		ICatchWificamLog.getInstance().setRtpLogLevel(ICatchLogLevel.ICH_LOG_LEVEL_INFO);
		ICatchWificamLog.getInstance().setPtpLogLevel(ICatchLogLevel.ICH_LOG_LEVEL_INFO);
	}

	public boolean aeeSetPimaCustomLEDState(int state){
//		if(state < 0 || state > 2){
//			return false;
//		}
		return CameraProperties.getInstance().setStringPropertyValue(PIMA_DPC_CUSTOME_STR_LED, String.valueOf(state));
	}
	
	public String aeeGetFlashState(){
		return cameraProperties.getCurrentStringPropertyValue(PIMA_DPC_CUSTOME_STR_LED);
	}
	
	private void initStatus(Activity activity){
		ExitApp.getInstance().addActivity(activity);
		globalInfo = MyConstants.getInstance();
		globalInfo.setCurrentApp(activity);
		if (cameraProperties.cameraModeSupport(ICatchMode.ICH_MODE_VIDEO) == false) {
			curMode = APP_STATE_STILL_PREVIEW;
		} else {
			curMode = APP_STATE_VIDEO_PREVIEW;
		}
		
		sdkEvent = new SDKEvent(myHandler);
		wifiTool = new WifiCheck(activity);
		wifiTool.openConnectCheck();
		wifiTool.checkWifiPolicy();



		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
		sdkEvent.addCustomizeEvent(0x5001);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);
		
		if (cameraProperties.hasFuction(0xD7F0)) {
			if (cameraProperties.setCaptureDelayMode(1))
				captureDelayMode = true;
		}
		cacheTime = cameraProperties.getPreviewCacheTime();
		if (cacheTime < 200) {
			cacheTime = 200;
		}
		
		ICatchWificamConfig.getInstance().setPreviewCacheParam(cacheTime, 200);
		
		initMode();
	}
	
	public void initMode(){
		Tristate ret = Tristate.FALSE;
		if (curMode == APP_STATE_VIDEO_PREVIEW) {
			ret = changeCameraMode(ICatchPreviewMode.ICH_VIDEO_PREVIEW_MODE);
			if (ret == Tristate.FALSE) {
				return;
			}
		}
		
		if (curMode == APP_STATE_STILL_PREVIEW) {
			ret = changeCameraMode(ICatchPreviewMode.ICH_STILL_PREVIEW_MODE);
			if (ret == Tristate.FALSE) {
				return;
			}
		}
		
		if (supportStreaming) {
			aeeStartPreview();
		}
		
		if(sdkEvent != null && isDeleteListener){
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
			sdkEvent.addCustomizeEvent(0x5001);
			sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);
			isDeleteListener = false;
		}
	}
	
	private boolean connectCam(String ssid) {
		MyCamera currentCamera = new MyCamera(context);
		if (currentCamera.getSDKsession().prepareSession() == false) {
			return false;
		}

		if (currentCamera.getSDKsession().checkWifiConnection() == true) {
//			AeeApplication.getInstance().addCamera(currentCamera);
			MyConstants.getInstance().setCurrentCamera(currentCamera);
			currentCamera.initCamera();
			if (CameraProperties.getInstance().hasFuction(PropertyId.CAMERA_DATE)) {
				long time = System.currentTimeMillis();
				Date date = new Date(time);
				SimpleDateFormat myFmt = new SimpleDateFormat("yyyyMMdd HHmmss");
				String temp = myFmt.format(date);
				temp = temp.replaceAll(" ", "T");
				temp = temp + ".0";
				CameraProperties.getInstance().setCameraDate(temp);
			}
			currentCamera.setMyMode(1);
			return true;
		}
		
		return false;
	}
	
	private Tristate changeCameraMode(ICatchPreviewMode previewMode) {
		Tristate ret = Tristate.FALSE;
		String cmd = cameraProperties.getCurrentStreamInfo();
		if (cmd == null) {
			return changeCameraModeNormal(previewMode);
		}

		if (cmd.contains("MJPG") == true) {
			ICatchMJPGStreamParam param = new ICatchMJPGStreamParam(getResolutionWidth(cmd), getResolutionHeigth(cmd), getResolutionBitrate(cmd), 50);

			ret = previewStream.startMediaStream(cameraPreviewStreamClint, param, previewMode);
			if (ret == Tristate.FALSE) {
				
			}else if (ret == Tristate.ABNORMAL) {
				supportStreaming = false;
				previewMjpg.setVisibility(View.GONE);
				previewH264.setVisibility(View.GONE);
			} else {
				supportStreaming = true;
			}
			return ret;
		} else {
			ICatchCustomerStreamParam param = null;
			if (MyConstants.enableSoftwareDecoder) {
				cmd = ResolutionConvert.convert(cmd);
			}
			param = new ICatchCustomerStreamParam(554, cmd);
			ret = previewStream.startMediaStream(cameraPreviewStreamClint, param, previewMode);
			if (ret == Tristate.FALSE) {
				
			}else if (ret == Tristate.ABNORMAL) {
				supportStreaming = false;
				previewMjpg.setVisibility(View.GONE);
				previewH264.setVisibility(View.GONE);
			} else {
				supportStreaming = true;
			}

			return ret;
		}
	}
	
	private Tristate changeCameraModeNormal(ICatchPreviewMode previewMode) {
		ICatchMJPGStreamParam param = new ICatchMJPGStreamParam();
		Tristate ret = Tristate.FALSE;

		ret = previewStream.startMediaStream(cameraPreviewStreamClint, param, previewMode);
		if (ret == Tristate.FALSE) {
			
		}else if (ret == Tristate.ABNORMAL) {
			previewMjpg.setVisibility(View.GONE);
			previewH264.setVisibility(View.GONE);
			supportStreaming = false;
		} else {
			supportStreaming = true;
		}
		return ret;
	}
	
	private void aeeStartPreview() {
		MyConstants constants = MyConstants.getInstance();
		if (previewStream.getCodec(cameraPreviewStreamClint) == ICatchCodec.ICH_CODEC_RGBA_8888) {
			previewMjpg.setVisibility(View.VISIBLE);
			previewMjpg.start(constants.getCurrentCamera());

			if (previewH264 != null) {
				previewH264.setVisibility(View.GONE);
			}
		} else if (previewStream.getCodec(cameraPreviewStreamClint) == ICatchCodec.ICH_CODEC_H264) {
			previewH264.setVisibility(View.VISIBLE);
			previewH264.start(constants.getCurrentCamera());

//			String filePath = Environment.getExternalStorageDirectory().getAbsolutePath();
//			ICatchWificamConfig.getInstance().enableDumpMediaStream(true, filePath);

			if (previewMjpg != null) {
				previewMjpg.setVisibility(View.GONE);
			}
		}
		currentCodec = previewStream.getCodec(cameraPreviewStreamClint);
	}
	
	private boolean startToRecord(){
		curMode = APP_STATE_VIDEO_CAPTURE;
		
		videoCaptureStartBeep.start();
		
		if (startMoiveRecording() == false) {
			curMode = APP_STATE_VIDEO_PREVIEW;
			return false;
		}
		return true;
	}
	
	private boolean startMoiveRecording() {
		boolean retValue = false;
		retValue = cameraAction.startMovieRecord();
		return retValue;
	}
	
	private boolean stopRecording(){
		videoCaptureStartBeep.start();
		if (stopMoiveRecording() == false) {
			return false;
		}
		curMode = APP_STATE_VIDEO_PREVIEW;
		return true;
	}
	
	private boolean stopMoiveRecording() {
		boolean retValue = false;
		retValue = cameraAction.stopVideoCapture();
		return retValue;
	}
	
	private boolean changeToPhotoMode(){
		Tristate ret = Tristate.FALSE;
		
		if (stopAppPreview() == false) {
			return false;
		}
		stopMediaStream();
		ret = changeCameraMode(ICatchPreviewMode.ICH_STILL_PREVIEW_MODE);
		if (ret == Tristate.FALSE) {
			return false;
		}
		curMode = APP_STATE_STILL_PREVIEW;
		if (ret == Tristate.NORMAL) {
			aeeStartPreview();
		}
		modeSwitchBeep.start();
		return true;
	}
	
	private boolean changeToNormalVideoMode(){
		Tristate ret_video = Tristate.FALSE;
		
		if (stopAppPreview() == false) {
			return false;
		}
		stopMediaStream();

		ret_video = changeCameraMode(ICatchPreviewMode.ICH_VIDEO_PREVIEW_MODE);
		if (ret_video == Tristate.FALSE) {
			return false;
		}
		curMode = APP_STATE_VIDEO_PREVIEW;
		if (ret_video == Tristate.NORMAL) {
			aeeStartPreview();
		}
		MyConstants.getInstance().getCurrentCamera().resetVideoSize();
		modeSwitchBeep.start();
		return true;
	}
	
	public boolean stopMediaStream() {
		boolean b = false;
		b = previewStream.stopMediaStream(cameraPreviewStreamClint);
		return b;
	}
	
	public boolean stopAppPreview() {
		boolean retValue = false;
		if (currentCodec == ICatchCodec.ICH_CODEC_RGBA_8888) {
			retValue = previewMjpg.stop();
		} else if (currentCodec == ICatchCodec.ICH_CODEC_H264) {
			retValue = previewH264.stop();
		}
		return retValue;
	}
	
//	public boolean pauseAppPreview(){
//		if(curMode == APP_STATE_STILL_PREVIEW){
//			return stopAppPreview();
//		}else if(curMode == APP_STATE_VIDEO_PREVIEW){
//			boolean b = false;
//			b= stopMediaStream();
//			b = stopAppPreview();
//			return b;
//		}
//		return false;
//	}
	
	public boolean pauseAppPreview(){
		if (curMode == APP_STATE_STILL_PREVIEW || curMode == APP_STATE_VIDEO_PREVIEW){
			if (stopAppPreview() == false) {
				return false;
			}
			if (stopMediaStream() == false) {
				return false;
			}
			
			if(!isDeleteListener){
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
				sdkEvent.delCustomizeEventListener(0x5001);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);
				isDeleteListener = true;
			}
			
			return true;
		}
		return false;
	}
	
//	public void aeeDeleteListener(){
//		if(!isDeleteListener){
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
//			sdkEvent.delCustomizeEventListener(0x5001);
//			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);
//			isDeleteListener = true;
//		}
//	}
	
	public boolean aeeDeleteListener(){
		if (curMode == APP_STATE_STILL_PREVIEW || curMode == APP_STATE_VIDEO_PREVIEW){
			if (stopAppPreview() == false) {
				return false;
			}
			if (stopMediaStream() == false) {
				return false;
			}
			
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
			sdkEvent.delCustomizeEventListener(0x5001);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);
			isDeleteListener = true;
			return true;
		}
		return false;
	}
	
	public String aeeGetCameraVersion(){
		return AppInfo.getAppVer();
	}
	
	private boolean takePicture(){
		curMode = APP_STATE_STILL_CAPTURE;
		stopAppPreview();
		previewStream.stopMediaStream(MyConstants.getInstance().getCurrentCamera().getpreviewStreamClient());
		boolean b = CameraAction.getInstance().capturePhoto();
		return b;
	}
	
	private void deleteTheListener() {
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
		sdkEvent.delCustomizeEventListener(0x5001);
		sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);

		stopPreview();

		MyConstants.getInstance().getCurrentCamera().destroyCamera();
		stopMediaStream();
	}

	public void aeeExitCamera(){
		if(sdkEvent != null && !isDeleteListener){
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
			sdkEvent.delCustomizeEventListener(0x5001);
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);
		}
		stopPreview();
		MyConstants.getInstance().getCurrentCamera().destroyCamera();
		stopMediaStream();
		wifiTool.cancelConnectCheck();
		sdkEvent = null;
		isDeleteListener = true;
	}

	public int aeeGetRemainImageNum(){
		if(cameraProperties == null){
			cameraProperties = CameraProperties.getInstance();
		}
		return cameraProperties.getRemainImageNum();
	}
	
	public int aeeGetRecordingRemainTime(){
		if(cameraProperties == null){
			cameraProperties = CameraProperties.getInstance();
		}
		return cameraProperties.getRecordingRemainTime();
	}
	
	public boolean aeeCheckSDCardFull(){
		int i = 0;
		if(curMode == APP_STATE_STILL_PREVIEW){
			i = aeeGetRemainImageNum();
		}else if(curMode == APP_STATE_VIDEO_PREVIEW || curMode == APP_STATE_VIDEO_CAPTURE){
			i = aeeGetRecordingRemainTime();
		}
		
		if(i >= 1){
			return false;
		}else{
			return true;
		}
	}
	
	public String[] aeeGetVideoResolutionArray(){
		List<String> videoResolutionList = CameraProperties.getInstance().getSupportedStringPropertyValues(VIDEO_RESOLUTION);
		videoResolutionArrays = new String[videoResolutionList.size()];
		for (int i = 0; i < videoResolutionArrays.length; i++) {
			videoResolutionArrays[i] = videoResolutionList.get(i) + "fps";
		}
		return videoResolutionArrays;
	}
	
	public String[] aeeGetImageSizeArray(){
		MyCamera myCurrentCamera = MyConstants.getInstance().getCurrentCamera();
		String[] str = myCurrentCamera.getImageSize().getValueArrayString();
		return str;
	}
	
	public boolean aeeGetSDCardExist(){
		return CameraProperties.getInstance().isSDCardExist();
	}
	
	public boolean aeeFormatSD(){
		if(aeeGetSDCardExist()){
			FormatSDCard formatSDCard = new FormatSDCard(myHandler);
			formatSDCard.start();
			SystemClock.sleep(3000);
		}
		return isFormatSuccess;
	}
	
	public boolean aeeResetCamera(){
//		boolean b = CameraProperties.getInstance().setStringPropertyValue(RESETCAMERA_A10, "1");
//		if(b){
//			aeeShutdownCamera();
//		}
		return false;
	}
	
	public void aeeShutdownCamera(){
		CameraProperties.getInstance().SetCameraPowerOff();
	}
	
	public boolean aeeModifyWifiPassword(String strPwd){
		if(strPwd == null || strPwd.length() > 10 || strPwd.length() < 8){
			return false;
		}
		
		String password = CameraProperties.getInstance().getCameraPassword();
		if(!password.equals(strPwd)){
			boolean b = CameraProperties.getInstance().setCameraPassword(strPwd);
			return b;
		}
		return false;
	}
	
	public boolean aeeSetPhotoSizeWithSelectId(int idx){
		MyCamera camera = MyConstants.getInstance().getCurrentCamera();
		return camera.getImageSize().setValueByPosition(idx);
	}
	
	public boolean aeeSetCameraSizeWithSelectId(int idx){
		String videoResolution = videoResolutionArrays[idx].substring(0, videoResolutionArrays[idx].length()-3);
		return CameraProperties.getInstance().setStringPropertyValue(VIDEO_RESOLUTION, videoResolution);
	}
	
	public boolean aeeSetCameraSizeWithSelectId(String[] videoResolutionArray, int idx){
		String videoResolution = videoResolutionArray[idx].substring(0, videoResolutionArray[idx].length()-3);
		return CameraProperties.getInstance().setStringPropertyValue(VIDEO_RESOLUTION, videoResolution);
	}
	
	public int aeeGetFileListCountWithFileType(int iFileType){
		FileOperation fileOperation = FileOperation.getInstance();
		if(iFileType == 0x01){
			List<ICatchFile> imageFileList = fileOperation.getFileList(ICatchFileType.ICH_TYPE_IMAGE);
			return imageFileList.size();
		}else if(iFileType == 0x02){
			List<ICatchFile> videoFileList = fileOperation.getFileList(ICatchFileType.ICH_TYPE_VIDEO);
			return videoFileList.size();
		}else{
			return -1;
		}
	}
	
	public List<ICatchFile> aeeGetCameraImageFile(){
		FileOperation fileOperation = FileOperation.getInstance();
		return fileOperation.getFileList(ICatchFileType.ICH_TYPE_IMAGE);
	}
	
	public List<ICatchFile> aeeGetCameraVideoFile(){
		FileOperation fileOperation = FileOperation.getInstance();
		return fileOperation.getFileList(ICatchFileType.ICH_TYPE_VIDEO);
	}
	
	public List<ICatchFile> aeeGetCameraAllFile(){
		FileOperation fileOperation = FileOperation.getInstance();
		return fileOperation.getFileList(ICatchFileType.ICH_TYPE_ALL);
	}
	
	public Bitmap aeeGetCameraFileThumbnail(List<ICatchFile> list, int position){
		Bitmap bitmap = null;
		MyCamera camera = null;
		camera = MyConstants.getInstance().getCurrentCamera();
		if(camera == null){
			camera = new MyCamera(context);
		}
		
		ICatchWificamSession sdkSession = camera.getSDKsession().getSDKSession();
		try {
			ICatchWificamPlayback photoClient = sdkSession.getPlaybackClient();
			bitmap = downloadBitmap(photoClient, list.get(position));
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	private Bitmap downloadBitmap(ICatchWificamPlayback mphotoClient, ICatchFile file1) {  
        Bitmap bitmap =  null ;  
		ICatchFrameBuffer buffer = null;
		
		try {
			buffer = mphotoClient.getThumbnail(file1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		int datalength =buffer.getFrameSize();
		if (datalength > 0) {
			bitmap = BitmapFactory.decodeByteArray(buffer.getBuffer(), 0, datalength);								
		}

        return  bitmap;  
    }
	
	public Bitmap aeeGetSingleImageBitmap(ICatchFile file){
		fileOperation = FileOperation.getInstance();
		if(bitmapDecode == null){
			bitmapDecode = new BitmapDecode();
		}
		ICatchFrameBuffer buffer = fileOperation.getQuickview(file);
		Bitmap bitmap = bitmapDecode.decodeSampledBitmapFromByteArray(buffer.getBuffer(), 0, buffer.getFrameSize(),
				context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
		return bitmap;
	}
	
	public boolean aeeGetSupportVideoPlayback(){
		CameraProperties properties = CameraProperties.getInstance();
		return properties.supportVideoPlayback();
	}
	
	public void aeeInitVideoPlayback(VideoPbMjpg myVideoPbMjpg){
		this.videoPbMjpg = myVideoPbMjpg;
		destroySurface();
	}
	
	public boolean aeeVideoPlaybackStart(ICatchFile file){
		if(file.getFileType() == ICatchFileType.ICH_TYPE_VIDEO){
			this.playFile = file;
			videoPlayback = VideoPlayback.getInstance();
			if(videoPlayback.startPlaybackStream(file)){
				destroySurface();
				return startVideoPb(file);
			}else{
				return false;
			}
		}
		return false;
	}
	
	public boolean aeeVideoPlaybackPause(){
		return videoPlayback.pausePlayback();
	}
	
	public boolean aeeVideoPlaybackResume(){
		return videoPlayback.resumePlayback();
	}
	
	public boolean aeeVideoPlaybackStop(){
		boolean b = false;
		if(playFile.getFileType() == ICatchFileType.ICH_TYPE_VIDEO){
			b = videoPlayback.stopPlaybackStream();
			b = stopThread();
			
			if (cacheStateChangedListener != null) {
				cameraAction.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_CHANGED, cacheStateChangedListener);
				cacheStateChangedListener = null;
			}
			if (cacheProgressListener != null) {
				cameraAction.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_PROGRESS, cacheProgressListener);
				cacheProgressListener = null;
			}
			if (videoIsEndListener != null) {
				cameraAction.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_STREAM_PLAYING_ENDED, videoIsEndListener);
				videoIsEndListener = null;
			}
		}
		return b;
	}
	
	private void destroySurface() {
		if(videoPbMjpg != null){
			videoPbMjpg.destorySurface();
		}
	}
	
	private boolean startVideoPb(ICatchFile file) {
		videoPbMjpg.startSurface();
		MyCamera myCamera = MyConstants.getInstance().getCurrentCamera();
		if(myCamera == null){
			myCamera = new MyCamera(context);
		}
		boolean b = videoPbMjpg.start(myCamera);
		if(b){
			initVideoPbMjpgListener();
			videoIsEndListener = new VideoIsEndListener(file);
			cacheStateChangedListener = new CacheStateChangedListener();
			cacheProgressListener = new CacheProgressListener();
			cameraAction = CameraAction.getInstance();
			cameraAction.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_STREAM_PLAYING_ENDED, videoIsEndListener);
			cameraAction.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_CHANGED, cacheStateChangedListener);
			cameraAction.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_PROGRESS, cacheProgressListener);
			videoPlayback.videoSeek(0);
		}
		return b;
	}
	
	public int aeeGetVideoDuration(){
		return videoPlayback.getVideoDuration();
	}
	
	public String aeeFormatVideoTime(int time){
		return ConvertTools.secondsToHours(time / 100);
	}
	
	public boolean aeeDeleteFile(ICatchFile file){
		FileOperation fileOperation = FileOperation.getInstance();
		return fileOperation.deleteFile(file);
	}
	
	public boolean aeeDownloadCameraFile(ICatchFile file, String localPath){
		FileOperation fileOperation = FileOperation.getInstance();
		return fileOperation.downloadFile(file, localPath);
	}
	
	public long aeeGetSingleDownloadProgress(ICatchFile iCatchFile, String localPath){
		File file = new File(localPath);
		if(file.exists()){
			long fileLength = file.length();
			if(fileLength == iCatchFile.getFileSize()){
				return 100;
			}else{
				return file.length() * 100 / iCatchFile.getFileSize();
			}
		}else{
			return 0;
		}
	}
	
	public boolean aeeCancelCurrentDownloadFile(){
		FileOperation fileOperation = FileOperation.getInstance();
		boolean cancelDownload = fileOperation.cancelDownload();
		return cancelDownload;
	}
	
	public boolean aeeVideoSeekTo(int seekBarProgress){
		VideoPlayback videoPlayback = VideoPlayback.getInstance();
		return videoPlayback.videoSeek(seekBarProgress / 100.0);
	}
	
	private int getResolutionWidth(String resolution) {
		String temp = resolution;
		temp = temp.replace("MJPG?W=", "");
		temp = temp.replace("&H=", " ");
		temp = temp.replace("&BR=", " ");
		temp = temp.replace("&", " ");
		String[] tempArray = temp.split(" ");
		return Integer.parseInt(tempArray[0]);
	}
	
	private int getResolutionHeigth(String resolution) {
		String temp = resolution;
		temp = temp.replace("MJPG?W=", "");
		temp = temp.replace("&H=", " ");
		temp = temp.replace("&BR=", " ");
		temp = temp.replace("&", " ");
		String[] tempArray = temp.split(" ");
		return Integer.parseInt(tempArray[1]);
	}
	
	private int getResolutionBitrate(String resolution) {
		String temp = resolution;
		temp = temp.replace("MJPG?W=", "");
		temp = temp.replace("&H=", " ");
		temp = temp.replace("&BR=", " ");
		temp = temp.replace("&", " ");
		String[] tempArray = temp.split(" ");
		int bitrate = Integer.parseInt(tempArray[2]);
		return bitrate;
	}
	
	private boolean stopThread() {
		boolean b = false;
		b = videoPlayback.stopPlaybackStream();
		destroySurface();
		if(videoPbMjpg != null){
			b = videoPbMjpg.stop();
		}
		return b;
	}
	
	private void finishTheVideoPage(ICatchFile iCatchFile){
		if(iCatchFile.getFileType() == ICatchFileType.ICH_TYPE_VIDEO){
			videoPlayback.stopPlaybackStream();
			stopThread();
			
			if (cacheStateChangedListener != null) {
				cameraAction.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_CHANGED, cacheStateChangedListener);
				cacheStateChangedListener = null;
			}
			if (cacheProgressListener != null) {
				cameraAction.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_PROGRESS, cacheProgressListener);
				cacheProgressListener = null;
			}
			if (videoIsEndListener != null) {
				cameraAction.delEventListener(ICatchEventID.ICH_EVENT_VIDEO_STREAM_PLAYING_ENDED, videoIsEndListener);
				videoIsEndListener = null;
			}
		}
	}
	
	private class VideoIsEndListener implements ICatchWificamListener {
		
		private ICatchFile file;
		public VideoIsEndListener(ICatchFile file){
			this.file = file;
		}
		
		@Override
		public void eventNotify(ICatchEvent arg0) {
			Message message = myHandler.obtainMessage();
			message.what = EVENT_VIDEO_PLAY_COMPLETED;
			message.obj = file;
			message.sendToTarget();
		}
	}
	
	private class CacheStateChangedListener implements ICatchWificamListener {
		@Override
		public void eventNotify(ICatchEvent arg0) {
			myHandler.obtainMessage(EVENT_CACHE_STATE_CHANGED, arg0.getIntValue1(), 0).sendToTarget();
		}
	}
	
	private class CacheProgressListener implements ICatchWificamListener {
		@Override
		public void eventNotify(ICatchEvent arg0) {
			int temp = new Double(arg0.getDoubleValue1() * 100).intValue();
			myHandler.obtainMessage(EVENT_CACHE_PROGRESS_NOTIFY, arg0.getIntValue1(), temp).sendToTarget();
		}
	}
	
	private void initVideoPbMjpgListener() {
		videoPbMjpg.addVideoPbUpdateBarLitener(new VideoPbMjpg.VideoPbUpdateBarLitener() {

			@Override
			public void updateBar(double pts) {
				int temp = new Double(pts * 100).intValue();
				myHandler.obtainMessage(MyConstants.MESSAGE_UPDATE_VIDEOPB_BAR, temp, 0).sendToTarget();
			}
			
		});
	}
	
	public void setOnPlaybackCompletedListener(PlaybackCompletedListener listener){
		this.listener = listener;
	}
	
	public void setOnPlaybackCacheStateListener(PlaybackCacheStateListener listener){
		this.listener_one = listener;
	}
	
	public void setOnPlaybackSBProgressChangeListener(PlaybackSBProgressChangeListener listener){
		this.listener_two = listener;
	}
	
	public void setOnBatteryLevelChangeListener(BatteryLevelChangeListener listener){
		this.listener_three = listener;
	}
	
	public interface PlaybackCompletedListener{
		void onCompleted();
	}
	
	public interface PlaybackCacheStateListener{
		void onReceiveState(int state);
	}
	
	public interface PlaybackSBProgressChangeListener{
		void onProgressChange(int progress);
	}
	
	public interface BatteryLevelChangeListener{
		void onChange(int level);
	}
}
