package com.iotgroup.roame.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.iotgroup.roame.constants.RoameConstants;
import com.iotgroup.roame.service.impl.FlightControlOI100;
import com.iotgroup.roame.service.impl.FlightDataReceive;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 
 * 控制后台服务
 * @author huanghao
 *
 */
public class FlightControlOI100Service extends Service {

	private FlightControlOI100 flightControlOI100;
	private FlightDataReceive flightDataReceive;
	private Timer timer;

	@Override
	public void onCreate() {
		
		super.onCreate();

		flightControlOI100 = FlightControlOI100.getInstance(FlightControlOI100Service.this);
		flightControlOI100.connectServer();

		flightDataReceive = FlightDataReceive.getInstance(FlightControlOI100Service.this);

		timer = new Timer();
		timer.schedule(new ReceiveTask(), (long) (1000*1.5));

	}

	class ReceiveTask extends TimerTask{

		@Override
		public void run() {
			Log.e(RoameConstants.ROAMETAG, "TimerTask" );
			flightDataReceive.connectServer();
			timer.cancel();
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	
	class ControlBinder extends Binder{
		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		flightControlOI100.isServerRun = false;
		flightControlOI100.disConnectScoket();
		flightDataReceive.isServerRun = false;
		flightDataReceive.disConnectSocket();
	}

}
