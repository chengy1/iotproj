package com.iotgroup.roame.service.impl;

import android.content.Context;
import android.util.Log;

import com.iotgroup.roame.bean.EventMsg;
import com.iotgroup.roame.bean.RebootFlyData;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.constants.FlyStatus;
import com.iotgroup.roame.constants.RoameConstants;
import com.iotgroup.roame.utils.Hex;
import com.iotgroup.roame.utils.WifiManagerUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

import static com.iotgroup.roame.RoameApplication.isNeedReboot;

public class FlightControlOI100 {

    private static FlightControlOI100 flightControls;

    private FlightDataChangesListener listener;

    private static Context mContext;



    public static FlightControlOI100 getInstance(Context contex) {
        if (flightControls == null) {
            synchronized (FlightControlOI100.class) {
                if (flightControls == null) {

                    flightControls = new FlightControlOI100();

                    mContext = contex;
                }
            }
        }

        return flightControls;
    }

    public boolean isServerRun = false;

    private Socket socket;

    private String host = "192.168.1.1";

    private int port = 8888;

    /**
     * socket是否正确连接
     */
    private boolean isConnectSocket = false;

    private InputStream is;

    private OutputStream os;

    public SendFlightData flightData;

    private boolean flag = true;

    public void connectServer() {
        new Thread(new Runnable() {

            private WifiManagerUtils wifiManage;

            @Override
            public void run() {

                isServerRun = true;


                while (isServerRun) {

                    wifiManage = new WifiManagerUtils(mContext);

                    if (wifiManage.getWifiStatus()) {

                        //TODO 根据wifi的SSID判断是否已经连接正确的Wifi

                        //初始化连接端口
                        if (!isConnectSocket) {

                            isConnectSocket = connectSocket();

                            if (isConnectSocket) {
                                //端口连接成功
                                if (flightData == null) {
                                    flightData = new SendFlightData();
                                    if (listener != null) {
                                        listener.onDataChange(flightData);
                                    }


                                    EventBus.getDefault().post(new EventMsg(flightData));

                                }
                            } else {
                                //端口连接失败


                            }
                        } else {

                            if(isNeedReboot){
                                RebootFlyData rebootFlyData = new RebootFlyData();
                                rebootFlyBoard(rebootFlyData);
                            }else{
                                heartBeat(flightData);
                            }


                            try {
                                TimeUnit.MILLISECONDS.sleep(31);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                        flag = true;
                    } else {
                        //Wifi状态异常
                        if (flag) {
                            flag = false;
                            EventBus.getDefault().post(FlyStatus.DISCONNECTED);
                        }


                    }


                }
            }
        }).start();
    }

    /**
     * 重启飞控系统
     * @param rebootFlyData
     */
    private void rebootFlyBoard(RebootFlyData rebootFlyData) {
        if (socket != null && !socket.isClosed() && socket.isConnected()) {

            byte[] cmdData = rebootFlyData.getCmdData();

            boolean isSendCommands = sendCommands(cmdData);

            if (isSendCommands) {
                //发生成功
                isNeedReboot = false;

            } else {
                //发送失败
                sendErro++;

                if (sendErro >= 8) {

                    sendErro = 0;
                    isConnectSocket = false;
                }

            }

        } else {
            //异常情况
            isConnectSocket = false;
        }
    }

    int sendErro = 0;

    /**
     * 发送心跳数据
     *
     * @param flightData 需要注意根据socket.isClosed()和socket.isConnected()
     *                   方法并不能有效判断端口是已经断开 ，需要考虑是否发送sendUrgentData(0xFF)
     *                   如果发生异常，代表端口已经处于异常断开状态
     *                   <p>
     *                   或是通过连续发送数据，多少次数据没有发送成功，认为端口默认已经断开
     */
    private void heartBeat(SendFlightData flightData) {
        if (socket != null && !socket.isClosed() && socket.isConnected()) {

            byte[] cmdData = flightData.getCmdData();

            boolean isSendCommands = sendCommands(cmdData);

            if (isSendCommands) {
                //发生成功


            } else {
                //发送失败
                sendErro++;

                if (sendErro >= 8) {

                    sendErro = 0;
                    isConnectSocket = false;
                }

            }

        } else {
            //异常情况
            isConnectSocket = false;
        }
    }

    /**
     * 发送控制数据
     *
     * @param cmdData 控制参数
     */
    private synchronized boolean sendCommands(byte[] cmdData) {

        try {
            if (os != null && socket != null) {
//				  Log.e(RoameConstants.ROAMETAG, "sendCommands: ------->cmdData"+ Arrays.toString(cmdData));
//                Log.e(RoameConstants.ROAMETAG, " reciveData:" + Hex.printStringDivision(Hex.encodeHexStr(cmdData).toUpperCase(), 2));
                os = socket.getOutputStream();
                os.write(cmdData);
                os.flush();
                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Socket 连接
     */
    private boolean connectSocket() {

        disConnectScoket();

        try {
            TimeUnit.MILLISECONDS.sleep(128);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        try {
            socket = new Socket();
            socket.setTcpNoDelay(true);
            socket.connect(new InetSocketAddress(host, port), 3800);
            socket.setKeepAlive(true);

            if (socket != null && !socket.isClosed() && socket.isConnected()) {

                is = socket.getInputStream();
                os = socket.getOutputStream();

                isConnectSocket = true;
            } else {
                isConnectSocket = false;
            }
        } catch (Exception e) {
            isConnectSocket = false;
            e.printStackTrace();
        }

        try {
            TimeUnit.MILLISECONDS.sleep(58);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return isConnectSocket;
    }

    public void changeFlightData(SendFlightData flightData) {

        if (flightData == null) {
            flightData = new SendFlightData();
        }

        flightData.setFlightData(flightData.left, flightData.forward, flightData.throttle, flightData.roll);

    }

    /**
     * 断开Sokcet连接
     */
    public void disConnectScoket() {

        try {
            if (is != null) {
                is.close();
                is = null;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try {
            if (os != null) {
                os.close();
                os = null;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }
    }

    public void setOnFlightDataChangesListener(FlightDataChangesListener listener) {
        this.listener = listener;
    }

    public interface FlightDataChangesListener {
        void onDataChange(SendFlightData data);
    }

}
