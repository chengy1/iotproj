package com.iotgroup.roame.service.impl;

import android.content.Context;
import android.util.Log;

import com.iotgroup.roame.bean.ReceiveFlightData;
import com.iotgroup.roame.constants.RoameConstants;
import com.iotgroup.roame.utils.FlightDataInputStream;
import com.iotgroup.roame.utils.Hex;
import com.iotgroup.roame.utils.WifiManagerUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

/**
 * Created by huanghao on 2017/3/7.
 */

public class FlightDataReceive {

    private static FlightDataReceive flightDataReceive;

    private static Context mContext;

    private InputStream is;

    private OutputStream os;

    public static FlightDataReceive getInstance(Context contex) {
        if (flightDataReceive == null) {
            synchronized (FlightDataReceive.class) {
                if (flightDataReceive == null) {
                    flightDataReceive = new FlightDataReceive();

                    mContext = contex;
                }
            }
        }
        return flightDataReceive;
    }

    public boolean isServerRun = false;

    /**
     * socket是否正确连接
     */
    private boolean isConnectSocket = false;

    private String host = "192.168.1.1";

    private int port = 8889;


    private Socket socket;

    public void connectServer() {
        new Thread(new Runnable() {

            private WifiManagerUtils wifiManage;

            @Override
            public void run() {

                isServerRun = true;


                while (isServerRun) {

                    wifiManage = new WifiManagerUtils(mContext);

                    if (wifiManage.getWifiStatus()) {
                        //wifi可用
                        if (!isConnectSocket) {

                            //连接接收端口
                            isConnectSocket = connectSocket();

//                            Log.e(RoameConstants.ROAMETAG, "run: isConnectSocket->" + isConnectSocket);

                            if (isConnectSocket) {
                                //连接成功
                                receiveFlightData();
                            } else {
                                //连接失败

                            }

                        } else {

                            //接收处理


                        }


                    } else {
                        //wifi状态异常

                    }

                }
            }
        }).start();
    }

    FlightDataInputStream fis;

    /**
     * 接收飞控回传的数据
     */
    private void receiveFlightData() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                while (isServerRun) {

                    if (is == null && socket != null) {
                        try {
                            is = socket.getInputStream();
                        } catch (IOException e) {
                            isConnectSocket = false;
                            e.printStackTrace();
                        }
                    }

                    if (fis == null) {
                        fis = new FlightDataInputStream(is);
                    }

                    byte[] validFlightData = fis.getValidFlightData();

//                    Log.e(RoameConstants.ROAMETAG, " validFlightData:" + Hex.printStringDivision(Hex.encodeHexStr(validFlightData).toUpperCase(), 2));

                    EventBus.getDefault().post(new ReceiveFlightData(validFlightData));

                }


//                while (isServerRun){
//                    if(is == null && socket!=null){
//                        try {
//                            is = socket.getInputStream();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    int availen = 0;
//                    int readlen = 0;
//
//                    Log.e(RoameConstants.ROAMETAG, "----receiveFlightData----" );
//
//                    for(;isServerRun;){
//                        try {
//                            availen = is.available();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        byte reciveData[] = new byte[availen];
//                        readlen = 0;
//                        try {
//                            readlen += is.read(reciveData,readlen,availen);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        if(readlen>0){
//                            Log.e(RoameConstants.ROAMETAG, " reciveData:"+ Hex.printStringDivision(Hex.encodeHexStr(reciveData).toUpperCase(), 2));
//                            Log.e(RoameConstants.ROAMETAG, "receiveFlightData: reciveData:"+ Arrays.toString(reciveData));
//                            getValidFlightData();
//
//                        }
//
//                    }
//                }


            }
        }).start();


    }

    /**
     * Socket 连接
     */
    private boolean connectSocket() {

        disConnectSocket();

        try {
            TimeUnit.MILLISECONDS.sleep(128);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        try {
            socket = new Socket();
            socket.setTcpNoDelay(true);
            socket.connect(new InetSocketAddress(host, port), 3800);
            socket.setKeepAlive(true);

            if (socket != null && !socket.isClosed() && socket.isConnected()) {

                is = socket.getInputStream();
                os = socket.getOutputStream();

                isConnectSocket = true;
            } else {
                isConnectSocket = false;
            }
        } catch (Exception e) {
            isConnectSocket = false;
            e.printStackTrace();
        }

        try {
            TimeUnit.MILLISECONDS.sleep(58);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return isConnectSocket;
    }

    /**
     * 断开Sokcet连接
     */
    public void disConnectSocket() {

        try {
            if (is != null) {
                is.close();
                is = null;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try {
            if (os != null) {
                os.close();
                os = null;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }
    }

}
