package com.iotgroup.roame.track;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.example.lib_gui.DragRectView;
import com.example.lib_gui.DrawRectView;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by isaac on 16/12/12.
 */

public abstract class DetectorViewHelper {


    String TAG = "DetectorViewHelper";
    DragRectView dragRectView;
    DrawRectView drawRectView;
    Lock lock;
    Handler handler;

    boolean working = true;

    Handler mBackgroundHandler;
    HandlerThread mBackgroundThread;

    /**
     * 启动供跟踪器运行的后台线程
     * 建议运行在Activity.onResume()
     */
    public void startBackgroundThread() {

        mBackgroundThread = new HandlerThread("CamBgt");//may have prob here, try to use a variable name
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
        Log.v(TAG, String.valueOf(mBackgroundHandler == null));
        Log.v(TAG, String.valueOf(mBackgroundThread == null));
    }

    /**
     * 关闭后台线程
     * 建议运行在Activity.onPause()
     */
    public void stopBackgroundThread() {

        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    void init() {
        lock = new ReentrantLock();
    }


    /**
     * 清屏
     */
    void cleanDrawRectView() {
        handler.post(new Runnable() {
            @Override
            public void run() {

                try {
                    drawRectView.cleanScreen();
                } catch (IllegalStateException e) {
                }

            }
        });
    }


}