package com.iotgroup.roame.track;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.curi.tracker.lib.BaseTracker;
import com.curi.tracker.lib.TrackerX;
import com.example.lib_gui.DragRectView;
import com.example.lib_gui.DrawRectView;
import com.iotgroup.roame.bean.SendFlightData;
import com.iotgroup.roame.constants.RoameConstants;

import java.nio.ByteBuffer;

/**
 * Created by isaac.    <br/>
 * 此类绑定Tracker、手选画框View、结果显示框View和后台进程<br/>
 * 使用方法为：<br/>
 * 1.在UI线程实例化本类;<br/>
 * 2.在UI线程设置startBackgroundThread和stopBackgroundThread方法;<br/>
 * 3.使用方法setTrackerMode设置跟踪器输入的图像格式<br/>
 * 4.使用方法doWork实现逐帧目标跟踪<br/>
 */

public class TrackerViewHelper extends DetectorViewHelper {


    SendFlightData flightData;

    String TAG = "TrackerViewHelper";
    TrackerX tracker= null;

    public  Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            if(msg.what == 123){
                flightData  = (SendFlightData) msg.obj;
                Log.e(RoameConstants.ROAMETAG, "getSendFlightData333333: "+flightData );
            }
        }
    };

    /**
     * 设置跟踪器模式
     * see BaseTracker.TrackerMode
     * @param trackerMode
     */
    public void setTrackerMode(BaseTracker.TrackerMode trackerMode) {
        this.trackerMode = trackerMode;
    }

    private BaseTracker.TrackerMode trackerMode= BaseTracker.TrackerMode.SPEED_UP_YUV420;

    /**
     * 类构造器
     * @param dragRectView  手画框View
     * @param drawRectView  结果显示框View
     * @param handler   UI线程句柄
     */
    public TrackerViewHelper(DragRectView dragRectView, DrawRectView drawRectView, Handler handler) {
        this.dragRectView = dragRectView;
        this.drawRectView= drawRectView;
        this.handler = handler;
        super.init();

    }

    /**
     * 处理一帧图像
     * @param bitmap    图像数据
     */
    public void doWork(Bitmap bitmap){
//        Log.v(TAG, String.format("bitmap size %d %d", bitmap.getWidth(), bitmap.getHeight()));
        byte[] bytes= new byte[bitmap.getByteCount()];
        ByteBuffer byteBuffer= ByteBuffer.wrap(bytes);
        bitmap.copyPixelsToBuffer(byteBuffer);
        doWork(bytes, bitmap.getWidth(), bitmap.getHeight());
    }

    private int targetRange = 128;

    /**
     * 处理一帧图像
     * @param bytes 图像数据
     * @param width 图像宽度
     * @param height    图像高度
     */
    public void doWork(final byte[] bytes, final int width, final int height) {

        if (!working) {
            cleanDrawRectView();
            return;
        }
//        Log.v(TAG, "dowork thread name:"+Thread.currentThread().getName());

        //判断当前是否有手画框，若为真，则新建一个跟踪器
        if (dragRectView.isDrawing() && dragRectView.getmRect() != null) {
            Rect rect = dragRectView.getmRect();

            //手选画框View的尺寸与图像尺寸可能不一致，此处处理二者对应关系
            rect.left*= ((double)width)/dragRectView.getWidth();
            rect.right*= ((double)width)/dragRectView.getWidth();
            rect.top*= ((double)height)/dragRectView.getHeight();
            rect.bottom*= ((double)height)/dragRectView.getHeight();

            //消耗当前手画框，从UI上清除手画框
            dragRectView.setDrawing(false);

            //若手画框尺寸太小则忽略之
            if (rect.right - rect.left > 10 && rect.bottom - rect.top > 10) {
                tracker = new TrackerX();
                tracker.Init(bytes, width, height, trackerMode, rect);
//                Log.v(TAG, String.format("tracker is init. frame size: %d %d. mode:%d. rect:%d %d %d %d", width, height, trackerMode, rect.left, rect.top, rect.right, rect.bottom));
            }
        }

        //如跟踪器不为空，且非忙碌，则放入一帧图像到跟踪器，如lock被占用则跟踪器当前正忙
        if (null != tracker && lock.tryLock()) {
            lock.unlock();
            if (null != mBackgroundHandler)
                mBackgroundHandler.post(new Runnable() {
                    @Override
                    public void run() {
//                        Log.v(TAG, "tracker thread name:"+Thread.currentThread().getName());
                        if (lock.tryLock()) {
                            try {
                                //跟踪一帧图像
                                final Rect rect = tracker.Track(bytes);

                                //若找不到目标则在UI上清除结果框
                                if (null == rect){
                                    cleanDrawRectView();

//                                    flightData.setFlightData(128,128,128,128);
                                    flightData.setJotstickMode(128,128,128,128);
                                } else {
//                                    Log.v(TAG, String .format("tracker finds the object. position: %d %d %d %d", rect.left, rect.top, rect.right, rect.bottom));
                                    int x = width/2;
                                    int y = height/2;

                                    //获取矩阵的中心位置
                                    int centerX = rect.centerX();
                                    int centerY = rect.centerY();

                                    int offsetX = centerX - x;
                                    int offsetY = centerY - y;

                                    //偏移比例
                                    int ratioX = width/255;
                                    int ratioY = height/255;

                                   int targetX = targetRange + offsetX/ratioX;
                                   int targetY = targetRange + offsetY/ratioY;

//                                    Log.e(TAG, "run: ----->targetX:"+targetX );
//                                    Log.e(TAG, "run: ----->targetY:"+targetY );

                                    flightData.setTrack(targetX,targetY);

                                    //找到目标则在UI上画出目标所在位置
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (View.VISIBLE == drawRectView.getVisibility())
                                                drawRectView.drawRects(new Rect[]{rect}, width, height, drawRectView.getWidth(), drawRectView.getHeight());
                                        }
                                    });
                                }
                            } finally {
                                lock.unlock();
                            }

                        }

                    }
                });
            else
                Log.v(TAG, "mBackgroundHandler is null.");

        }
    }



}
