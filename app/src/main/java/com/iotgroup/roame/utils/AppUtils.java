package com.iotgroup.roame.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by huanghao on 2017/3/23.
 */

public class AppUtils {

    /**
     * 获取程序的版本名称和信息
     * @param context
     * @return
     */
    public static String getVersionName(Context context){

        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;

    }

}
