package com.iotgroup.roame.utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by huanghao on 2017/3/8.
 */

public class FlightDataInputStream extends DataInputStream {

    private final byte[] START_MARKER = { (byte) 0xCC};
    private final byte[] END_MARKER = { (byte) 0x33 };
    private static final int FRAME_MAX_LENGTH = 25 ;

    public FlightDataInputStream(InputStream in) {
        super(new BufferedInputStream(in,FRAME_MAX_LENGTH));
    }

    private int getStartOfSequence(DataInputStream in ,byte[] sequence) throws IOException {

        int end = getEndOfSequence(in, sequence);
        return (end < 0) ? (-1) : (end - sequence.length);

    }

    private int getEndOfSequence(DataInputStream in, byte[] sequence) throws IOException {

        int seqIndex = 0;
        byte c;
        for (int i = 0; i < FRAME_MAX_LENGTH; i++) {
            c = (byte) in.readUnsignedByte();

            if (c == sequence[seqIndex]) {

                seqIndex++;

                if (seqIndex == sequence.length){
                    return i + 1;
                }
            } else
                seqIndex = 0;
        }
        return -1;

    }

    private int mContentLength = -1;

    public byte[] getValidFlightData(){
        mark(FRAME_MAX_LENGTH);

        int headerLen = 0;

        try {
            headerLen = getStartOfSequence(this,START_MARKER);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reset();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(headerLen != 0){
            byte[] header = new byte[headerLen];

            try {
                readFully(header);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            mContentLength = getEndOfSequence(this,END_MARKER);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            if(mContentLength == -1){
                return null;
            }
            reset();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] frameData = new byte[mContentLength];

        try {
            skipBytes(headerLen);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            readFully(frameData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return frameData;

    }

}
