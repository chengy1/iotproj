package com.iotgroup.roame.utils;


import java.lang.Float;

/**
 * 十六进制 字节数组转换
 *
 * @author Administrator
 */
public class Hex {

    /**
     * 用于建立十六进制字符的输出的小写字符数组
     */
    private static final char[] DIGITS_LOWER = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * 用于建立十六进制字符的输出的大写字符数组
     */
    private static final char[] DIGITS_UPPER = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /**
     * 将字节数组转换为十六进制字符数组
     *
     * @param data byte[]
     * @return 十六进制char[]
     */
    public static char[] encodeHex(byte[] data) {
        return encodeHex(data, true);
    }

    /**
     * 将字节数组转换为十六进制字符数组
     *
     * @param data        byte[]
     * @param toLowerCase <code>true</code> 传换成小写格式 ， <code>false</code> 传换成大写格式
     * @return 十六进制char[]
     */
    public static char[] encodeHex(byte[] data, boolean toLowerCase) {
        return encodeHex(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
    }

    /**
     * 将字节数组转换为十六进制字符数组
     *
     * @param data     byte[]
     * @param toDigits 用于控制输出的char[]
     * @return 十六进制char[]
     */
    protected static char[] encodeHex(byte[] data, char[] toDigits) {
        if (data == null)
            return new char[0];
        int l = data.length;
        char[] out = new char[l << 1];
        // two characters form the hex value.
        for (int i = 0, j = 0; i < l; i++) {
            out[j++] = toDigits[(0xF0 & data[i]) >>> 4];
            out[j++] = toDigits[0x0F & data[i]];
        }
        return out;
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param data byte[]
     * @return 十六进制String
     */
    public static String encodeHexStr(byte[] data) {
        return encodeHexStr(data, true);
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param data        byte[]
     * @param toLowerCase <code>true</code> 传换成小写格式 ， <code>false</code> 传换成大写格式
     * @return 十六进制String
     */
    public static String encodeHexStr(byte[] data, boolean toLowerCase) {
        return encodeHexStr(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param data     byte[]
     * @param toDigits 用于控制输出的char[]
     * @return 十六进制String
     */
    protected static String encodeHexStr(byte[] data, char[] toDigits) {
        return new String(encodeHex(data, toDigits));
    }

    /**
     * 将十六进制字符数组转换为字节数组
     *
     * @param data 十六进制char[]
     * @return byte[]
     * @throws RuntimeException 如果源十六进制字符数组是一个奇怪的长度，将抛出运行时异常
     */
    public static byte[] decodeHex(char[] data) {

        int len = data.length;

        if ((len & 0x01) != 0) {
            throw new RuntimeException("Odd number of characters.");
        }

        byte[] out = new byte[len >> 1];

        // two characters form the hex value.
        for (int i = 0, j = 0; j < len; i++) {
            int f = toDigit(data[j], j) << 4;
            j++;
            f = f | toDigit(data[j], j);
            j++;
            out[i] = (byte) (f & 0xFF);
        }

        return out;
    }

    /**
     * 将十六进制字符转换成一个整数
     *
     * @param ch    十六进制char
     * @param index 十六进制字符在字符数组中的位置
     * @return 一个整数
     * @throws RuntimeException 当ch不是一个合法的十六进制字符时，抛出运行时异常
     */
    protected static int toDigit(char ch, int index) {
        int digit = Character.digit(ch, 16);
        if (digit == -1) {
            throw new RuntimeException("Illegal hexadecimal character " + ch + " at index " + index);
        }
        return digit;
    }

    /**
     * 字符串分割打印
     *
     * @param info
     * @param division
     */
    public static String printStringDivision(String info, int division) {
        StringBuffer sb = new StringBuffer();
        int size = info.length() / division;
        int remainder = info.length() % division;
        for (int i = 0; i < size; i++) {
            sb.append(info.substring(i * division, (i + 1) * division) + " ");
        }
        if (remainder != 0)
            sb.append(info.substring(size * division, info.length()) + " ");
//		System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * 无符号32位bit转化为无符号int
     *
     * @return
     */
    static public int u32ToInt(int start, byte[] buf) {
        int flag = 0;
        for (int j = 0; j < 4; j++) {
            flag += Math.round((buf[start + j] & 0xFF) << (8 * j));
        }
        return (int) (flag < 0 ? flag + Math.pow(2, 32) : flag);

    }


    /***********************************************
     * Copyright  : @aee-tech
     * Description: 无符号整数转换程序
     * Date       : 2016-06
     * Author     : softgroup05@aee.com
     ***********************************************/
    static public long Byte4ToInt(byte[] buf, int istart) {
        int[] iArrayData = new int[4];
        for (int i = 0; i < 4; i++) {
            iArrayData[i] = (0x000000FF & (buf[i + istart]));
        }
        long unsignedResult = (iArrayData[0] | iArrayData[1] << 8 | iArrayData[2] << 16 | iArrayData[3] << 24) & 0xFFFFFFFFL;
        return unsignedResult;
    }

    /**
     * 无符号32位bit转化为无符号int 高位在前
     *
     * @return
     */
    static public int u32ToIntHigh(int start, byte[] buf) {
        int flag = 0;

        flag += Math.round((buf[start + 2] & 0xFF) << (8 * 0));
        flag += Math.round((buf[start + 3] & 0xFF) << (8 * 1));
        flag += Math.round((buf[start + 0] & 0xFF) << (8 * 2));
        flag += Math.round((buf[start + 1] & 0xFF) << (8 * 3));
        return (int) (flag < 0 ? flag + Math.pow(2, 32) : flag);

    }


    /**
     * 无符号16位bit转化为无符号int
     *
     * @return
     */
    static public int u16ToInt(int start, byte[] buf) {
        int flag = 0;
        for (int j = 0; j < 2; j++) {
            flag += Math.round((buf[start + j] & 0xFF) << (8 * j));
        }
        return (int) (flag < 0 ? flag + Math.pow(2, 16) : flag);

    }

    /**
     * 32位bit转化为浮点型
     *
     * @return
     */
//    static public float byte32ToFloat(int start, byte[] buf) {
//        byte[] arr = new byte[4];
//        System.arraycopy(buf, start, arr, 0, 4);
//        return ByteUtils.byte4ToFloat(arr);
//    }


    /**
     * 得到经纬度数值
     *
     * @return
     */
    public static float getLLValue(int start, byte[] data) {
        int flag = 0;

        flag += Math.round((data[start + 1] & 0xFF) << (8 * 3));
        flag += Math.round((data[start + 0] & 0xFF) << (8 * 2));
        flag += Math.round((data[start + 3] & 0xFF) << (8 * 1));
        flag += Math.round((data[start + 2] & 0xFF) << (8 * 0));

        return (float) (((float) (flag < 0 ? flag + Math.pow(2, 32) : flag)) / Math.pow(10, 7));
    }

    public static float getLongValue(int start, byte[] data) {
        int flag = 0;

        flag += Math.round((data[start + 0] & 0xFF) << (8 * 3));
        flag += Math.round((data[start + 1] & 0xFF) << (8 * 2));
        flag += Math.round((data[start + 2] & 0xFF) << (8 * 1));
        flag += Math.round((data[start + 3] & 0xFF) << (8 * 0));

        return (float) (((float) (flag < 0 ? flag + Math.pow(2, 32) : flag)) / Math.pow(10, 7));
    }


    /**
     * 得到高度或速度或距离
     *
     * @return
     */
    public static float getHeightOrSpeedOrDistance(int start, byte[] data) {
        int flag = 0;

        flag += Math.round((data[start + 1] & 0xFF) << (8 * 3));
        flag += Math.round((data[start + 0] & 0xFF) << (8 * 2));
        flag += Math.round((data[start + 3] & 0xFF) << (8 * 1));
        flag += Math.round((data[start + 2] & 0xFF) << (8 * 0));

        return (float) (((flag)) / Math.pow(10, 2));
    }


    public static void main(String[] args) {
        String srcStr = "待转换字符串";
        String encodeStr = encodeHexStr(srcStr.getBytes());
        String decodeStr = new String(decodeHex(encodeStr.toCharArray()));
        System.out.println("转换前：" + srcStr);
        System.out.println("转换后：" + encodeStr);
        System.out.println("还原后：" + decodeStr);
    }


    public static int GetVaueByPos(int start, byte[] data) {
        int iValue = 0;
        iValue += Math.round(data[start] & 0xFF);
        return iValue;

    }


    public static float GetFloat(int iNum) {
        float fRet = 0;
        int iRead = iNum;
        if (iNum >= 32768) {
            iRead -= 32768;
            fRet = (float) (-0.1 * iRead);
        } else {
            fRet = (float) (0.1 * iRead);
        }
        return fRet;
    }

    public static float GetFloatValue(int iValue) {
        float fRet = 0;

        fRet = (float) (0.1 * iValue);
        return fRet;
    }


    /***********************************************
     * 状态字解析
     *
     * @author aee
     * mode 1-7  speed 1-3
     ***********************************************/
    public byte[] GetStation(int istate) {
        byte[] CurrentState = new byte[32];

        for (int i = 0; i < 32; i++) {
            if ((byte) ((istate >>> i) & 1) == 1) {
                CurrentState[i] = 1;
            } else {
                CurrentState[i] = 0;
            }
        }

        return CurrentState;
    }

    public static byte[] GetByteFromStr(String strByte) {
        byte[] bSend;
        String strValue = "";
        int bvalue = 0;
        int iLen = strByte.length() / 2;
        bSend = new byte[iLen];
        try {
            for (int i = 0; i < strByte.length() / 2; i++) {
                strValue = strByte.substring(i * 2, i * 2 + 2);
                bvalue = Integer.parseInt(strValue, 16);
                bSend[i] = (byte) bvalue;
            }

        } catch (Exception localException) {

        }
        return bSend;
    }


    public static int LongToBytes(byte[] data, long num, int offset, int len) {
        for (int i = 0; i < len; i++) {
            data[offset + i] = (byte) num;
            num >>= 8;
        }
        return len;
    }


    /***********************************************
     * Copyright  : @aee-tech
     * Description: 浮点转换为字节
     * Date       : 2016-07
     * Author     : softgroup05@aee.com
     ***********************************************/

    public static byte[] float2byte(float f) {

        int fbit = Float.floatToIntBits(f);

        byte[] b = new byte[4];
        for (int i = 0; i < 4; i++) {
            b[i] = (byte) (fbit >> (24 - i * 8));
        }


        int len = b.length;
        byte[] dest = new byte[len];

        System.arraycopy(b, 0, dest, 0, len);
        byte temp;
        for (int i = 0; i < len / 2; ++i) {
            temp = dest[i];
            dest[i] = dest[len - i - 1];
            dest[len - i - 1] = temp;
        }

        return dest;

    }


    /***********************************************
     * Copyright  : @aee-tech
     * Description: 字节转换为浮点 (index 开始位置 )
     * Date       : 2016-07
     * Author     : softgroup05@aee.com
     ***********************************************/

    public static float byte2float(byte[] b, int index) {
        int l;
        l = b[index + 0];
        l &= 0xff;
        l |= ((long) b[index + 1] << 8);
        l &= 0xffff;
        l |= ((long) b[index + 2] << 16);
        l &= 0xffffff;
        l |= ((long) b[index + 3] << 24);
        return Float.intBitsToFloat(l);
    }

    public static int bytetoshort(byte[] b, int index) {
        int ret = 0;
        byte high = (byte) (0x000000FF & b[index + 1]);
        byte low = (byte) (0x000000FF & b[index]);
        short z = (short) (((high & 0x000000FF) << 8) | (0x000000FF & low));
        return z;
    }

    /***********************************************
     * Copyright  : @aee-tech
     * Description: byte to string
     * Date       : 2016-08
     * Author     : softgroup05@aee.com
     ***********************************************/
    public static String bytetostring(byte[] aryHex, int ilen) {
        String ResultString = "";
        StringBuffer tStringBuf = new StringBuffer();
        char[] tChars = new char[ilen];
        for (int i = 0; i < ilen; i++) {
            tChars[i] = (char) aryHex[i];
        }
        tStringBuf.append(tChars);
        ResultString = tStringBuf.toString();
        return ResultString;
    }


}

