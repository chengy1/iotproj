package com.iotgroup.roame.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日志写入工具类
 * @author huanghao
 *
 */
public class LogWriter {

	private static LogWriter mLogWriter;

	private static String mFilePath;

	private static Writer mWriter;

	private static SimpleDateFormat df;

	private LogWriter(String filePath) {
		LogWriter.mFilePath = filePath;
		LogWriter.mWriter = null;
	}

	public static LogWriter open(String filePath) throws IOException {

		if (mLogWriter == null) {
			mLogWriter = new LogWriter(filePath);
		}

		File mFile = new File(mFilePath);

		mWriter = new BufferedWriter(new FileWriter(mFilePath), 1024 * 1000);

		df = new SimpleDateFormat("[yy-MM-dd hh:mm:ss:SSS]: ");

		return mLogWriter;
	}

	public void close() throws IOException {
		mWriter.close();
	}

	public void print(byte log) throws IOException {
		mWriter.write(df.format(new Date()));
		mWriter.write(log);
		mWriter.write("\n");
		mWriter.flush();
	}

}
