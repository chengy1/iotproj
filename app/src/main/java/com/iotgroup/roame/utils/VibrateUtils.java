package com.iotgroup.roame.utils;

import android.content.Context;
import android.os.Vibrator;

/**
 * Created by  huanghao on 2017/2/24.
 */

public class VibrateUtils {

    private static VibrateUtils vibrateUtils;
    private static Vibrator vibrator;

    /**
     * 简单震动
     * @param context
     * @param millisecond 震动时间，毫秒
     */
    public static void shakeSimple(Context context, int millisecond){
        vibrator = (Vibrator)context.getSystemService(context.VIBRATOR_SERVICE);
        vibrator.vibrate(millisecond);
    }

    /**
     * 复杂的震动
     * @param context
     * @param pattern 震动的形式
     *
     *                new long[]{100,10,100,1000}
     *                第一个参数为等待时间后开始震动
     *                第二个参数为震动时间
     *                第三个参数为等待震动的时间
     *                第四个参数为震动的事件时间
     *
     * @param repeate 震动的次数 -1位不重复，非-1为从pattern的指定下标开始重复
     */
    public static void shakeComplicated(Context context, long[] pattern, int repeate){
        vibrator = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
        vibrator.vibrate(pattern, repeate);
    }

    public  static  void shakeStop(){
        if(vibrator != null){
            vibrator.cancel();
        }
    }

}
