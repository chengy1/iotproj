package com.iotgroup.roame.utils;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;

import java.util.List;

public class WifiManagerUtils {
	private WifiManager wifiManager;
	private WifiInfo wifiInfo;
	private List<ScanResult> scanResultList;
	private List<WifiConfiguration> wifiConfigList;
	private WifiLock wifiLock;

	public WifiManagerUtils(Context context) {
		this.wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		this.wifiInfo = wifiManager.getConnectionInfo();
	}

	public boolean getWifiStatus() {
		return wifiManager.isWifiEnabled();
	}

	public boolean openWifi() {
		if (!wifiManager.isWifiEnabled()) {
			return wifiManager.setWifiEnabled(true);
		} else {
			return false;
		}
	}

	public boolean closeWifi() {
		if (!wifiManager.isWifiEnabled()) {
			return true;
		} else {
			return wifiManager.setWifiEnabled(false);
		}
	}

	public void lockWifi() {
		wifiLock.acquire();
	}

	public void unLockWifi() {
		if (!wifiLock.isHeld()) {
			wifiLock.release();
		}
	}

	public void createWifiLock() {
		wifiLock = wifiManager.createWifiLock("flyfly");
		// 创建一个锁的标志
	}

	// 扫描网络
	public void startScan() {
		wifiManager.startScan();
		scanResultList = wifiManager.getScanResults();
		// 扫描返回结果列表
		wifiConfigList = wifiManager.getConfiguredNetworks();
		// 扫描配置列表
	}

	public List<ScanResult> getWifiList() {
		return scanResultList;
	}

	public List<WifiConfiguration> getWifiConfigList() {
		return wifiConfigList;
	}

	// 获取扫描列表
	public StringBuilder lookUpscan() {
		StringBuilder scanBuilder = new StringBuilder();
		for (int i = 0; i < scanResultList.size(); i++) {
			scanBuilder.append("编号：" + (i + 1));
			scanBuilder.append(scanResultList.get(i).toString());
			// 所有信息
			scanBuilder.append("\n");
		}
		return scanBuilder;
	}

	// 获取指定信号的强度
	public int getLevel(int NetId) {
		return scanResultList.get(NetId).level;
	}

	// 获取本机Mac地址
	public String getMac() {
		return (wifiInfo == null) ? "" : wifiInfo.getMacAddress();
	}

	public String getBSSID() {
		return (wifiInfo == null) ? null : wifiInfo.getBSSID();
	}

	public String getSSID() {
		return (wifiInfo == null) ? null : wifiInfo.getSSID();
	}

	// 返回当前连接的网络的ID
	public int getCurrentNetId() {
		return (wifiInfo == null) ? null : wifiInfo.getNetworkId();
	}

	// 返回所有信息
	public String getwifiInfo() {
		return (wifiInfo == null) ? null : wifiInfo.toString();
	}

	// 获取IP地址
	public int getIP() {
		return (wifiInfo == null) ? null : wifiInfo.getIpAddress();
	}

	// 添加一个连接
	public boolean addNetWordLink(WifiConfiguration config) {
		int NetId = wifiManager.addNetwork(config);
		return wifiManager.enableNetwork(NetId, true);
	}

	// 禁用一个链接
	public boolean disableNetWordLick(int NetId) {
		wifiManager.disableNetwork(NetId);
		return wifiManager.disconnect();
	}

	// 移除一个链接
	public boolean removeNetworkLink(int NetId) {
		return wifiManager.removeNetwork(NetId);
	}

	// 不显示SSID
	public void hiddenSSID(int NetId) {
		wifiConfigList.get(NetId).hiddenSSID = true;
	}

	// 显示SSID
	public void displaySSID(int NetId) {
		wifiConfigList.get(NetId).hiddenSSID = false;
	}
}